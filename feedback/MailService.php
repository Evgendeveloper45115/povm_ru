<?php

namespace app\feedback;

use app\auth\models\User;
use app\companion\model\Trip;
use app\models\Respond;
use Yii;
use yii\base\Exception;
use yii\helpers\VarDumper;
use yii\mail\MailerInterface;

class MailService
{
    private $mailer;

    public function __construct
    (
        MailerInterface $mailer
    )
    {
        $this->mailer = $mailer;
    }

    public function sendConfirmEmail(User $user)
    {
        if ($user->email || $user->email != '') {
            $sent = $this->mailer
                ->compose(
                    ['html' => 'register', 'text' => 'register-text'],
                    [
                        'email' => '',
                        'confirmCode' => $user->confirm_code,
                    ]
                )
                ->setFrom('info@povm.ru')
                ->setTo($user->email)
                ->setSubject('Регистрация на сайте ' . Yii::$app->name)
                ->send();

            if (!$sent) {
                throw new \RuntimeException('Email sending error.');
            }
        } else {
            throw new \RuntimeException('Email sending error.');
        }
    }

    /**
     * @param User $user
     * @param $message_text
     * @return bool
     */
    public function sendNewMessage(User $user, $message_text)
    {
        $user->confirm_code = Yii::$app->security->generateRandomString(64);
        $user->save(false);
        if ($user->email || $user->email != '') {
            $this->mailer
                ->compose(
                    'send_message',
                    [
                        'email' => '',
                        'user' => $user,
                        'message_text' => $message_text
                    ]
                )
                ->setFrom('info@povm.ru')
                ->setTo($user->email)
                ->setSubject('Вам пришло новое сообщение')
                ->send();
        }
        return true;
    }

    /**
     * @param User $user
     * @param Trip $trip
     * @return bool
     */
    public function sendRespondMessage(User $user, Trip $trip)
    {
        $respond = Respond::findOne(['trip_id' => $trip->id, 'user_id' => Yii::$app->user->identity->id]);
        $user->save(false);
        if ($user->email || $user->email != '') {
            $this->mailer
                ->compose(
                    'send_respond_message',
                    [
                        'user' => $user,
                        'trip' => $trip,
                        'respond' => $respond,
                    ]
                )
                ->setFrom('info@povm.ru')
                ->setTo($user->email)
                ->setSubject('Новый отклик!')
                ->send();
        }
        return true;
    }
}
