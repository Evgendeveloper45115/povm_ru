<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "search_log_trip".
 *
 * @property int $id
 * @property string $townFrom
 * @property string $townTo
 * @property string $town_from_id
 * @property string $town_to_id
 * @property int $counter
 * @property GeoTown $townToModel
 * @property GeoTown $townFromModel
 */
class SearchLogTrip extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'search_log_trip';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['counter','town_from_id','town_to_id'], 'integer'],
            [['townFrom', 'townTo'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'townFrom' => 'Town From',
            'townTo' => 'Town To',
            'counter' => 'Counter',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTownToModel()
    {
        return $this->hasOne(GeoTown::class, ['id' => 'town_to_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTownFromModel()
    {
        return $this->hasOne(GeoTown::class, ['id' => 'town_from_id']);
    }

}
