<?php

namespace app\models;


use app\companion\model\Trip;
use yii\data\ActiveDataProvider;
use yii\helpers\VarDumper;

class TripSearch extends Trip
{
    public $tripTime;
    public $townFrom;
    public $townTo;
    public $tripStatus;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'in', 'range' => array_keys(static::getTypeList())],
            [['tripStatus'], 'in', 'range' => array_keys(static::getStatusList())],
            [['tripTime'], 'safe'],
        ];
    }

    /**
     * @param $params
     * @param bool $my
     * @return ActiveDataProvider
     */
    public function search($params, $my = false)
    {
        $query = static::find();
        if ($my) {
            $query->where(['user_id' => \Yii::$app->user->id]);
            $query->orderBy('id DESC');
        } else {
            $query->orderBy('trip_time ASC');
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['type' => $this->type]);
        $tripTime = date('Y-m-d H:i:s');
        if ($this->tripTime && $this->tripTime > date('Y-m-d H:i:s')) {
            $tripTime = $this->tripTime;
        }
        if (!$my) {
            //  $query->andFilterWhere(['>=', 'trip_time', $tripTime]);
            $query->andWhere(['in', 'status', [static::STATUS_ACTIVE]]);
        } else {
            $query->andWhere(['in', 'status', [static::STATUS_ACTIVE, static::STATUS_FINISHED]]);
        }


        return $dataProvider;
    }

    /**
     * @param $townFrom
     * @param $townTo
     * @param null $type
     * @return ActiveDataProvider
     */
    public function searchByTown($townFrom, $townTo, $type = null)
    {
        $query = Trip::find();
        $query->orderBy('trip_time ASC');
        $query->where(['in', 'status', [static::STATUS_ACTIVE, static::STATUS_FINISHED]]);
        $query->andWhere(['town_from' => $townFrom->name]);
        $query->andWhere(['town_to' => $townTo->name]);
        if ($type !== null) {
            $query->andWhere(['type' => $type]);
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }
}
