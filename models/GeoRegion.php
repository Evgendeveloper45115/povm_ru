<?php

namespace app\models;

/**
 * This is the model class for table "geo_region".
 *
 * @property int $id
 * @property string $name
 * @property string $alias
 * @property int $country_id
 * @property int $buy_price
 * @property int $sell_price
 *
 * @property GeoCountry $country
 * @property GeoTown[] $towns
 */
class GeoRegion extends \yii\db\ActiveRecord implements GeoInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'geo_region';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'alias', 'country_id'], 'required'],
            [['country_id', 'buy_price', 'sell_price'], 'integer'],
            [['name', 'alias'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'alias' => 'Alias',
            'country_id' => 'Country ID',
            'buy_price' => 'Buy Price',
            'sell_price' => 'Sell Price',
        ];
    }

    /**
     * @return array
     */
    public function getUrl()
    {
        return ['/trip/region', 'country' => $this->country->alias, 'region' => $this->alias];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(GeoCountry::class, ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTowns()
    {
        return $this->hasMany(GeoTown::class, ['region_id' => 'id'])->orderBy(['name' => SORT_ASC]);
    }

    /**
     * @param string $alias
     * @return GeoRegion|array|\yii\db\ActiveRecord|null
     */
    public static function getByAlias($alias)
    {
        return static::find()->where(['alias' => $alias])->one();
    }
}
