<?php

namespace app\models;

use app\auth\models\User;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "message".
 *
 * @property integer $id
 * @property integer $messageFrom
 * @property integer $messageTo
 * @property string $text
 * @property integer $viewed
 * @property string $ip
 * @property string $datetime
 * @property integer $dialog_id
 * @property User $fromUser
 * @property User $toUser
 * @property Dialog $dialog
 */
class Message extends ActiveRecord
{
    public $messageName;
    public $avatar;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'message';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['messageFrom', 'messageTo', 'text'], 'required'],
            [['viewed', 'dialog_id'], 'integer'],
            [['text', 'ip'], 'string'],
            [['datetime'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'messageTo' => 'Получатель',
            'messageFrom' => 'Отправитель',
            'viewed' => 'Увидел ли клиент',
            'text' => 'Текст сообщения',
            'ip' => 'Ип',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToUser()
    {
        return $this->hasOne(User::class, ['id' => 'messageTo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public function getFromUser()
    {
        return $this->hasOne(User::class, ['id' => 'messageFrom']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public function getDialog()
    {
        return $this->hasOne(Dialog::class, ['id' => 'dialog_id']);
    }

}
