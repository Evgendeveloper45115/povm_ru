<?php

namespace app\models;


use app\companion\model\Trip;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Query;
use yii\helpers\VarDumper;

class MessageSearch extends Message
{

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Dialog::find();
        $query->leftJoin(Message::tableName(), 'dialog.id = message.dialog_id');
        $query->where(['messageTo' => \Yii::$app->user->id]);
        $query->orWhere(['messageFrom' => \Yii::$app->user->id]);
        $query->orderBy("message.id DESC");
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);
        return $dataProvider;
    }

    public function searchView($dialog_id)
    {
        $query = static::find();
        $query->where(['dialog_id' => $dialog_id]);
        $query->with(["toUser", "fromUser"]);
        $cnt = $query->count();
        $query->limit(15);
        $query->offset($cnt - $query->limit);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);
        return $dataProvider;
    }
}
