<?php

namespace app\models;


use yii\base\Model;

class SearchForm extends Model
{
    public $from;

    public $to;

    public $date;

    public $passengers;

    public $role;


    public function rules()
    {
        return [
            [['from', 'to', 'passengers', 'role'], 'integer'],
        ];
    }
}