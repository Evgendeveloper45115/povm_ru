<?php
/**
 * Created by PhpStorm.
 * User: Vitman
 * Date: 07.09.2019
 * Time: 11:18
 */

namespace app\models;


use Yii;
use yii\helpers\VarDumper;

class RouteQueryObject
{
    /**
     * Получение массива первых букв городов отправления в поисковых запросах
     * @return array
     * @throws \yii\db\Exception
     */
    public static function getFirstLettersOfTownsFrom()
    {
        $sql = 'SELECT SUBSTRING(town_from.name, 1, 1) first_letter
FROM `search_log_trip` log
LEFT JOIN geo_town town_to ON town_to.id = log.town_to_id  
LEFT JOIN geo_town town_from ON town_from.id = log.town_from_id 
where town_from.id is not null AND town_to.id is not null
GROUP BY first_letter
ORDER BY town_from.name ASC';

        return \Yii::$app->db->createCommand($sql)->queryColumn();
    }

    /**
     * Получение массива данных о поисках поездок по первой букве города отправления
     * @param string $letter
     * @return array
     * @throws \yii\db\Exception
     */
    public static function getRoutesByFirstLetter($letter, $limit = 10000, $cacheTime = 3600)
    {
        $key = 'routes-by-letter-' . $letter . $limit;
        $trips = Yii::$app->cache->get($key);

        if ($trips === false) {

            $sql = "SELECT town_from.name from_name, town_from.alias from_alias, town_to.name to_name, town_to.alias to_alias
FROM `search_log_trip` log
LEFT JOIN geo_town town_to ON town_to.id = log.town_to_id  
LEFT JOIN geo_town town_from ON town_from.id = log.town_from_id 
where town_from.id is not null AND town_to.id is not null AND SUBSTRING(town_from.name, 1, 1) = :letter
ORDER BY from_name ASC
LIMIT :limit";

            $trips = \Yii::$app->db->createCommand($sql)
                ->bindValues([
                    ':letter' => $letter,
                    ':limit' => $limit,
                ])
                ->queryAll();

            Yii::$app->cache->set($key, $trips, $cacheTime);
        }

        return $trips;
    }

    /**
     * Определяем есть ли такой поиск в БД
     * @param $from_id
     * @param $to_id
     * @param $fromName
     * @param $toName
     */
    public static function updateCreateSearchLogByFromTo($from_id, $to_id, $fromName, $toName)
    {
        $sql = "SELECT id, counter
FROM `search_log_trip` log
where log.town_from_id " . ($from_id ? '=' : 'IS') . " :townFrom AND log.town_to_id " . ($to_id ? '=' : 'IS') . " :townTo";

        $connection = Yii::$app->db;
        $search = $connection->createCommand($sql)
            ->bindValues([
                ':townFrom' => $from_id,
                ':townTo' => $to_id,
            ])->queryOne();
        if ($search) {
            $connection->createCommand()->update('search_log_trip', ['counter' => $search['counter'] + 1], 'id = ' . $search['id'])->execute();
        } else {
            $model = new SearchLogTrip();
            $model->townFrom = $fromName;
            $model->townTo = $toName;
            $model->town_from_id = $from_id;
            $model->town_to_id = $to_id;
            $model->save();
        }
    }
}