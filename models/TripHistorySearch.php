<?php

namespace app\models;

use app\companion\model\Trip;
use yii\data\ActiveDataProvider;
use yii\helpers\VarDumper;

/**
 * Class TripHistorySearch
 * @package app\models
 */
class TripHistorySearch extends Trip
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'integer'],
        ];
    }

    /**
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = static::find()->where(['in', 'status', [Trip::STATUS_ACTIVE, Trip::STATUS_FINISHED]]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['trip_time' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->andWhere(['or', ['user_id' => $this->user_id], ['driver_id' => $this->user_id]]);

        if (\Yii::$app->controller->id == 'auto' && \Yii::$app->controller->action->id == 'view') {
            $query->andWhere(['car_id' => \Yii::$app->request->get('id')]);
        }
        //$query->andWhere(['in', 'status', [static::STATUS_ACTIVE, static::STATUS_FINISHED]]);

        return $dataProvider;
    }
}
