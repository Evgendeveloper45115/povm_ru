<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "car_model".
 *
 * @property int $id ID
 * @property int $mark_id
 * @property string $name
 * @property string $name_rus
 * @property int $date_create
 * @property int $date_update
 * @property int $id_car_type
 * @property string $alias
 */
class CarModel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'car_model';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mark_id', 'name', 'id_car_type', 'alias'], 'required'],
            [['mark_id', 'date_create', 'date_update', 'id_car_type'], 'integer'],
            [['name', 'name_rus', 'alias'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mark_id' => 'Mark ID',
            'name' => 'Name',
            'name_rus' => 'Name Rus',
            'date_create' => 'Date Create',
            'date_update' => 'Date Update',
            'id_car_type' => 'Id Car Type',
            'alias' => 'Alias',
        ];
    }

    /**
     * @param $markId
     * @return array
     */
    public static function getDropdownList($markId)
    {
        if (!$markId) {
            return [];
        }

        $list = static::find()
            ->select(['id', 'name'])
            ->where(['mark_id' => $markId])
            ->orderBy(['name' => SORT_ASC])
            ->asArray()
            ->all();

        return ArrayHelper::map($list, 'id', 'name');
    }
}
