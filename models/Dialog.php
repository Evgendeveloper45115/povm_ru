<?php

namespace app\models;

use app\auth\models\User;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "dialog".
 *
 * @property integer $id
 * @property Message[] $messages
 * @property Message $message
 * @property Message $messagesBySender
 */
class Dialog extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dialog';
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public function getDialog()
    {
        return $this->hasMany(DialogHasUser::class, ['dialog_id' => 'id']);
    }

    public function getMessages()
    {
        return $this->hasMany(Message::class, ['dialog_id' => 'id']);
    }

    public function getMessagesBySender()
    {
        return $this
            ->hasMany(Message::class, ['dialog_id' => 'id'])
            ->andWhere(['messageFrom' => Yii::$app->user->id])
            ->orderBy('id DESC');
    }

    public function getMessage()
    {
        return $this->hasOne(Message::class, ['dialog_id' => 'id']);
    }
}
