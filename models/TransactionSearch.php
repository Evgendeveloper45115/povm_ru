<?php

namespace app\models;


use app\companion\model\Trip;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Query;
use yii\helpers\VarDumper;

class TransactionSearch extends Transaction
{

    /**
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = static::find()->where(['user_id' => \Yii::$app->getUser()->getId()]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 15,
            ],
        ]);
        return $dataProvider;
    }
}
