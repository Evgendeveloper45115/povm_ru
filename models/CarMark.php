<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "car_mark".
 *
 * @property int $id ID
 * @property string $name
 * @property string $name_rus
 * @property int $date_create
 * @property int $date_update
 * @property int $id_car_type
 * @property int $popular
 * @property string $alias
 */
class CarMark extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'car_mark';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'id_car_type', 'alias'], 'required'],
            [['date_create', 'date_update', 'id_car_type', 'popular'], 'integer'],
            [['name', 'name_rus', 'alias'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'name_rus' => 'Name Rus',
            'date_create' => 'Date Create',
            'date_update' => 'Date Update',
            'id_car_type' => 'Id Car Type',
            'popular' => 'Popular',
            'alias' => 'Alias',
        ];
    }

    /**
     * @return array
     */
    public static function getDropdownList()
    {
        $list = static::find()
            ->select(['id', 'name'])
            ->orderBy(['name' => SORT_ASC])
            ->asArray()
            ->all();

        return ArrayHelper::map($list, 'id', 'name');
    }
}
