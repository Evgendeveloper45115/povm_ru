<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "transaction".
 *
 * @property int $id
 * @property int $amount
 * @property string $create_at
 * @property int $user_id
 * @property string $description
 */
class Transaction extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transaction';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['amount', 'user_id'], 'required'],
            [['create_at'], 'safe'],
            [['user_id'], 'integer'],
            [['description'], 'string'],
            [['amount'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'amount' => 'Сумма',
            'create_at' => 'Дата',
            'user_id' => 'Клиент',
            'description' => 'Комментарий',
        ];
    }
}
