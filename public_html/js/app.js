$(function () {

    $('.my_auto').on('change', function () {
        if ($(this).val() != 0) {
            $('.custom_disable').find('select').each(function (k, v) {
                $(this).attr('disabled', 'disabled')
            });
        } else {
            $('.custom_disable').find('select').each(function (k, v) {
                $(this).attr('disabled', false)
            });
        }
    });
    $("select.select2").select2();

    $("select.select-town").select2({
        minimumInputLength: 3,
        ajax: {
            cache: false,
            allowClear: true,
            dataType: 'json',
            delay: 250,
            type: "POST",
            timeout: 6000,
            data: function (params) {
                return {
                    query: params.term,
                    _csrf: yii.getCsrfToken()
                };
            },
        }
    });


    var $carMark = $("#car-mark");
    var $carModel = $("#car-model");

    $carMark.change(function () {
        var id = $(this).val();
        $carModel.empty();
        $carModel.append('<option value="">- Выбрать -</option>');
        $.post("/car/models/", {id: id}, function (data) {
            if (data && data.length > 0) {
                for (var i in data) {
                    $carModel.append(new Option(data[i].name, data[i].id, false, false));
                }
                $carModel.trigger('change');
            }
        }, "json");
    });

    var $step2FormwayBack = $("#step2form-wayback");
    $("#step2form-wayback").change(function () {
        if ($("#step2form-wayback").is(":checked")) {
            $(".date-back").show();
        } else {
            $(".date-back").hide();
        }
    });
    $("#step2form-wayback").change();


    $("#simpletripform-wayback").change(function () {
        if ($("#simpletripform-wayback").is(":checked")) {
            $(".date-back").show();
        } else {
            $(".date-back").hide();
        }
    });
    $("#simpletripform-wayback").change();

    $(document).on('change touch', '.progress_load_form_photo_upload input', function () {
        InputFile = $(this);
        var files = this.files;
        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            if (!file.type.match(/image\/(jpeg|jpg|png|gif)/)) {
                alert('Фотография должна быть в формате jpg, png или jpeg');
                return false;
            }
            preview(files[i]);
        }
    });
    var isSupportDragAndDrop = function () {
        var div = document.createElement('div');
        return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
    }()
    if (isSupportDragAndDrop) {

        $('body').on('drag dragstart dragend dragover dragenter dragleave drop', '.progress_load_form_photo, .profile_photo, .progress_load_form_photo_upload', function (e) {
            e.preventDefault();
            e.stopPropagation();
        })
        $('body').on('drop dragdrop', '.progress_load_form_photo, .profile_photo, .progress_load_form_photo_upload', function (e) {

            var files = e.originalEvent.dataTransfer.files;
            var $fileInput = $('.progress_load_form_photo_upload:visible, .profile_photo').find('input[type="file"]');
            $fileInput.prop('files', files);
            $fileInput.change();

            preview(files[0]);
        })

    }

    function preview(file) {
        var reader = new FileReader();
        reader.addEventListener('load', function (event) {
            InputFile.parent().prev('.img-responsive').attr('src', event.target.result);
            InputFile.parent().prev('.img-responsive').find('.text').remove();
            if ($('.img_profile').length) {
                $('.img_profile').attr('src', '')
            }
            $('.send-button').css('display', 'block');
            if (InputFile.parent().prev('.img-responsive').find('.help-block').length) {
                InputFile.parent().prev('.img-responsive').find('.help-block').remove();
            }
        });
        reader.readAsDataURL(file);
    }

    $('.roles').on('change', function () {
        if ($(this).val() === 'carrier') {
            $('.carrier_fields').css('display', 'block')
        }else{
            $('.carrier_fields').css('display', 'none')
        }
        console.log($(this).val());
    })
});
