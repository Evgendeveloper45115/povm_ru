<?php

namespace app\assets;


use yii\web\AssetBundle;
use yii\web\JqueryAsset;
use yii\web\YiiAsset;
use yii\bootstrap\BootstrapAsset;

/**
 * Class Select2Asset
 * @package app\assets
 */
class Select2Asset extends AssetBundle
{
    public $sourcePath = '@bower/select2/dist';

    public $js = [
        'js/select2.js',
        'js/i18n/ru.js',
    ];

    public $css = [
        'css/select2.min.css',
    ];

    public $depends = [
        JqueryAsset::class,
    ];
}