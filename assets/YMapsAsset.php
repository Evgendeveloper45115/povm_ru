<?php


namespace app\assets;


use yii\web\AssetBundle;
use yii\web\YiiAsset;

class YMapsAsset extends AssetBundle
{
    public $js = [
        'https://api-maps.yandex.ru/2.1/?lang=ru_RU',
    ];

    public $depends = [
        YiiAsset::class
    ];
}