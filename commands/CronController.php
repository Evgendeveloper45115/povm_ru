<?php

namespace app\commands;

use app\companion\model\Trip;
use app\components\LinkPages;
use SitemapGenerator\SitemapGenerator;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\db\Query;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 * Проходит по поездкам и меняет им статус с активной на завершенную если дата поездки уже прошла
 * @since 2.0
 */
class CronController extends Controller
{

    public function actionAddArchiveTrip()
    {
        $time = new \DateTime('now');
        $today = $time->format('Y-m-d H:i:s');
        $result = \Yii::$app->db
            ->createCommand()
            ->update(Trip::tableName(),
                [
                    'status' => Trip::STATUS_FINISHED
                ],
                'status = ' . Trip::STATUS_ACTIVE . ' and trip_time <= :trip_time ', ['trip_time' => $today])
            ->execute();
        $this->stdout($result ? 'Done' : '0 rows' . "\n\n");

    }

    public function actionSiteMapGenerator()
    {
        $sitemapGenerator = new SitemapGenerator();
        $links = LinkPages::pages(\Yii::$app->urlManager->baseUrl);
        $sitemapGenerator->setSiteUrl(\Yii::$app->urlManager->baseUrl)
            ->setLinks($links)
            ->createSitemaps();
        $sitemapGenerator->saveAsFiles('public_html');
    }
}
