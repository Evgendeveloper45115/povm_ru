<?php

namespace app\commands;

use app\models\Dialog;
use app\models\DialogHasUser;
use app\models\GeoTown;
use Yii;
use yii\console\Controller;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;
use yii\helpers\VarDumper;

/**
 * Class ImportController
 * @package app\commands
 */
class ImportController extends Controller
{
    public $oldCountryTable = 'trip_country';
    public $oldRegionTable = 'trip_region';
    public $oldTownTable = '100_town';
    public $oldTownPhotoTable = 'trip_townphoto';
    public $countryTable = '{{%geo_country}}';
    public $regionTable = '{{%geo_region}}';
    public $townTable = '{{%geo_town}}';
    public $townPhotoTable = '{{%town_photo}}';

    public $oldUserTable = 'trip_user';
    public $userTable = '{{%user}}';
    public $oldUserPhotoTable = 'trip_userphoto';
    public $userPhotoTable = '{{%user_photo}}';

    public $oldCarTable = 'trip_car';
    public $carTable = '{{%car}}';
    public $carMarkTable = '{{%car_mark}}';
    public $carModelTable = '{{%car_model}}';
    public $oldCarPhotoTable = 'trip_carphoto';
    public $carPhotoTable = '{{%car_photo}}';

    public $oldTripTable = 'trip_trip';
    public $tripTable = '{{%trip}}';

    public $messageTable = '{{%message}}';
    public $oldMessagesTable = "trip_im";

    /** @var int лимит в запросах к большим таблицам */
    public $limit = 1000;

    /**
     * Импорт стран, регионов, городов, фоток городов
     *
     * @throws \yii\db\Exception
     */
    public function actionGeo()
    {
        // Страны
        $this->stdout("Truncate country table\n");
        $this->trancateTable($this->countryTable);

        $this->stdout("Importing countries\n");
        $list = (new Query())->from($this->oldCountryTable)->all(Yii::$app->db_old);
        $result = Yii::$app->db->createCommand()->batchInsert($this->countryTable, array_keys($list[0]), $list)->execute();
        $this->stdout("Done: $result\n\n");

        // Регионы
        $this->stdout("Truncate region table\n");
        Yii::$app->db->createCommand()->truncateTable($this->regionTable)->execute();

        $this->stdout("Importing regions\n");
        $list = (new Query())->from($this->oldRegionTable)->all(Yii::$app->db_old);

        $arr = [];
        foreach ($list as $item) {
            $arr[] = [
                'id' => $item['id'],
                'name' => $item['name'],
                'alias' => $item['alias'],
                'country_id' => $item['countryId'],
                'buy_price' => $item['buyPrice'],
                'sell_price' => $item['sellPrice'],
            ];
        }

        $result = Yii::$app->db->createCommand()->batchInsert($this->regionTable, array_keys($arr[0]), $arr)->execute();
        $this->stdout("Done: $result\n\n");

        // Города
        $this->stdout("Truncate town table\n");
        Yii::$app->db->createCommand()->truncateTable($this->townTable)->execute();

        $this->stdout("Importing towns\n");
        $list = (new Query())->from($this->oldTownTable)->all(Yii::$app->db_old);

        $arr = [];
        foreach ($list as $item) {
            $arr[] = [
                'id' => $item['id'],
                'name' => $item['name'],
                'alias' => $item['alias'],
                'country_id' => $item['countryId'],
                'region_id' => $item['regionId'],
                'description' => $item['description'],
                'size' => $item['size'],
                'seo_title' => $item['seoTitle'],
                'seo_description' => $item['seoDescription'],
                'seo_keywords' => $item['seoKeywords'],
                'photo' => $item['photo'],
                'lat' => $item['lat'],
                'lng' => $item['lng'],
                'is_capital' => $item['isCapital'],
            ];
        }

        $result = Yii::$app->db->createCommand()->batchInsert($this->townTable, array_keys($arr[0]), $arr)->execute();
        $this->stdout("Done: $result\n\n");

        // Фото городов
        $this->stdout("Truncate town_photo table\n");
        Yii::$app->db->createCommand()->truncateTable($this->townPhotoTable)->execute();

        $this->stdout("Importing town photos\n");
        $list = (new Query())->from($this->oldTownPhotoTable)->all(Yii::$app->db_old);

        $arr = [];
        foreach ($list as $item) {
            $arr[] = [
                'id' => $item['id'],
                'name' => $item['name'],
                'town_id' => $item['townId'],
                'main' => $item['avatar'],
            ];
        }

        $result = Yii::$app->db->createCommand()->batchInsert($this->townPhotoTable, array_keys($arr[0]), $arr)->execute();
        $this->stdout("Done: $result\n\n");
    }


    /**
     * Импорт таблицы поиска попутичков
     *
     * @throws \yii\db\Exception
     */
    public function actionTripSearchLog()
    {
        $this->stdout("Truncate search_log_trip table\n");
        $this->trancateTable('{{%search_log_trip}}');
        $this->stdout("Importing search_log_trip\n");
        $list = (new Query())->from('{{%trip_searchlog}}')
            ->select(['*, COUNT(*) AS counter'])
            ->groupBy(['townFrom', 'townTo'])
            ->all(Yii::$app->db_old);


        foreach ($list as $itemNumber => $row) {
            try {
                echo $itemNumber . PHP_EOL;
                $query = new Query();
                $query->from(GeoTown::tableName())
                    ->select('id')
                    ->where(['name' => $row['townFrom']]);
                $modelFrom = $query->one();
                $query->where(['name' => $row['townTo']]);
                $modelTo = $query->one();

                $result = Yii::$app->db
                    ->createCommand()
                    ->insert('{{%search_log_trip}}', [
                        'townFrom' => $row['townFrom'],
                        'townTo' => $row['townTo'],
                        'town_from_id' => isset($modelFrom['id']) ? $modelFrom['id'] : null,
                        'town_to_id' => isset($modelTo['id']) ? $modelTo['id'] : null,
                        'counter' => $row['counter']])
                    ->execute();
            } catch (\Exception $exception) {
            }
        }

        $this->stdout("Done: $itemNumber" . PHP_EOL);

    }


    /**
     * Импорт таблицы откликов
     *
     * @throws \yii\db\Exception
     */
    public function actionRespond()
    {
        $this->stdout("Truncate respond table\n");
        $this->trancateTable('{{%respond}}');
        $this->stdout("Importing trip_response\n");
        $list = (new Query())->from('{{%trip_response}}')
            ->select(['tripId', 'userId', 'comment'])
            ->all(Yii::$app->db_old);

        foreach ($list as $itemNumber => $row) {
            try {
                echo $itemNumber . PHP_EOL;
                Yii::$app->db
                    ->createCommand()
                    ->insert('{{%respond}}', [
                        'user_id' => $row['userId'],
                        'trip_id' => $row['tripId'],
                        'status' => true,
                        'comment' => $row['comment'],
                        'create_at' => date('Y-m-d H:i:s')])
                    ->execute();
            } catch (\Exception $exception) {
                VarDumper::dump($exception);
            }
        }

        $this->stdout("Done: $itemNumber" . PHP_EOL);

    }

    /**
     * Импорт юзеров, фоток юзеров
     *
     * @throws \yii\db\Exception
     */
    public function actionUsers()
    {
        // Юзеры
        $this->stdout("Truncate user table\n");
        $this->trancateTable($this->userTable);
        $this->stdout("Importing users\n");
        $towns = $this->getTowns();
        $done = 0;
        $offset = 0;
        $finish = false;
        $count = (new Query())->from($this->oldUserTable)->count('*', Yii::$app->db_old);

        Console::startProgress($done, $count);
        while (!$finish) {
            $list = (new Query())->from($this->oldUserTable)->distinct('email')->offset($offset)->limit($this->limit)->all(Yii::$app->db_old);

            $arr = [];
            foreach ($list as $item) {
                $key = array_search(mb_strtolower($item['town']), $towns);
                $townId = $key > 0 ? $key : null;
                $arr[] = [
                    'id' => $item['id'],
                    'name' => $item['name'],
                    'password_hash' => $item['password'],
                    'email' => $item['email'],
                    'confirm_code' => $item['confirm_code'],
                    'hash' => $item['hash'],
                    'login' => $item['login'],
                    'description' => $item['about'],
                    'gender' => $item['gender'],
                    'role' => $item['role'],
                    'rating' => $item['rating'],
                    'vk_id' => $item['vkId'],
                    'fb_id' => $item['fbId'],
                    'car' => $item['car'],
                    'phone' => $item['phone'],
                    'town_id' => $townId,
                    'email_access' => $item['emailAccess'],
                    'phone_access' => $item['phoneAccess'],
                    'created_at' => $item['regdate'],
                    'status' => $item['active'] == 1 ? 10 : 0,
                ];
            }

            $result = Yii::$app->db->createCommand()->batchInsert($this->userTable, array_keys($arr[0]), $arr)->execute();
            $done += $result;
            $offset += $result;
            if ($result < $this->limit) {
                $finish = true;
            }
            Console::updateProgress($done, $count);
        }

        Console::endProgress();

        // Фото юзеров
        $this->stdout("Truncate user_photo table\n");
        $this->trancateTable($this->userPhotoTable);
        $this->stdout("Importing user photos\n");
        $done = 0;
        $offset = 0;
        $finish = false;
        $count = (new Query())->from($this->oldUserPhotoTable)->count('*', Yii::$app->db_old);

        Console::startProgress($done, $count);
        while (!$finish) {
            $list = (new Query())->from($this->oldUserPhotoTable)->offset($offset)->limit($this->limit)->all(Yii::$app->db_old);

            $arr = [];
            foreach ($list as $item) {
                $arr[] = [
                    'id' => $item['id'],
                    'name' => $item['name'],
                    'user_id' => $item['userId'],
                    'main' => $item['avatar'],
                ];
            }

            $result = Yii::$app->db->createCommand()->batchInsert($this->userPhotoTable, array_keys($arr[0]), $arr)->execute();
            $done += $result;
            $offset += $result;
            if ($result < $this->limit) {
                $finish = true;
            }
            Console::updateProgress($done, $count);
        }

        Console::endProgress();
    }


    /**
     * Просто копирует сообщения из старой базы в новую
     * @throws \yii\db\Exception
     */
    public function actionMessages()
    {
        $this->stdout("Truncate message table\n");
        $this->trancateTable('{{%message}}');

        $oldMessages = (new Query())->from($this->oldMessagesTable)
            ->all(Yii::$app->db_old);

        $newMessages = [];
        foreach ($oldMessages as $message) {
            $newMessages[] = [
                'id' => (int)$message['id'],
                'messageFrom' => (int)$message['messageFrom'],
                'messageTo' => (int)$message['messageTo'],
                'text' => $message['text'],
                'viewed' => (int)$message['viewed'],
                'datetime' => $message['datetime'],
                'ip' => $message['ip'],
            ];
        }
        $messagesCount = sizeof($newMessages);

        foreach ($newMessages as $messageNumber => $message) {
            echo $messageNumber + 1 . '/' . $messagesCount . PHP_EOL;
            Yii::$app->db->createCommand()
                ->insert($this->messageTable, $message)
                ->execute();
        }
        echo 'Messages moved' . PHP_EOL;
    }

    /**
     * Создание диалогов и их привязка к пользователям и сообщениям
     * @throws \yii\db\Exception
     */
    public function actionDialogs()
    {
        $this->stdout("Truncate dialogs tables\n");
        $this->trancateTable('{{%dialog}}');
        $this->trancateTable('{{%dialog_has_user}}');

        $messagesGroupped = (new Query())->select(['messageFrom', 'messageTo', 'dialog_id'])
            ->from($this->messageTable)
            ->groupBy(['messageFrom', 'messageTo'])
            ->all();

        // ключи - комбинации ОТ и КОМУ, значения - id диалога
        $pairs = [];
        foreach ($messagesGroupped as $group) {
            $pairs[$group['messageFrom'] . '-' . $group['messageTo']] = 0;
        }

        foreach ($messagesGroupped as $group) {
            if (
            (
                $pairs[$group['messageFrom'] . '-' . $group['messageTo']] == 0 &&
                $pairs[$group['messageTo'] . '-' . $group['messageFrom']] == 0
            )
            ) {
                // Создаем диалог
                $dialog = new Dialog();
                if ($dialog->save()) {
                    $pairs[$group['messageFrom'] . '-' . $group['messageTo']] = $dialog->id;
                    $pairs[$group['messageTo'] . '-' . $group['messageFrom']] = $dialog->id;

                    $dialogPair1 = new DialogHasUser();
                    $dialogPair1->user_id = $group['messageFrom'];
                    $dialogPair1->dialog_id = $dialog->id;

                    $dialogPair2 = new DialogHasUser();
                    $dialogPair2->user_id = $group['messageTo'];
                    $dialogPair2->dialog_id = $dialog->id;

                    $dialogPair1->save();
                    $dialogPair2->save();

                    Yii::$app->db->createCommand()
                        ->update('{{%message}}', ['dialog_id' => $dialog->id], 'messageFrom = :from AND messageTo = :to', [
                            ':from' => $group['messageFrom'],
                            ':to' => $group['messageTo'],
                        ])
                        ->execute();

                    Yii::$app->db->createCommand()
                        ->update('{{%message}}', ['dialog_id' => $dialog->id], 'messageFrom = :from AND messageTo = :to', [
                            ':from' => $group['messageTo'],
                            ':to' => $group['messageFrom'],
                        ])
                        ->execute();

                    echo "Dialog #" . $dialog->id . ", users " . $group['messageFrom'] . ' and ' . $group['messageTo'] . PHP_EOL;
                }
            }
        }
    }

    /**
     * Импорт машин, фоток машин
     *
     * @throws \yii\db\Exception
     */
    public function actionCars()
    {
        // Машины
        $this->stdout("Truncate car table\n");
        $this->trancateTable($this->carTable);
        $this->stdout("Importing cars\n");
        $done = 0;
        $offset = 0;
        $finish = false;
        $count = (new Query())->from($this->oldCarTable)->count('*', Yii::$app->db_old);

        Console::startProgress($done, $count);
        while (!$finish) {
            $list = (new Query())->from($this->oldCarTable)->offset($offset)->limit($this->limit)->all(Yii::$app->db_old);

            $arr = [];
            foreach ($list as $item) {
                $arr[] = [
                    'id' => $item['id'],
                    'manufacturer' => $item['manufacturer'],
                    'model' => $item['model'],
                    'year' => $item['year'],
                    'user_id' => $item['ownerId'],
                    'pollution' => $item['pollution'],
                    'consumption' => $item['consumption'],
                    'name' => $item['nickname'],
                    'capacity' => $item['capacity'],
                    'description' => $item['description'],
                ];
            }

            $result = Yii::$app->db->createCommand()->batchInsert($this->carTable, array_keys($arr[0]), $arr)->execute();
            $done += $result;
            $offset += $result;
            if ($result < $this->limit) {
                $finish = true;
            }
            Console::updateProgress($done, $count);
        }

        Console::endProgress();

        // Фото машин
        $this->stdout("Truncate car_photo table\n");
        $this->trancateTable($this->carPhotoTable);
        $this->stdout("Importing car photos\n");
        $done = 0;
        $offset = 0;
        $finish = false;
        $count = (new Query())->from($this->oldCarPhotoTable)->count('*', Yii::$app->db_old);

        Console::startProgress($done, $count);
        while (!$finish) {
            $list = (new Query())->from($this->oldCarPhotoTable)->offset($offset)->limit($this->limit)->all(Yii::$app->db_old);

            $arr = [];
            foreach ($list as $item) {
                $arr[] = [
                    'id' => $item['id'],
                    'name' => $item['name'],
                    'car_id' => $item['carId'],
                    'main' => $item['avatar'],
                ];
            }

            $result = Yii::$app->db->createCommand()->batchInsert($this->carPhotoTable, array_keys($arr[0]), $arr)->execute();
            $done += $result;
            $offset += $result;
            if ($result < $this->limit) {
                $finish = true;
            }
            Console::updateProgress($done, $count);
        }

        Console::endProgress();
    }

    /**
     * Привязка марок и моделей к машинам
     *
     * @throws \yii\db\Exception
     */
    public function actionMapCars()
    {
        $this->stdout("Map cars\n");

        $done = 0;
        $cars = (new Query())->select(['id', 'manufacturer', 'model'])->from($this->carTable)->all();
        $count = count($cars);
        $total = $count;
        $updated = 0;

        $list = (new Query())->select(['id', 'name'])->from($this->carMarkTable)->all();
        $marks = ArrayHelper::map($list, 'id', 'name');

        $list = (new Query())->select(['id', 'mark_id', 'name'])->from($this->carModelTable)->all();
        $models = ArrayHelper::map($list, 'id', 'name', 'mark_id');

        unset($list);


        foreach ($cars as $car) {
            $markId = array_search($car['manufacturer'], $marks);
            if ($markId !== false) {
                $modelId = array_search($car['model'], $models[$markId]);
                if ($modelId !== false) {
                    $updated += Yii::$app->db->createCommand()->update($this->carTable, [
                        'mark_id' => $markId,
                        'model_id' => $modelId,
                        'manufacturer' => $marks[$markId],
                        'model' => $models[$markId][$modelId],
                    ], ['id' => $car['id']])->execute();
                }
            }

            $done++;
        }

        $this->stdout("Машин всего/обновлено: $total/$updated\n");
    }

    /**
     * Импорт поездок
     *
     * @throws \yii\db\Exception
     */
    public function actionTrips()
    {
        // Поездки
        $this->stdout("Truncate trip table\n");
        $this->trancateTable($this->tripTable);
        $this->stdout("Importing trips\n");
        $towns = $this->getTowns();
        $done = 0;
        $offset = 0;
        $finish = false;
        $count = (new Query())->from($this->oldTripTable)->count('*', Yii::$app->db_old);

        Console::startProgress($done, $count);
        while (!$finish) {
            $list = (new Query())->from($this->oldTripTable)->leftJoin('trip_tripviews', 'id=tripId')->offset($offset)->limit($this->limit)->all(Yii::$app->db_old);

            $arr = [];
            foreach ($list as $item) {
                $from = array_search(mb_strtolower($item['townFrom']), $towns);
                $to = array_search(mb_strtolower($item['townTo']), $towns);
                $fromId = $from > 0 ? $from : 0;
                $toId = $to > 0 ? $to : 0;

                $status = 0;
                if ($item['active'] == 1) {
                    $status = 10;
                } else if ($item['complete'] == 1) {
                    $status = 20;
                }

                if ($fromId == 0 || $toId == 0) {
                    $status = 0;
                }

                $arr[] = [
                    'id' => $item['id'],
                    'type' => $item['driver'] > 0 ? 2 : 1,
                    'user_id' => $item['autorId'],
                    'trip_time' => $item['date'],
                    'town_from' => $item['townFrom'],
                    'town_to' => $item['townTo'],
                    'from_id' => $fromId,
                    'to_id' => $toId,
                    'driver_id' => $item['driver'] > 0 ? $item['driver'] : null,
                    'passengers' => $item['passengers'],
                    'description' => $item['comment'],
                    'price' => $item['price'] < 1000000 ? $item['price'] : 0,
                    'periodic' => $item['periodic'],
                    'car_id' => $item['carId'],
                    'views' => $item['views'] ? $item['views'] : 0,
                    'status' => $status,
                    'created_at' => $item['created'],
                ];
            }

            $result = Yii::$app->db->createCommand()->batchInsert($this->tripTable, array_keys($arr[0]), $arr)->execute();
            $done += $result;
            $offset += $result;
            if ($result < $this->limit) {
                $finish = true;
            }
            Console::updateProgress($done, $count);
        }

        Console::endProgress();
    }

    /**
     * Корректирует поездки с нереальными датами
     * @throws \yii\db\Exception
     */
    public function actionIncorrectTrips()
    {
        $sqlJesusBirthday = "UPDATE {{%trip}} t
LEFT JOIN {{%user}} u ON t.user_id = u.id
SET t.trip_time = u.created_at
WHERE t.trip_time = '0000-00-00 00:00:00' AND t.id>0";

        Yii::$app->db->createCommand($sqlJesusBirthday)->execute();

        $sqlNoMonth = "DELETE FROM {{%trip}} WHERE MONTH(trip_time) = 0";
        Yii::$app->db->createCommand($sqlNoMonth)->execute();
    }

    /**
     * Массив городов id => name
     *
     * @return array
     */
    protected function getTowns()
    {
        $townsArr = (new Query())->select('id, TRIM(LOWER(name)) AS name')->from($this->townTable)->all();
        return ArrayHelper::map($townsArr, 'id', 'name');
    }

    /**
     * @param string $tableName
     * @throws \yii\db\Exception
     */
    private function trancateTable($tableName): void
    {
        Yii::$app->db->createCommand()->truncateTable($tableName)->execute();
    }
}