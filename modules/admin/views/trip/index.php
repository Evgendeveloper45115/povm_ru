<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\search\TripSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Trips';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trip-index">

    <h2>Поездки</h2>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'type',
                'value' => function (\app\companion\model\Trip $data) {
                    return $data::getTypeList()[$data->type];
                }
            ],
            [
                'attribute' => 'user_id',
                'value' => function (\app\companion\model\Trip $data) {
                    return $data->user->email;
                }
            ],
            [
                'attribute' => 'car_id',
                'value' => function (\app\companion\model\Trip $data) {
                    return $data->car->manufacturer;
                }
            ],
            [
                'attribute' => 'status',
                'value' => function (\app\companion\model\Trip $data) {
                    return $data::getStatusList()[$data->status];
                }
            ],
            'passengers',
            'trip_time',
            'town_from',
            'town_to',
            'description:ntext',
            'price',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
