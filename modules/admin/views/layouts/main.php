<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);

/** @var \app\auth\models\User $user */
$user = Yii::$app->user->identity;
    $menuItems = [
        [
            'label' => Html::img($user->avatar ? '/img/profile/' . $user->avatar : '/img/thumbs/user150.jpg', ['class' => 'user_avatar_menu', 'title' => 'Ваш профиль']),
            'url' => ['/profile/view'],
        ],
        [
            'label' => '<span class="glyphicon glyphicon-log-out"></span>',
            'url' => '/site/logout/',
            'linkOptions' => ['data-method' => 'post']
        ],
    ];
$this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= $this->title ?></title>
    <?php $this->head() ?>

</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap container">
    <div class="container">
        <h1>Панель управления проектом</h1>
    </div>
    <div class="">
        <?php NavBar::begin([
            'brandLabel' => 'Меню',
            'brandUrl' => '/admin/',
            'options' => [
                'class' => 'navbar-default container',
            ],
        ]); ?>
        <?php echo Nav::widget([
            'encodeLabels' => false,
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => \yii\helpers\ArrayHelper::merge([
                ['label' => '<i class="fa" aria-hidden="true"></i> Пользователи', 'url' => ['/admin/user/']],
                ['label' => '<i class="fa" aria-hidden="true"></i> Личные сообщения', 'url' => ['/admin/message/']],
                ['label' => '<i class="fa" aria-hidden="true"></i> Поездки ', 'url' => ['/admin/trip/']],
                ['label' => '<i class="fa" aria-hidden="true"></i> Рассылки  ', 'url' => ['/admin/mail/']],
            ], $menuItems)
        ]); ?>
        <?php NavBar::end(); ?>
    </div>

    <div class="container">
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
