<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\search\UserSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'password_hash') ?>

    <?= $form->field($model, 'email') ?>

    <?= $form->field($model, 'confirm_code') ?>

    <?php // echo $form->field($model, 'hash') ?>

    <?php // echo $form->field($model, 'login') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'gender') ?>

    <?php // echo $form->field($model, 'role') ?>

    <?php // echo $form->field($model, 'rating') ?>

    <?php // echo $form->field($model, 'vk_id') ?>

    <?php // echo $form->field($model, 'fb_id') ?>

    <?php // echo $form->field($model, 'car') ?>

    <?php // echo $form->field($model, 'phone') ?>

    <?php // echo $form->field($model, 'town_id') ?>

    <?php // echo $form->field($model, 'email_access') ?>

    <?php // echo $form->field($model, 'phone_access') ?>

    <?php // echo $form->field($model, 'subscribe') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'last_login') ?>

    <?php // echo $form->field($model, 'avatar') ?>

    <?php // echo $form->field($model, 'balance') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
