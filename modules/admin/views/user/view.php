<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\auth\models\User */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<!-- Просмотр конкретного пользователя -->

<div class="user-view">

    <h1>
        <?= Html::encode($this->title) ?>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'password_hash',
            'email:email',
            'confirm_code',
            'hash',
            'login',
            'description:ntext',
            'gender',
            'role',
            'rating',
            'vk_id',
            'fb_id',
            'car',
            'phone',
            'town_id',
            'email_access:email',
            'phone_access',
            'subscribe',
            'created_at',
            'status',
            'last_login',
            'avatar',
            'balance',
        ],
    ]) ?>

</div>
