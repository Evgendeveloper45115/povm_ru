<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h2>Пользователи</h2>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'name',
            // 'password_hash',
            'email:email',
            //  'confirm_code',
            //'hash',
            //'login',
            //'description:ntext',
            //'gender',
            'role',
            //'rating',
            //'vk_id',
            //'fb_id',
            //'car',
            'phone',
            [
                'attribute' => 'email_access',
                'value' => function (\app\auth\models\User $data) {
                    return $data->phoneAccess()[$data->email_access];
                }
            ],
            [
                'attribute' => 'phone_access',
                'value' => function (\app\auth\models\User $data) {
                    return $data->phoneAccess()[$data->phone_access];
                }
            ],

            [
                'attribute' => 'town_id',
                'value' => function (\app\auth\models\User $data) {
                    return $data->town->name;
                }
            ],
            [
                'attribute' => 'subscribe',
                'value' => function (\app\auth\models\User $data) {
                    return $data->subscribe ? 'Да' : 'Нет';
                }
            ],

            //'created_at',
            'status',
            //'last_login',
            //'avatar',
            'balance',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
