<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\search\MessageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Messages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="message-index">

    <h2>Личные сообщения</h2>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'messageFrom',
                'value' => function (\app\models\Message $data) {
                    return $data->fromUser->email;
                }
            ],
            [
                'attribute' => 'messageTo',
                'value' => function (\app\models\Message $data) {
                    return $data->toUser->email;
                }
            ],
            [
                'attribute' => 'viewed',
                'value' => function (\app\models\Message $data) {
                    return $data->viewed ? 'Да' : 'Нет';
                }
            ],
            'text:ntext',
            //'datetime',
            //'ip',
            //'dialog_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
