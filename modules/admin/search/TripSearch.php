<?php

namespace app\modules\admin\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\companion\model\Trip;

/**
 * TripSearch represents the model behind the search form of `app\companion\model\Trip`.
 */
class TripSearch extends Trip
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'type', 'user_id', 'from_id', 'to_id', 'driver_id', 'passengers', 'price', 'periodic', 'car_id', 'views', 'status'], 'integer'],
            [['trip_time', 'town_from', 'town_to', 'from_place', 'to_place', 'description', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Trip::find()->orderBy('id DESC');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'user_id' => $this->user_id,
            'trip_time' => $this->trip_time,
            'from_id' => $this->from_id,
            'to_id' => $this->to_id,
            'driver_id' => $this->driver_id,
            'passengers' => $this->passengers,
            'price' => $this->price,
            'periodic' => $this->periodic,
            'car_id' => $this->car_id,
            'views' => $this->views,
            'status' => $this->status,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'town_from', $this->town_from])
            ->andFilterWhere(['like', 'town_to', $this->town_to])
            ->andFilterWhere(['like', 'from_place', $this->from_place])
            ->andFilterWhere(['like', 'to_place', $this->to_place])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
