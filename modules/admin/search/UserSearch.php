<?php

namespace app\modules\admin\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\auth\models\User;

/**
 * UserSearch represents the model behind the search form of `app\auth\models\User`.
 */
class UserSearch extends User
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'gender', 'rating', 'vk_id', 'fb_id', 'town_id', 'email_access', 'phone_access', 'subscribe', 'status', 'balance'], 'integer'],
            [['name', 'password_hash', 'email', 'confirm_code', 'hash', 'login', 'description', 'role', 'car', 'phone', 'created_at', 'last_login', 'avatar'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find()->orderBy('id DESC');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'gender' => $this->gender,
            'rating' => $this->rating,
            'vk_id' => $this->vk_id,
            'fb_id' => $this->fb_id,
            'town_id' => $this->town_id,
            'email_access' => $this->email_access,
            'phone_access' => $this->phone_access,
            'subscribe' => $this->subscribe,
            'created_at' => $this->created_at,
            'status' => $this->status,
            'last_login' => $this->last_login,
            'balance' => $this->balance,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'confirm_code', $this->confirm_code])
            ->andFilterWhere(['like', 'hash', $this->hash])
            ->andFilterWhere(['like', 'login', $this->login])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'role', $this->role])
            ->andFilterWhere(['like', 'car', $this->car])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'avatar', $this->avatar]);

        return $dataProvider;
    }
}
