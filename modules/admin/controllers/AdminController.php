<?php

namespace app\modules\admin\controllers;

use app\auth\models\User;
use yii\web\Controller;


class AdminController extends Controller
{
    public function beforeAction($action)
    {
        $this->layout = 'main';
        if (\Yii::$app->user->isGuest || \Yii::$app->user->identity->role != User::ROLE_ADMIN) {
            \Yii::$app->session->setFlash('error', 'Отказ доступа');
            return $this->redirect('/site/login');
        }
        return parent::beforeAction($action);
    }

}
