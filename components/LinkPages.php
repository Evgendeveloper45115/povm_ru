<?php


namespace app\components;


use app\auth\models\User;
use app\companion\model\Trip;
use app\models\RouteQueryObject;
use yii\helpers\Url;
use yii\helpers\VarDumper;

class LinkPages
{
    public static function pages($siteUrl)
    {
        $links = [
            [
                'link' => $siteUrl . '/',
                'date' => date('Y-m-d'),
                'frequency' => 'weekly',
                'priority' => 0.9,
            ],
            [
                'link' => $siteUrl . '/vacation/',
                'date' => date('Y-m-d'),
                'frequency' => 'weekly',
                'priority' => 0.5,
            ],
        ];
        //Поездки начало//
        $sqlTrip = 'SELECT id FROM trip WHERE status IN (' . Trip::STATUS_ACTIVE . ',' . Trip::STATUS_FINISHED . ')';
        $trips = \Yii::$app->db->createCommand($sqlTrip)->queryAll();
        foreach ($trips as $model) {
            $links[] = [
                'link' => $siteUrl . '/trip/' . $model['id'] . '/',
                'date' => date('Y-m-d'),
                'frequency' => 'weekly',
                'priority' => 0.6,
            ];
        }
        //Поездки конец//
        //Страны начало//
        $sqlTown = '
SELECT geo_town.id,geo_town.alias,geo_country.alias as country_alias, geo_region.alias as region_alias
 FROM geo_town 
 LEFT JOIN geo_country ON geo_town.country_id = geo_country.id 
 LEFT JOIN geo_region ON geo_town.region_id = geo_region.id';

        $countries = \Yii::$app->db->createCommand($sqlTown)->queryAll();
        $urls = [];
        foreach ($countries as $model) {
            if (isset($urls[$model['country_alias']])) {
                continue;
            }
            $links[] = [
                'link' => $siteUrl . '/' . $model['country_alias'] . '/',
                'date' => date('Y-m-d'),
                'frequency' => 'weekly',
                'priority' => 0.5,
            ];
            $urls[$model['country_alias']] = $siteUrl . '/' . $model['country_alias'] . '/';
        }

        //Страны конец//
        //Регионы начало//
        foreach ($countries as $model) {
            if (isset($urls[$model['region_alias']])) {
                continue;
            }
            $links[] = [
                'link' => $siteUrl . '/' . $model['country_alias'] . '/' . $model['region_alias'] . '/',
                'date' => date('Y-m-d'),
                'frequency' => 'weekly',
                'priority' => 0.5,
            ];
            $urls[$model['region_alias']] = $siteUrl . '/' . $model['country_alias'] . '/' . $model['region_alias'] . '/';

        }
        //Регионы конец//
        //Города начало//
        foreach ($countries as $model) {
            $links[] = [
                'link' => $siteUrl . '/' . $model['country_alias'] . '/' . $model['region_alias'] . '/' . $model['alias'] . '/',
                'date' => date('Y-m-d'),
                'frequency' => 'weekly',
                'priority' => 0.5,
            ];
        }
        //Города конец//

        //Авто начало//
        $sqlCar = 'SELECT id FROM car';
        $cars = \Yii::$app->db->createCommand($sqlCar)->queryAll();
        foreach ($cars as $model) {
            $links[] = [
                'link' => $siteUrl . '/car/' . $model['id'] . '/',
                'date' => date('Y-m-d'),
                'frequency' => 'weekly',
                'priority' => 0.5,
            ];
        }
        //Авто конец//
        //Клиенты начало//
        $sqlUser = 'SELECT id,status FROM user WHERE status = ' . User::STATUS_ACTIVE;
        $users = \Yii::$app->db->createCommand($sqlUser)->queryAll();
        foreach ($users as $model) {
            $links[] = [
                'link' => $siteUrl . '/user/' . $model['id'] . '/',
                'date' => date('Y-m-d'),
                'frequency' => 'weekly',
                'priority' => 0.3,
            ];
        }
        //Клиенты конец//
        //Маршруты//
        $links[] = [
            'link' => $siteUrl . '/route/',
            'date' => date('Y-m-d'),
            'frequency' => 'weekly',
            'priority' => 0.5,
        ];

        //Маршруты//
        //Маршруты на букву и все страницы конкретных  маршрутов//
        $letters = RouteQueryObject::getFirstLettersOfTownsFrom();
        $trips = [];
        foreach ($letters as $letter) {
            $trips = RouteQueryObject::getRoutesByFirstLetter($letter);
            $trips[$letter] = RouteQueryObject::getRoutesByFirstLetter($letter);
            $links[] = ['link' => Url::to(['route/routes', 'letter' => $letter]),
                'date' => date('Y-m-d'),
                'frequency' => 'weekly',
                'priority' => 0.5,
            ];
            foreach ($trips[$letter] as $trip) {
                $links[] = [
                    'link' => Url::to(['/route/view', 'cityFrom' => $trip['from_alias'], 'cityTo' => $trip['to_alias']]),
                    'date' => date('Y-m-d'),
                    'frequency' => 'weekly',
                    'priority' => 0.5,
                ];
            }

        }
        //Маршруты на букву и все страницы конкретных  маршрутов//

        //site/action//

        $links[] = [
            'link' => $siteUrl . '/site/feedback/',
            'date' => date('Y-m-d'),
            'frequency' => 'weekly',
            'priority' => 0.5,
        ];
        $links[] = [
            'link' => $siteUrl . '/site/about/',
            'date' => date('Y-m-d'),
            'frequency' => 'weekly',
            'priority' => 0.5,
        ];
        $links[] = [
            'link' => $siteUrl . '/site/safety/',
            'date' => date('Y-m-d'),
            'frequency' => 'weekly',
            'priority' => 0.5,
        ];
        $links[] = [
            'link' => $siteUrl . '/site/rules/',
            'date' => date('Y-m-d'),
            'frequency' => 'weekly',
            'priority' => 0.5,
        ];
        $links[] = [
            'link' => $siteUrl . '/site/partners/',
            'date' => date('Y-m-d'),
            'frequency' => 'weekly',
            'priority' => 0.5,
        ];
        $links[] = [
            'link' => $siteUrl . '/site/offer/',
            'date' => date('Y-m-d'),
            'frequency' => 'weekly',
            'priority' => 0.5,
        ];
        //site/action//
        return $links;
    }

}