<?php

use yii\db\Migration;

/**
 * Class m190801_102023_add_car_number
 */
class m190801_102023_add_car_number extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%car}}', 'state_number', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190801_102023_add_car_number cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190801_102023_add_car_number cannot be reverted.\n";

        return false;
    }
    */
}
