<?php

use yii\db\Migration;

/**
 * Class m190911_114520_add_column_user
 */
class m190911_114520_add_column_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'name_carrier', $this->string()->defaultValue(null));
        $this->addColumn('{{%user}}', 'carrier_count_car', $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190911_114520_add_column_user cannot be reverted.\n";

        return false;
    }
}
