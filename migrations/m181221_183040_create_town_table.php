<?php

use yii\db\Migration;

/**
 * Handles the creation of table `town`.
 */
class m181221_183040_create_town_table extends Migration
{
    public $table = '{{%geo_town}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table, [
            'id'                => $this->primaryKey(),
            'name'              => $this->string()->notNull(),
            'alias'             => $this->string()->notNull(),
            'country_id'        => $this->integer()->notNull(),
            'region_id'         => $this->integer()->notNull(),
            'description'       => $this->text()->null(),
            'size'              => $this->integer()->notNull()->defaultValue(0),
            'seo_title'         => $this->string()->notNull()->defaultValue(''),
            'seo_description'   => $this->string()->notNull()->defaultValue(''),
            'seo_keywords'      => $this->string()->notNull()->defaultValue(''),
            'photo'             => $this->string()->notNull()->defaultValue(''),
            'lat'               => $this->decimal(9, 6)->null(),
            'lng'               => $this->decimal(9, 6)->null(),
            'is_capital'        => $this->tinyInteger()->notNull()->defaultValue(0),
        ], $tableOptions);

        $this->createIndex('town_country', $this->table, 'country_id');
        $this->createIndex('town_region', $this->table, 'region_id');
        $this->createIndex('town_alias', $this->table, 'alias');
        $this->createIndex('is_capital', $this->table, 'is_capital');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
