<?php

use yii\db\Migration;

/**
 * Class m190730_143516_add_user_avatar
 */
class m190730_143516_add_user_avatar extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'avatar', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190730_143516_add_user_avatar cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190730_143516_add_user_avatar cannot be reverted.\n";

        return false;
    }
    */
}
