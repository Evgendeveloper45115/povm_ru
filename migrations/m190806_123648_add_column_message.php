<?php

use yii\db\Migration;

/**
 * Class m190806_123648_add_column_message
 */
class m190806_123648_add_column_message extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%message}}', 'dialog_id', $this->integer()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190806_123648_add_column_message cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190806_123648_add_column_message cannot be reverted.\n";

        return false;
    }
    */
}
