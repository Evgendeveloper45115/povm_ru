<?php

use yii\db\Migration;

/**
 * Handles the creation of table `car`.
 */
class m181228_123256_create_car_table extends Migration
{
    public $table = '{{%car}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table, [
            'id'            => $this->primaryKey(),
            'mark_id'       => $this->integer()->null(),
            'model_id'      => $this->integer()->null(),
            'manufacturer'  => $this->string(50)->notNull(),
            'model'         => $this->string(50)->notNull(),
            'year'          => $this->integer()->notNull()->defaultValue(2008),
            'user_id'       => $this->integer()->notNull(),
            'pollution'     => $this->float()->notNull(),
            'consumption'   => $this->decimal(4, 1)->notNull(),
            'name'          => $this->string()->null(),
            'capacity'      => $this->tinyInteger(4)->null(),
            'description'   => $this->text(),
            'status'        => $this->smallInteger()->defaultValue(10),
        ], $tableOptions);

        $this->createIndex('mark', $this->table, 'mark_id');
        $this->createIndex('model', $this->table, 'model_id');
        $this->createIndex('user', $this->table, 'user_id');
        $this->createIndex('status', $this->table, 'status');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
