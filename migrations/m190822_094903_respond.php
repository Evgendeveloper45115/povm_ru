<?php

use yii\db\Migration;

/**
 * Class m190822_094903_respond
 */
class m190822_094903_respond extends Migration
{
    public $table = '{{%respond}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'trip_id' => $this->integer()->notNull(),
            'create_at' => $this->timestamp()->defaultValue(new \yii\db\Expression('current_timestamp()')),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190822_094903_respond cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190822_094903_respond cannot be reverted.\n";

        return false;
    }
    */
}
