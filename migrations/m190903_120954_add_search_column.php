<?php

use yii\db\Migration;

/**
 * Class m190903_120954_add_search_column
 */
class m190903_120954_add_search_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%search_log_trip}}', 'town_from_id', $this->integer()->defaultValue(null));
        $this->addColumn('{{%search_log_trip}}', 'town_to_id', $this->integer()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190903_120954_add_search_column cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190903_120954_add_search_column cannot be reverted.\n";

        return false;
    }
    */
}
