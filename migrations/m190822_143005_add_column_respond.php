<?php

use yii\db\Migration;

/**
 * Class m190822_143005_add_column_respond
 */
class m190822_143005_add_column_respond extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%respond}}', 'status', $this->integer()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190822_143005_add_column_respond cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190822_143005_add_column_respond cannot be reverted.\n";

        return false;
    }
    */
}
