<?php

use yii\db\Migration;

/**
 * Handles the creation of table `region`.
 */
class m181221_182534_create_region_table extends Migration
{
    public $table = '{{%geo_region}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table, [
            'id'         => $this->primaryKey(),
            'name'       => $this->string()->notNull(),
            'alias'      => $this->string()->notNull(),
            'country_id' => $this->integer()->notNull(),
            'buy_price'  => $this->integer()->notNull()->defaultValue(0),
            'sell_price' => $this->integer()->notNull()->defaultValue(0),
        ], $tableOptions);

        $this->createIndex('region_country', $this->table, 'country_id');
        $this->createIndex('region_alias', $this->table, 'alias');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
