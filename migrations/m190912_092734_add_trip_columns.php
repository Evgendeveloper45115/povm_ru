<?php

use yii\db\Migration;

/**
 * Class m190912_092734_add_trip_columns
 */
class m190912_092734_add_trip_columns extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%trip}}', 'carrier_route', $this->string()->defaultValue(0));
        $this->addColumn('{{%trip}}', 'route_active_to', $this->dateTime()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190912_092734_add_trip_columns cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190912_092734_add_trip_columns cannot be reverted.\n";

        return false;
    }
    */
}
