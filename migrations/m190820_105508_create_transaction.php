<?php

use yii\db\Migration;

/**
 * Class m190820_105508_create_transaction
 */
class m190820_105508_create_transaction extends Migration
{
    public $table = '{{%transaction}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'amount' => $this->integer()->notNull(),
            'create_at' => $this->timestamp()->defaultValue(new \yii\db\Expression('current_timestamp()')),
            'user_id' => $this->integer()->notNull(),
            'description' => $this->text()->defaultValue(null),
        ], $tableOptions);
        $this->addColumn('{{%user}}', 'balance', $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190820_105508_create_transaction cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190820_105508_create_transaction cannot be reverted.\n";

        return false;
    }
    */
}
