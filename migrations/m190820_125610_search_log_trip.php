<?php

use yii\db\Migration;

/**
 * Class m190820_125610_search_log_trip
 */
class m190820_125610_search_log_trip extends Migration
{
    public $table = '{{%search_log_trip}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'townFrom' => $this->string()->defaultValue(null),
            'townTo' => $this->string()->defaultValue(null),
            'counter' => $this->integer()->defaultValue(1),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190820_125610_search_log_trip cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190820_125610_search_log_trip cannot be reverted.\n";

        return false;
    }
    */
}
