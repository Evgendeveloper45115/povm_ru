<?php

use yii\db\Migration;

/**
 * Class m190806_101537_message_create
 */
class m190806_101537_message_create extends Migration
{
    public $table = '{{%message}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'messageFrom' => $this->integer()->notNull(),
            'messageTo' => $this->integer()->notNull(),
            'text' => $this->text()->notNull(),
            'viewed' => $this->integer()->defaultValue(0),
            'datetime' => $this->dateTime(),
            'ip' => $this->string()->defaultValue(0),
        ], $tableOptions);

        $this->createIndex('user', $this->table, 'messageFrom');
        $this->createIndex('sender', $this->table, 'messageTo');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190806_101537_message_create cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190806_101537_message_create cannot be reverted.\n";

        return false;
    }
    */
}
