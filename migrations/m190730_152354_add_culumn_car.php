<?php

use yii\db\Migration;

/**
 * Class m190730_152354_add_culumn_car
 */
class m190730_152354_add_culumn_car extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%car}}', 'avatar', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190730_152354_add_culumn_car cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190730_152354_add_culumn_car cannot be reverted.\n";

        return false;
    }
    */
}
