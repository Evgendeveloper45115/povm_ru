<?php

use yii\db\Migration;

/**
 * Handles the creation of table `country`.
 */
class m181221_182314_create_country_table extends Migration
{
    public $table = '{{%geo_country}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table, [
            'id'    => $this->primaryKey(),
            'name'  => $this->string()-> notNull(),
            'alias' => $this->string()->notNull(),
        ], $tableOptions);

        $this->createIndex('country_alias', $this->table, 'alias');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
