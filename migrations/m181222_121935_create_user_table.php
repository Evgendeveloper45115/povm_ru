<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m181222_121935_create_user_table extends Migration
{
    public $table = '{{%user}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table, [
            'id'            => $this->primaryKey(),
            'name'          => $this->string()->notNull(),
            'password_hash' => $this->string()->notNull(),
            'email'         => $this->string()->notNull(),
            'confirm_code'  => $this->string()->null(),
            'hash'          => $this->string()->null(),
            'login'         => $this->string(30),
            'description'   => $this->text(),
            'gender'        => $this->tinyInteger()->null(),
            'role'          => $this->string(30)->notNull()->defaultValue('user'),
            'rating'        => $this->integer()->notNull()->defaultValue(0),
            'vk_id'         => $this->integer()->null(),
            'fb_id'         => $this->integer()->null(),
            'car'           => $this->string(50)->null(),
            'phone'         => $this->string(16),
            'town_id'       => $this->integer()->null(),
            'email_access'  => $this->tinyInteger()->notNull()->defaultValue(0),
            'phone_access'  => $this->tinyInteger()->notNull()->defaultValue(0),
            'subscribe'     => $this->tinyInteger()->notNull()->defaultValue(1),
            'created_at'    => $this->timestamp()->defaultValue(new \yii\db\Expression('current_timestamp()')),
            'status'        => $this->tinyInteger()->defaultValue(0),
            'last_login'    => $this->dateTime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('town', $this->table, 'town_id');
        $this->createIndex('status', $this->table, 'status');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
