<?php

use yii\db\Migration;

/**
 * Handles the creation of table `town_photo`.
 */
class m181228_095921_create_town_photo_table extends Migration
{
    public $table = 'town_photo';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table, [
            'id'        => $this->primaryKey(),
            'name'      => $this->string(60)->notNull(),
            'town_id'   => $this->integer(),
            'main'      => $this->tinyInteger()->defaultValue(0),
        ], $tableOptions);

        $this->createIndex('town', $this->table, 'town_id');
        $this->createIndex('main', $this->table, 'main');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
