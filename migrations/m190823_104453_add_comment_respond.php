<?php

use yii\db\Migration;

/**
 * Class m190823_104453_add_comment_respond
 */
class m190823_104453_add_comment_respond extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%respond}}', 'comment', $this->text()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190823_104453_add_comment_respond cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190823_104453_add_comment_respond cannot be reverted.\n";

        return false;
    }
    */
}
