<?php

namespace app\auth\services;

use app\auth\forms\RegistrationForm;
use app\auth\models\User;
use Yii;
use yii\base\Exception;

class SignUpService
{
    /**
     * @param User $user
     * @return User
     */
    public function confirm(User $user): User
    {
        $user->confirmSignup();
        return $user;
    }

    /**
     * @param RegistrationForm $form
     * @return User
     * @throws Exception
     */
    public function createUser(RegistrationForm $form): User
    {
        $user = new User();
        $user->name = $form->name;
        $user->email = $form->email;
        $user->phone = $form->phone;
        $user->password_hash = Yii::$app->security->generatePasswordHash($form->password);
        $user->confirm_code = Yii::$app->security->generateRandomString(64);
        $user->status = User::STATUS_INACTIVE;
        return $user;
    }
}
