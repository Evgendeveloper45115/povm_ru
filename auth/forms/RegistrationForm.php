<?php

namespace app\auth\forms;

use app\auth\models\User;
use yii\base\Model;

class RegistrationForm extends Model
{
    /** @var string */
    public $name;

    /** @var string */
    public $email;

    /** @var string */
    public $password;

    /** @var string */
    public $phone;
    public $phone_access;
    public $town_id;
    public $comment;
    public $name_carrier;
    public $carrier_count_car;
    public $role;

    /** @var User */
    private $_user;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['email', 'password', 'phone', 'name'], 'required'],
            [['email', 'password', 'phone', 'name', 'town_id', 'phone_access', 'role'], 'required', 'on' => User::ROLE_USER],
            [['email', 'password', 'phone', 'name', 'town_id', 'phone_access', 'name_carrier', 'carrier_count_car', 'role'], 'required', 'on' => User::ROLE_CARRIER],
            [['email'], 'email'],
            [['email'], 'unique', 'targetClass' => User::class, 'targetAttribute' => 'email'],
            [['password', 'phone'], 'string', 'min' => 6],
            [['name'], 'string', 'min' => 3],
            [['comment', 'name_carrier', 'carrier_count_car', 'town_id', 'phone_access', 'role'], 'safe'],
        ];
    }

    /*public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[User::ROLE_CARRIER] = ['orig_password', 'new_password', 'password1'];
        return $scenarios;
    }*/

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => 'E-mail',
            'password' => 'Пароль',
            'phone' => 'Телефон',
            'name' => 'Ваше имя',
            'comment' => 'Комментарий к отклику',
            'name_carrier' => 'Название перевозчика',
            'carrier_count_car' => 'К-тво транспорта в автопарке',
        ];
    }

    /**
     * @inheritDoc
     */
    public function attributeHints()
    {
        return [
            'e-mail' => 'E-mail',
            'password' => 'Минимум 6 символов',
        ];
    }

    public function phoneAccess()
    {
        return [
            'Всем',
            'Авторизованным пользователям',
            'Никому'
        ];
    }

    public function addAttributes()
    {
        $user = new User();
        $user->load($this->attributes);
        foreach ($this->attributes as $name => $attribute) {
            if ($user->hasAttribute($name)) {
                $user->setAttribute($name, $attribute);
            }
        }
        $user->password_hash = \Yii::$app->security->generatePasswordHash($this->password);
        $user->confirm_code = \Yii::$app->security->generateRandomString(64);
        $user->status = User::STATUS_INACTIVE;
        return $user;
    }
}
