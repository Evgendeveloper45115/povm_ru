<?php

namespace app\auth\repositories;

use app\auth\models\User;
use app\components\Dumper;
use app\repositories\NotFoundException;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\VarDumper;

class UserRepository
{
    public function get($id): User
    {
        return $this->getBy(['id' => $id]);
    }

    public function getByConfirmCode($token): User
    {
        return $this->getBy(['confirm_code' => $token]);
    }

    public function getByEmail(string $email): User
    {
        return $this->getBy(['email' => $email]);
    }

    public function checkByEmail(string $email): bool
    {
        return User::find()->where(['email' => $email])->exists();
    }

    public function checkById($id): bool
    {
        return User::find()->where(['id' => $id])->exists();
    }

    public function save(User $user): void
    {
        if (!$user->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        return User::find()->all();
    }

    /**
     * @param User $user
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function remove(User $user): void
    {
        if (!$user->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }

    /**
     * @param array $condition
     * @return User|ActiveRecord
     */
    private function getBy(array $condition): User
    {
        if (!$user = User::find()->andWhere($condition)->limit(1)->one()) {
            throw new NotFoundException('Пользователь не найден');
        }
        return $user;
    }

    public function query()
    {
        return new ActiveQuery(User::class);
    }
}
