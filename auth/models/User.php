<?php

namespace app\auth\models;

use app\companion\model\Trip;
use app\companion\model\Car;
use app\models\GeoTown;
use app\models\Message;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsTrait;
use Yii;
use yii\base\InvalidArgumentException;
use yii\db\ActiveQuery;
use yii\helpers\VarDumper;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $name
 * @property string $password_hash
 * @property string $email
 * @property string $confirm_code
 * @property string $hash
 * @property string $login
 * @property string $description
 * @property int $gender
 * @property string $role
 * @property int $rating
 * @property int $vk_id
 * @property int $fb_id
 * @property string $car
 * @property string $phone
 * @property string $avatar
 * @property int $town_id
 * @property int $phone_access
 * @property int $email_access
 * @property int $subscribe
 * @property string $created_at
 * @property int $status
 * @property int $carrier_count_car
 * @property string $balance
 * @property string $last_login
 * @property string $name_carrier
 *
 * @property string $authKey
 *
 * @property GeoTown $town
 * @property Car[] $cars
 * @property Trip[] $trips
 * @property Trip[] $inactiveTrips
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    use SaveRelationsTrait;

// статусы пользователей
    public const STATUS_INACTIVE = 0;
    public const STATUS_ACTIVE = 10;
    public const STATUS_BANNED = 20;
    public const STATUS_DELETED = -10;

// кому виден номер телефона в профиле
    public const ACCESS_NONE = 0;
    public const ACCESS_REGISTERED = 1;
    public const ACCESS_ALL = 2;

// подписан или не подписан на рассылки
    public const SUBSCRIBE_INACTIVE = 0;
    public const SUBSCRIBE_ACTIVE = 1;

//роли пользователей
    const ROLE_USER = 'user';
    const ROLE_ADMIN = 'admin';
    const ROLE_CARRIER = 'carrier';

//пол пользователя
    public const GENDER_MALE = 0;
    public const GENDER_FEMALE = 1;

    const SCENARIO_CHANGE_PASSWORD_USER = 'change_password_user';

    public $orig_password;
    public $new_password;
    public $password1;

    public static $roles = [
        self::ROLE_USER => 'Попутчик',
        self::ROLE_CARRIER => 'Коммерческий перевозчик',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'password_hash', 'email'], 'required'],
            [['email'], 'unique'],
            [['description', 'avatar'], 'string'],
            [['gender', 'rating', 'vk_id', 'fb_id', 'town_id', 'status'], 'integer'],
            [['created_at', 'last_login', 'name_carrier', 'carrier_count_car'], 'safe'],
            [['name', 'password_hash', 'email', 'confirm_code', 'hash'], 'string', 'max' => 255],
            [['login', 'role'], 'string', 'max' => 30],
            [['car'], 'string', 'max' => 50],
            [['phone'], 'string', 'max' => 16],
            [['phone_access', 'email_access'], 'in', 'range' => [static::ACCESS_NONE, static::ACCESS_REGISTERED, static::ACCESS_ALL]],
            [['subscribe'], 'in', 'range' => [static::SUBSCRIBE_INACTIVE, static::SUBSCRIBE_ACTIVE]],
            [['cars', 'trips', 'inactiveTrips', 'balance'], 'safe'],
            ['password1', 'compare',
                'compareAttribute' => 'new_password',
                'on' => self::SCENARIO_CHANGE_PASSWORD_USER,
                'message' => 'Пароли не совпадают'
            ],
            ['orig_password', 'myCompare', 'on' => self::SCENARIO_CHANGE_PASSWORD_USER],
            [['orig_password', 'new_password', 'password1'], 'required', 'on' => self::SCENARIO_CHANGE_PASSWORD_USER, 'message' => 'Заполните данные!'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CHANGE_PASSWORD_USER] = ['orig_password', 'new_password', 'password1'];
        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'password_hash' => 'Password Hash',
            'email' => 'Email',
            'confirm_code' => 'Confirm Code',
            'hash' => 'Hash',
            'login' => 'Login',
            'description' => 'О себе',
            'gender' => 'Пол',
            'role' => 'Role',
            'rating' => 'Rating',
            'vk_id' => 'Vk ID',
            'fb_id' => 'Fb ID',
            'car' => 'Car',
            'phone' => 'Номер телефона',
            'town_id' => 'Город проживания',
            'created_at' => 'Created At',
            'status' => 'Status',
            'last_login' => 'Last Login',
            'avatar' => 'Avater',
            'email_access' => 'Кому виден емейл',
            'phone_access' => 'Кому виден телефон',
            'subscribe' => 'Получать рассылки',
            'balance' => 'Баланс'
        ];
    }

    public function phoneAccess()
    {
        return [
            'Всем',
            'Авторизованным пользователям',
            'Никому'
        ];
    }

    public function getTownName()
    {
        return GeoTown::findOne($this->town_id)->name;
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function myCompare($attribute, $params)
    {
        if (!$this->validatePassword($this->orig_password)) {
            $this->addError($attribute, 'Старый пароль не совпадает');
        }
    }

    /**
     * @param $email
     * @return User|array|\yii\db\ActiveRecord|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => static::STATUS_ACTIVE]);
    }

    /**
     * @param $password
     * @return bool
     */
    public function validatePassword($password)
    {

        // Старый вариант Yii1
        $salt = substr($this->password_hash, 0, 64); //get the salt from the front of the hash
        $validHash = substr($this->password_hash, 64, 64); //the SHA256

        $testHash = hash('sha256', $salt . $password); //hash the password being tested

        //if the hashes are exactly the same, the password is valid
        if ($testHash === $validHash) {
            return true;
        }

        // Новый вариант Yii2
        $result = false;
        try {
            $result = Yii::$app->security->validatePassword($password, $this->password_hash);
        } catch (InvalidArgumentException$e) {
            return false;
        }
        return $result;
    }

    public function confirmSignup(): void
    {
        $this->status = self::STATUS_ACTIVE;
        $this->confirm_code = null;
    }

    public function isActive(): bool
    {
        return $this->status === self::STATUS_ACTIVE;
    }

    public function isCanCreateTrip(): bool
    {
        $time = (new \DateTime('now'))->modify('-1 day')->format('Y-m-d H:i:s');
        $count = self::getTrips()->andWhere(['>', 'created_at', $time])->count();

        return $count = $count < 3 ? true : false;
    }

    public function addTrip(Trip $trip)
    {
        $trips = $this->trips;
        $trips[] = $trip;
        $this->trips = $trips;
    }

    public function addCar(Car $car)
    {
        $cars = $this->cars;
        $cars[] = $car;
        $this->cars = $cars;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'saveRelations' => [
                'class' => SaveRelationsBehavior::class,
                'relations' => [
                    'cars',
                    'trips',
                    'inactiveTrips',
                ],
            ],
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * Finds an identity by the given ID.
     * @param string|int $id the ID to be looked for
     * @return IdentityInterface the identity object that matches the given ID.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds an identity by the given token.
     * @param mixed $token the token to be looked for
     * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
     * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     * @return IdentityInterface the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        // TODO: Implement findIdentityByAccessToken() method.
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|int an ID that uniquely identifies a user identity.
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey()
    {
        // TODO: Implement getAuthKey() method.
    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @param string $authKey the given auth key
     * @return bool whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey)
    {
        // TODO: Implement validateAuthKey() method.
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTown()
    {
        return $this->hasOne(GeoTown::class, ['id' => 'town_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCars()
    {
        return $this->hasMany(\app\companion\model\Car::class, ['user_id' => 'id'])->where(['status' => Car::STATUS_ACTIVE]);
    }

    public function getTrips(): ActiveQuery
    {
        return $this->hasMany(Trip::class, ['user_id' => 'id']);
    }

    public function getMessages(): ActiveQuery
    {
        return $this->hasMany(Message::class, ['messageTo' => 'id'])->andWhere(['viewed' => 0]);
    }

    public function getInactiveTrips(): ActiveQuery
    {
        return $this->hasMany(Trip::class, ['user_id' => 'id'])->where(['status' => Trip::STATUS_INACTIVE]);
    }
}
