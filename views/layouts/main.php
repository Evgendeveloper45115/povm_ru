<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);

/** @var \app\auth\models\User $user */
$user = Yii::$app->user->identity;
$cnt = 0;
if ($user->messages) {
    $cnt = count($user->messages);
}
$menuItems = [];
if (Yii::$app->user->isGuest) {
    $menuItems = [
        [
            'label' => '<i class="fa fa-user-plus" aria-hidden="true"></i> Регистрация',
            'url' => ['/site/register']
        ],
        [
            'label' => '<i class="fa fa-user" aria-hidden="true"></i> Вход',
            'url' => ['/site/login']
        ],
    ];
} else {
    $menuItems = [
        [
            'label' => '<i class="fa fa-road" aria-hidden="true"></i> Мои поездки',
            'url' => ['/trip/my']
        ],
        [
            'label' => '<i class="fa fa-comments" aria-hidden="true"></i> Сообщения ' . Html::tag('span', $cnt, ['class' => 'badge']),
            'url' => ['/dialog/']
        ],
        [
            'label' => Html::img($user->avatar ? '/img/profile/' . $user->avatar : '/img/thumbs/user150.jpg', ['class' => 'user_avatar_menu', 'title' => 'Ваш профиль']),
            'url' => ['/profile/view'],
        ],
        [
            'label' => '<span class="glyphicon glyphicon-log-out"></span>',
            'url' => '/site/logout/',
            'linkOptions' => ['data-method' => 'post']
        ],
    ];
}
$this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php
    if (Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'index') {
        $this->registerLinkTag(['rel' => 'canonical', 'href' => \yii\helpers\Url::to('/', true)]);
    } else {
        $this->registerLinkTag(['rel' => 'canonical', 'href' => \yii\helpers\Url::canonical()]);
    }
    ?>
    <?= Html::csrfMetaTags() ?>
    <title><?= $this->title ?></title>
    <?php $this->head() ?>
    <!--<script src="https://vk.com/js/api/openapi.js?160" type="text/javascript"></script>-->

</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap container">
    <div class="container">
        <div class="big-banner text-center">
            <a href="/"><img src="/img/logo_bar.png"></a>
            <h4 class="vert-margin20">онлайн сервис поиска попутчиков</h4>

            <p class="home-title vert-margin10 hidden-xs">Уже более 100000 человек из России и СНГ используют наш сайт
                для поиска попутчиков в
                самые разные поездки и путешествия</p>

            <div class="row text-center">
                <div class="col-md-3 hidden-xs">
                    <div class="puzomerka">102 756</div>
                    попучиков
                </div>
                <div class="col-md-3 hidden-xs">
                    <div class="puzomerka">160 678</div>
                    создано поездок
                </div>
                <div class="col-md-3 hidden-xs">
                    <div class="puzomerka">45</div>
                    создается поездок в день
                </div>
                <div class="col-md-3">
                    <?= Html::a('Найти попутчика', ['/trip/start'], [
                        'class' => 'btn btn-default btn-lg btn-block',
                    ]); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="">
        <?php NavBar::begin([
            'brandLabel' => 'Меню',
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'navbar-default container',
            ],
        ]); ?>
        <?php echo Nav::widget([
            'encodeLabels' => false,
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => \yii\helpers\ArrayHelper::merge([
                // ['label' => 'Создать поездку', 'url' => ['/trip/start']],
                ['label' => '<i class="fa fa-car" aria-hidden="true"></i> Межгород на автомобиле', 'url' => ['/trip/']],
                ['label' => '<i class="fa fa-plane" aria-hidden="true"></i> В отпуск заграницу', 'url' => ['/vacation/']],
            ], $menuItems)
        ]); ?>
        <?php NavBar::end(); ?>
    </div>

    <div class="container">
        <?= Breadcrumbs::widget([
            'encodeLabels' => false,
            'homeLink' => [
                'label' => 'Попутчики',
                'url' => '/',
            ],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-sm-2">
                <strong>Попутчики.Ру</strong>
                <ul>
                    <li> info@povm.ru<br/>
                        2012 ©
                    </li>
                </ul>
            </div>
            <div class="col-md-2 col-sm-2">
                <strong>Мы в соцсетях</strong>
                <ul>
                    <noindex>
                        <li><a href="https://vk.com/sto_yuristov" rel="nofollow" target="_blank">Вконтакте</a></li>
                        <li><a href="https://www.youtube.com/channel/UCBUQAbVFrpy7CYa83cCTHkw"
                               rel="nofollow" target="_blank">YouTube</a></li>
                    </noindex>
                </ul>
            </div>
            <div class="col-md-2 col-sm-2">
                <strong>О сайте</strong>
                <ul>
                    <li><?= Html::a('Автопарк проекта', ['/auto/index']); ?></li>
                    <li><?= Html::a('Техника безопасности', ['/site/safety']); ?></li>
                    <li><?= Html::a('Пользовательское соглашение', ['/site/offer']); ?></li>
                </ul>
            </div>
            <div class="col-md-2 col-sm-2">
                <strong>Информация</strong>
                <ul>
                    <li><?= Html::a('Как это работает', ['/site/about']); ?></li>
                    <li><?= Html::a('Правила сервиса', ['/site/rules']); ?></li>
                    <li><?= Html::a('Обратная связь', ['/site/feedback']); ?></li>
                    <li><?= Html::a('Наши партнеры', ['/site/partners']); ?></li>
                </ul>
            </div>
            <div class="col-md-2 col-sm-2">
                <strong>География</strong>
                <?= \app\widgets\CountryWidget::widget(); ?>
            </div>
            <div class="col-md-2 col-sm-2">
                <strong>Сервисы</strong>
                <ul>
                    <li><?= Html::a('Маршруты', ['/route/']); ?></li>
                    <li> <?= Html::a('Расхода топлива', ['/fuel-calculator/']); ?></li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
