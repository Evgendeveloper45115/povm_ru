<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Калькулятор расхода топллива. Расход топлива на 100 км.';
$this->params['breadcrumbs'][] = $this->title;
?>
    <h1 class="vert-margin40">Калькулятор расхода топлива автомобилей</h1>
    <div class="row">
        <div class="col-sm-6">
            <h2 class="center-align vert-margin20">Калькулятор расхода топлива на расстояние</h2>
            <p class="center-align">Если вы планируете поездку и вам нужно расчитать объём и стоимость, необходимого
                топливана весь путь или его часть.</p>
            <form class="form-horizontal">
                <div class="form-group">
                    <label for="distance" class="col-sm-6 control-label">Укажите расстояние</label>
                    <div class="col-sm-6">
                        <div class="input-group">
                            <?= Html::textInput('distance', 0, [
                                'id' => 'distance',
                                'class' => 'form-control',
                            ]); ?>
                            <span class="input-group-addon">км</span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="consumption" class="col-sm-6 control-label">Средний расход топлива</label>
                    <div class="col-sm-6">
                        <div class="input-group">
                            <?= Html::textInput('consumption', 0, [
                                'id' => 'consumption',
                                'class' => 'form-control',
                            ]); ?>
                            <span class="input-group-addon">л / 100 км</span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="price" class="col-sm-6 control-label">Стоимость топлива за л.</label>
                    <div class="col-sm-6">
                        <div class="input-group">
                            <?= Html::textInput('price', 0, [
                                'id' => 'price',
                                'class' => 'form-control',
                            ]); ?>
                            <span class="input-group-addon">р/л</span>
                        </div>
                    </div>
                </div>

                <hr/>

                <div class="col-sm-12">
                    <div class="right-align">
                        <button type="button" class="btn btn-lg btn-default">Рассчитать расход</button>
                    </div>
                </div>
                <div class="col-sm-12">
                    <h4 class="vert-margin30">Результаты расчёта:</h4>
                </div>

                <div class="form-group">
                    <label for="total-fuel" class="col-sm-6 control-label">Итого расход топлива</label>
                    <div class="col-sm-6">
                        <div class="input-group">
                            <?= Html::textInput('total-fuel', 0, [
                                'id' => 'total-fuel',
                                'class' => 'form-control',
                            ]); ?>
                            <span class="input-group-addon">л / 500 км</span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="total-price" class="col-sm-6 control-label">Итого стоимость</label>
                    <div class="col-sm-6">
                        <div class="input-group">
                            <?= Html::textInput('total-price', 0, [
                                'id' => 'total-price',
                                'class' => 'form-control',
                            ]); ?>
                            <span class="input-group-addon">руб</span>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-sm-6">
            <h2 class="center-align vert-margin20">Расход топлива на 100 км</h2>
            <p class="center-align">Если вам необходимо выяснить средний расход топлива вашего автомобиля, по факту
                совершения поездки</p>
            <form class="form-horizontal">
                <div class="form-group">
                    <label for="distance" class="col-sm-6 control-label">Пройденное расстояние</label>
                    <div class="col-sm-6">
                        <div class="input-group">
                            <?= Html::textInput('distance', 0, [
                                'id' => 'distance',
                                'class' => 'form-control',
                            ]); ?>
                            <span class="input-group-addon">км</span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="total-fuel" class="col-sm-6 control-label">Израсходовано топлива</label>
                    <div class="col-sm-6">
                        <div class="input-group">
                            <?= Html::textInput('total-fuel', 0, [
                                'id' => 'total-fuel',
                                'class' => 'form-control',
                            ]); ?>
                            <span class="input-group-addon">л.</span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="price" class="col-sm-6 control-label">Стоимость топлива за л.</label>
                    <div class="col-sm-6">
                        <div class="input-group">
                            <?= Html::textInput('price', 0, [
                                'id' => 'price',
                                'class' => 'form-control',
                            ]); ?>
                            <span class="input-group-addon">р/л</span>
                        </div>
                    </div>
                </div>

                <hr/>
                <div class="col-sm-12">
                    <div class="right-align">
                        <button type="button" class="btn btn-lg btn-default">Рассчитать расход</button>
                    </div>
                </div>
                <div class="col-sm-12">
                    <h4 class="vert-margin30">Результаты расчёта:</h4>
                </div>
                <div class="form-group">
                    <label for="consumption" class="col-sm-6 control-label">Средний расход топлива</label>
                    <div class="col-sm-6">
                        <div class="input-group">
                            <?= Html::textInput('consumption', 0, [
                                'id' => 'consumption',
                                'class' => 'form-control',
                            ]); ?>
                            <span class="input-group-addon">л / 100 км</span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="total-price" class="col-sm-6 control-label">Средняя стоимость</label>
                    <div class="col-sm-6">
                        <div class="input-group">
                            <?= Html::textInput('total-price', 0, [
                                'id' => 'total-price',
                                'class' => 'form-control',
                            ]); ?>
                            <span class="input-group-addon">руб./ 1 км</span>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

<?php $this->registerJs(
    <<<JS


JS
);