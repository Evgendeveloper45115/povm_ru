<?php

use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var array $letters
 * @var array $trips
 */
$this->title = "Все марштуры - поиск попутчкиков на автомобиле";

$this->registerMetaTag(['name' => 'description',
    'content' => 'Всем маршруты по которым люди ищут себе попутчиков, выберите тот который вам нужен.']);

$this->params['breadcrumbs'][] = [
    'label' => 'Попутчики по маршруту'
];
?>

<h1>Маршруты</h1>

<?php
foreach ($letters as $key => $letter) {
    if ($key % 3 == 0): ?>
        <div class="row">
    <?php endif; ?>
    <div class="col-sm-4 vert-margin20">
        <?php if (isset($trips[$letter])): ?>
            <p>
                <?php foreach ($trips[$letter] as $trip): ?>
                    <?php echo Html::a($trip['from_name'] . ' - ' . $trip['to_name'], Url::to([
                        'route/view',
                        'cityFrom' => $trip['from_alias'],
                        'cityTo' => $trip['to_alias']
                    ])); ?>
                    <br/>
                <?php endforeach; ?>
            </p>
        <?php endif; ?>

        <strong>
            <?= Html::a(
                "Все маршруты на " . $letter,
                Url::to(['route/routes', 'letter' => $letter])
            ) ?>
        </strong>
    </div>
    <?php if ($key % 3 == 2): ?>
        </div>
    <?php endif; ?>
    <?php
}
?>

<?php if ($key % 3 != 2): ?>
    </div>
<?php endif; ?>

