<?php
/**
 * @var $trips \app\models\SearchLogTrip[]
 */
$this->title = "Маршруты на букву " . $letter . " поиск попутчкиков на автомобиле";
$this->registerMetaTag(['name' => 'description',
    'content' => "Все маршруты города отправлния которых начинаются на букву $letter  - попутчкиков на автомобиле"]);

$this->params['breadcrumbs'][] = [
    'label' => 'Попутчики по маршруту',
    'url' => ['/route/']
];
$this->params['breadcrumbs'][] = 'Маршруты на букву ' . $letter;
?>

<h1>Все маршруты на букву "<?php echo $letter; ?>"</h1>

<?php
foreach ($trips as $key => $trip) {
    if ($key % 3 == 0): ?>
        <div class="row">
    <?php endif; ?>
    <div class="col-sm-4">
        <?= \yii\helpers\Html::a(
            $trip['from_name'] . ' - ' . $trip['to_name'],
            \yii\helpers\Url::to(['/route/view', 'cityFrom' => $trip['from_alias'], 'cityTo' => $trip['to_alias']])
        ) ?>
    </div>
    <?php if ($key % 3 == 2): ?>
        </div>
    <?php endif; ?>
    <?php
}
?>
<?php if ($key % 3 != 2): ?>
    </div>
<?php endif; ?>
