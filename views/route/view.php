<?php
use yii\helpers\Html;

/**
 * @var \app\models\GeoTown $from
 * @var \app\models\GeoTown $to
 */
$this->title = "Попутчики из " . $from->name . ' в ' . $to->name . " на автомобиле";

$this->registerMetaTag(['name' => 'description',
    'content' => "Все попутчики по маршруту с отправлением из " . $from->name . ' и прибытием в ' . $to->name . " на автомобиле"]);

$this->params['breadcrumbs'][] = [
    'label' => 'Попутчики по маршруту',
    'url' => ['/route/']
];
$this->params['breadcrumbs'][] = $from->name . ' - ' . $to->name;

?>
<div class="row">
    <div class="col-md-8">
        <h1>Попутчики <?php echo $from->name . ' &rarr; ' . $to->name; ?></h1>
    </div>
    <div class="col-md-4">
        <?= Html::a('Создать поездку по этому маршруту', ['/trip/start'], ['class' => 'btn btn-default btn-lg btn-block',]); ?>
    </div>

</div>
<div class="row">
    <div class="col-sm-6">
        <h2 class="vert-margin40">Объявления попутчиков:</h2>

        <ul class="nav nav-tabs">
            <li class="in nav nav-tabs active"><a href="#search-pass" data-toggle="tab"><i aria-hidden="true"></i>Ищут
                    пасажиров</a></li>
            <li class="in nav nav-tabs"><a href="#search-auto" data-toggle="tab"><i aria-hidden="true"></i>Ищут
                    водителей</a>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="search-pass">
                <h3>Водители ищут пассажиров</h3>
                <?= \app\widgets\TripSearchWidget::widget([
                    'type' => $type
                ]); ?>
            </div>
            <div class="tab-pane active" id="search-auto">
                <h3>Пассажиры ищут водителей</h3>
                <?= \app\widgets\TripSearchWidget::widget([
                    'type' => $type
                ]); ?>
            </div>
        </div>

    </div>
    <div class="col-sm-6">
        <h2 class="vert-margin40">Предложения коммерческих перевозчиков:</h2>
        <div class="alert alert-info lead">
            Если вы, занимаетесь коммерческими пасажирскими перевозками и вы хотите разместить тут информацию о своих
            маршутах, то вам <a href="#">сюда</a>
        </div>
    </div>
</div>

