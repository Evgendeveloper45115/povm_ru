<?php
/**
 * @var $model \app\models\Message
 */
?>
<div class="message my-message" style="text-align: right">
    <img src="<?= $model->fromUser->avatar ? '/img/profile/' . $model->fromUser->avatar : '/img/thumbs/user150.jpg' ?>"
         alt="Avatar"
         class="right">
    <div style="font-weight: bold" class="text-muted small">Я:</div>
    <p><?= $model->text ?></p>
    <span class="time-right small"><?= date('d-m-Y H:i', strtotime($model->datetime)) ?></span>
    <div class="small" style="float: left"><?= $model->viewed ? '<i class="fa fa-check-circle text-muted" title="Прочитано" aria-hidden="true"></i>' : '<i class="fa fa-check text-muted" title="Не прочитано" aria-hidden="true"></i>' ?></div>
</div>