<?php

use yii\helpers\Html;

/**
 * @var $model \app\models\Dialog
 */
$avatar = $model->message->messageFrom == Yii::$app->user->id ? $model->message->toUser->avatar : $model->message->fromUser->avatar;
$cnt = \app\models\Message::find()->where(['messageTo' => Yii::$app->user->id, 'dialog_id' => $model->id, 'viewed' => 0])->count();
?>
<tr data-key="<?= $model->id ?>">
    <td class="center-align">
        <?= \yii\helpers\Html::img($avatar ? '/img/profile/' . $avatar : '/img/thumbs/user150.jpg', ['class' => 'avatar-dialog']) ?>
    </td>
    <td class="align-left"><?= $model->message->messageFrom == Yii::$app->user->id ? $model->message->toUser->name : $model->message->fromUser->name ?></td>
    <td>
        <div class="center">
            <p>
                <?php
                if ($cnt) {
                    ?>
                    <span>Новых сообщений:</span>
                    <span><?= Html::tag('span', $cnt, ['class' => 'badge badge_custom']) ?></span>
                    <?php
                }
                ?>
            </p>
            <span style="text-wrap: none">
                    <a href="/dialog/<?= $model->id ?>/" title="Открыть диалог с пользователем">
                        <p class="center btn btn-block btn-default">Открыть диалог</p>
                    </a>
            </span>
    </td>
</tr>
