<?php
/**
 * @var $model \app\models\Message
 */
?>
<div class="message from-message">
    <img src="<?= $model->fromUser->avatar ? '/img/profile/' . $model->fromUser->avatar : '/img/thumbs/user150.jpg' ?>"
         alt="Avatar">
    <div style="font-weight: bold" class="text-muted small"><?= $model->fromUser->name ?>:</div>
    <p><?= $model->text ?></p>
    <span class="time-left small"><?= date('d-m-Y H:i', strtotime($model->datetime)) ?></span>
</div>