<?php
/**
 * @var $model \app\models\Message
 */
$render = 'sender_content';

if ($model->messageTo != Yii::$app->user->id) {
    $render = 'my_content';
}
echo Yii::$app->controller->renderPartial('content/' . $render, ['model' => $model]);