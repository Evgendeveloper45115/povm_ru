<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $user \app\auth\models\User */

/* @var $model \app\models\Message */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use \dosamigos\tinymce\TinyMce;

$this->title = $title = 'Личное сообщение для: ' . $user->name;
$this->params['breadcrumbs'][] = $title;
?>
<div class="site-contact">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <h1><?= Html::encode($title) ?></h1>
            <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

            <?= $form->field($model, 'text', [
            ])->textarea(['class' => 'textarea_message_dialog textarea_message', 'rows' => 10])->label(false) ?>

            <div class="form-group">
                <?= Html::submitButton('Отправить', ['class' => 'btn btn-lg btn-primary', 'name' => 'contact-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
        <div class="col-md-3"></div>
    </div>
</div>
