<?php

use \yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;

/* @var \yii\web\View $this */
/* @var \yii\data\ActiveDataProvider $dataProvider */
/* @var \yii\data\ActiveDataProvider $dataProvider2 */
/* @var string $title */
/* @var \app\models\DialogHasUser $sender */

$this->title = 'Диалог с пользователем';
?>
<div style="row">
    <div class="col-md-8 col-md-offset-2">
        <h1>Диалог с пользователем:
            <?= Html::a($sender->user->name, \yii\helpers\Url::to(['/profile/view', 'id' => $sender->user->id])) ?>
        </h1>
        <div class="dialog-window">

            <?= \yii\widgets\ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => 'message',
                'emptyText' => false,
                'layout' => "{items}"
            ]); ?>
            <br/>

        </div>
        <div class="dialog-message-form">
            <h3>Напишите сообщение:</h3>
            <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
            <div>
                <?= $form->field(new \app\models\Message(), 'text', [
                ])->textarea(['class' => 'textarea_message', 'rows' => 6])->label(false) ?>
            </div>

            <div class="form-group center-align">
                <?= Html::submitButton('Отправить', ['class' => 'btn btn-lg btn-primary', 'name' => 'contact-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>