<?php
/**
 * @var $this \yii\web\View
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $searchModel \app\models\MessageSearch
 * @var $dataProviderOut \yii\data\ActiveDataProvider
 */

use \yii\bootstrap\Tabs;
use yii\grid\GridView;


$this->title = 'Мои диалоги';
?>


<div style="row">
    <div class="col-md-8 col-md-offset-2">
        <h1>Мои диалоги</h1>
        <div>
            <table class="table table_striped">
                <tbody>
                <?= \yii\widgets\ListView::widget([
                    'dataProvider' => $dataProvider,
                    'itemView' => 'content/list',
                    'emptyText' => false
                ]); ?>
                </tbody>
            </table>
        </div>
    </div>
</div>