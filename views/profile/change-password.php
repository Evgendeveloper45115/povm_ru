<?php

/* @var $this yii\web\View */

/* @var $user \app\auth\models\User */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'Смена пароля для: ' . $user->name;
?>
    <div class="auto-form">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <h1><?= Html::encode($this->title); ?></h1>
            </div>
            <?php
            $form = ActiveForm::begin([
                'fieldConfig' => [
                    'options' => ['class' => 'form_group clearfix'],
                    'template' => "{label}\n<div class=\"col_right\">{input}\n<div class=\"error\">{error}</div></div>",
                ],
            ]) ?>
            <div class="col-md-12 col-sm-12">
                <?= $form->field($user, 'orig_password')->passwordInput()->label('Старый пароль') ?>
            </div>
            <div class="col-md-6 col-sm-6">
                <?= $form->field($user, 'new_password')->passwordInput()->label('Новый пароль') ?>
            </div>
            <div class="col-md-6 col-sm-6">
                <?= $form->field($user, 'password1')->passwordInput()->label('Повтор пароля') ?>
            </div>
            <div class="col-md-12 col-sm-12">
                <div class="form-group">
                    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
<?php

