<?php

/* @var $this yii\web\View */

/* @var $user \app\auth\models\User */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use \kartik\switchinput\SwitchInput;
use dosamigos\tinymce\TinyMce;

$this->title = 'Редактирование данных для : ' . $user->name;
?>
    <div class="auto-form">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <h1><?= Html::encode($this->title); ?></h1>
            </div>
            <?php
            $form = ActiveForm::begin([
                'fieldConfig' => [
                    'options' => ['class' => 'form_group clearfix'],
                    'template' => "{label}\n<div class=\"col_right\">{input}\n<div class=\"error\">{error}</div></div>",
                ],
            ]) ?>
            <div class="col-md-6 col-sm-6">
                <?= $form->field($user, 'town_id')->dropDownList([], [
                    //  'prompt' => $user->getAttributeLabel('town_id'),
                    'class' => 'form-control select-town',
                    'data' => [
                        'ajax--url' => Url::to(['/site/town'], true),
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                    ]
                ])->label('Город проживания' . ($user->town_id ? ': ' . $user->getTownName() : null)) ?>
            </div>
            <?php
            if ($user->gender === null) {
                ?>
                <div class="col-md-6 col-sm-6">
                    <?= $form->field($user, 'gender')->widget(SwitchInput::class, [
                        'type' => SwitchInput::CHECKBOX,
                        'pluginOptions' => [
                            'handleWidth' => 60,
                            'onText' => 'Мужской',
                            'offText' => 'Женский'
                        ]
                    ]) ?>
                </div>

                <?php
            }
            ?>
            <div class="col-md-6 col-sm-6">
                <?= $form->field($user, 'phone')->widget(\yii\widgets\MaskedInput::class, [
                    'mask' => '+7 999 999 99 99',
                ]) ?>
            </div>
            <div class="col-md-6 col-sm-6">
                <?= $form->field($user, 'phone_access')->dropDownList($user->phoneAccess())->label('Кому виден ваш телефон') ?>
            </div>
            <div class="col-md-12 col-sm-12">
                <?= $form->field($user, 'description', [
                ])->textarea(['class' => 'textarea_message_dialog textarea_message', 'rows' => 15]) ?>
            </div>
            <div class="col-md-12 col-sm-12">
                <?= $form->field($user, 'email_access')->widget(SwitchInput::class, [
                    'type' => SwitchInput::CHECKBOX,
                    'pluginOptions' => [
                        'handleWidth' => 60,
                        'onText' => 'Да',
                        'offText' => 'Нет'
                    ]
                ]) ?>
            </div>
            <div class="col-md-12 col-sm-12">
                <div class="form-group">
                    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
<?php

