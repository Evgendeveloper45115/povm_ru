<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\extensions\MoneyFormat;


/* @var $this yii\web\View */
/* @var $model \app\auth\models\User */
/* @var $id int */

$userName = Html::encode($model->name);
$town = $model->town;
$this->title = 'Профиль пользователя ' . $userName;


if ($model->role == \app\auth\models\User::ROLE_USER) {
    $this->registerMetaTag(
        [
            'name' => 'description',
            'content' => 'Пользователь сервиса поиска попутчиков ' . $userName .
                ' из города: ' . ($model->town_id ? $model->town->country->name . ' ' . $model->town->region->name . ' ' . $model->town->name : null) .
                ' попутчик.'
        ]
    );
} else {
    $this->registerMetaTag(
        [
            'name' => 'description',
            'content' => 'Комерческий перевозчик ' . $model->name_carrier . ' контактные данные и адрес и отзывы. ']);
}

$form = ActiveForm::begin(['fieldConfig' => ['options' => [],
    'template' => "{label}\n{input}\n<div class=\"error\">{error}</div>"
    ,],]);

$userMenu = [
    Html::a('<i class="fas fa-pencil-alt"></i> Редактировать', ['/profile/update']),
    Html::a('<i class="fas fa-lock"></i> Сменить пароль', ['/profile/change-password']),
    Html::a('<i class="fas fa-exclamation-circle "></i> Удалить профиль', ['/profile/delete'], ['class' => 'text-danger'])
];


?>
<div class="profile-view">
    <div class="row">
        <div class="col-md-2 col-sm-2">
            <label class="progress_load_form_photo_upload">
                <?= Html::img($model->avatar ? '/img/profile/' . $model->avatar : '/img/thumbs/user150.jpg', [
                    'class' => 'img-responsive',
                    'alt' => $userName . ($town ? ' ' . Html::encode($town->name) : ''),
                ]); ?>
                <?php
                if ($id && $id == Yii::$app->user->id) {
                    echo $form->field($model, 'avatar')->fileInput(['class' => 'avatar_img'])->label(false);
                }
                ?>
            </label>
            <?php
            if ($id && $id == Yii::$app->user->id) {
                ?>
                <div style="text-align: center" class="small text-mutted">
                    Кликните или перетяните фото для загрузки аватарки профиля
                </div>
                <?php
            }
            ?>
            <div style="text-align: center; margin-top: 10px;display: none" class="send-button">
                <button type="submit"><?= Html::a('<i class="fas fa-camera"></i> Сохранить фото') ?></button>
            </div>
            <?php
            if ($id && $id == Yii::$app->user->id) {
                ?>
                <h2>Управление</h2>
                <?= Html::ul($userMenu, [
                    'encode' => false,
                    'style' => 'list-style-position:inside;',
                ]); ?>
                <?php
            } elseif ($id && $id != Yii::$app->user->id) {
                echo Html::ul([Html::a('<i class="glyphicon glyphicon-send"></i> Написать сообщение', \yii\helpers\Url::to(['/dialog/send', 'id' => $id]))], [
                    'encode' => false,
                    'style' => 'list-style-position:inside;',
                ]);
            }
            ?>
        </div>
        <div class="col-md-6 col-sm-6">
            <h1><?= $userName; ?></h1>

            <?php if ($town) { ?>
                <?= '<p>Город: <i class="fas fa-map-marker-alt"></i> ' . Html::a(Html::encode($town->name), $town->getUrl()) . '</p>'; ?>
            <?php } ?>

            <?php if ($model->description) { ?>
                <p>О себе:</p>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <?= $model->description; ?>
                    </div>
                </div>
            <?php } ?>

            <?= \app\widgets\GarageWidget::widget([
                'user' => $model,
            ]); ?>
        </div>
        <div class="col-md-4 col-sm-4">
            <?php
            if ($id && $id == Yii::$app->user->id) {

                ?>
                <div class="balance-block">
                    <p class="lead text-center">Баланс: <?= MoneyFormat::rubles($model->balance) ?> руб. <a
                                href="<?= \yii\helpers\Url::to("/user/balance") ?>">Пополнить</a></p>
                </div>
                <?php
            }
            ?>
            <?= \app\widgets\TripHistory::widget([
                'userId' => $model->id,
            ]); ?>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>
