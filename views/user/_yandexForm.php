<form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml" class="balance-form">
    <input type="hidden" name="receiver" value="410011405228263">
    <input type="hidden" name="label" value="<?php echo Yii::$app->user->identity->id; ?>">
    <input type="hidden" name="quickpay-form" value="shop">    
    <input type="hidden" name="successURL" value="<?php echo Yii::$app->request->absoluteUrl; ?>">
    <input type="hidden" name="targets" value="Пополнение баланса пользователя <?php echo Yii::$app->user->identity->name; ?>">
    <div class="form-group">
        <div class="input-group text-center">
            <input type="text" name="sum" value="450" data-type="number" class="form-control text-right" >
            <div class="input-group-addon">руб.</div>
        </div>
    </div>
    <div>
        <label><input type="radio" name="paymentType" value="PC"> Яндекс.Деньгами <br />
        </label>    
        <label><input type="radio" name="paymentType" value="AC" checked> Банковской картой<br />
        </label> 
    </div>

    <input type="submit" class="btn btn-primary" value="Пополнить баланс">
</form>