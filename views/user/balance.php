<?php
/**
 * @var $this \yii\web\View
 * @var $dataProvider \yii\data\ActiveDataProvider
 */

use \app\models\Transaction;


$this->title = 'Мои финансы';
?>

<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <div style="text-align: center">
            <h1>Ваш баланс: <?= \app\extensions\MoneyFormat::rubles(Yii::$app->user->identity->balance) ?> руб.</h1>
            <p>Введите сумму пополнения:</p>
            <?php echo $this->render("@app/views/user/_yandexForm.php"); ?>
        </div>
    </div>
</div>
<hr/>
<div style="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <h3>История изменения баланса</h3>
        <?php
        echo \yii\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'tableOptions' => [
                'class' => 'table table-striped table-cell-valign-middle'
            ],
            'layout' => "{items}",
            'columns' => [
                'amount' => [
                    'attribute' => 'amount',
                    'value' => function (Transaction $model) {
                        return \app\extensions\MoneyFormat::rubles($model->amount) . ' руб.';
                    },
                    //   'label' => false
                ],
                'create_at' => [
                    'attribute' => 'create_at',
                    'value' => function (Transaction $model) {
                        return $model->create_at;
                    },
                    // 'label' => false
                ],
                'description' => [
                    'attribute' => 'description',
                    'value' => function (Transaction $model) {
                        return $model->description;
                    },
                    // 'label' => false
                ],
            ]
        ]);
        ?>
    </div>
    <div class="col-md-2"></div>

</div>