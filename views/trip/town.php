<?php

use yii\helpers\Html;

/* @var \yii\web\View $this */
/* @var \app\models\GeoCountry|null $country */
/* @var \app\models\GeoRegion|null $region */
/* @var \app\models\GeoTown|null $town */

$title = Html::encode($town->name);
$this->title = 'Попутчики из/в ' . $title . ' на машине';

$this->registerMetaTag(['name' => 'description',
    'content' => 'Поиск попутчиков в совместные поездки на автомобиле в городе  ' . $title . ' (' . $region->name . ', ' . $country->name . ')']);

$this->params['breadcrumbs'][] = [
    'label' => Html::encode($country->name),
    'url' => $country->getUrl(),
];

$this->params['breadcrumbs'][] = [
    'label' => Html::encode($region->name),
    'url' => $region->getUrl(),
];

$this->params['breadcrumbs'][] = $town->name;

?>

<div class="trips-town">
    <h1>Попутчики города <?= $town->name ?> (<?= $region->name ?>, <?= $country->name ?>)</h1>
    <div class="town-photo">
        тут вывести фото города, если фото не то заглушку по умолчанию
    </div>
    <div>
        тут выведем форму поиска поездки
    </div>

    <div class="row">
        <div class="col-sm-6">
            <h2>Попутчики с отправлением из этого города</h2>
        </div>
        <div class="col-sm-6">
            <h2>Коммерческие перевозки с отправлением из этого города</h2>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <h2>Попутчики с прибытием в этот город</h2>
        </div>
        <div class="col-sm-6">
            <h2>Коммерческие перевозки с прибытием в этот город</h2>
        </div>
    </div>

</div>
