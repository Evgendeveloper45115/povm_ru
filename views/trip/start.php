<?php

use yii\web\View;
use yii\helpers\Url;

/* @var $this View */

$this->title = 'Новая поездка: шаг 1 из 4';
?>
<div class="row">
    <div class="col-md-6 col-xs-12 col-lg-offset-3 vert-margin40">
        <h1><?= $this->title ?></h1>

        <a href="<?= Url::to(['trip/passenger', 'step' => 2]) ?>" class="btn btn-lg btn-block btn-success select-role-btn">Я пассажир, ищу попутную машину</a>
        <a href="<?= Url::to(['trip/driver', 'step' => 2]) ?>" class="btn btn-lg btn-block btn-success select-role-btn">Я водитель, ищу пассажиров</a>

    </div>
</div>
