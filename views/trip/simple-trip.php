<?php

use app\companion\form\SimpleTripForm;
use kartik\datetime\DateTimePicker;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\web\View;

/* @var $this View */
/* @var $form ActiveForm */
/* @var $model SimpleTripForm */
?>
<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4">

        <?= $form->field($model, 'fromLocationId')->dropDownList([], [
            'prompt' => $model->getAttributeLabel('fromLocationId'),
            'class' => 'form-control select-town',
            'data' => [
                'ajax--url' => Url::to(['/site/town'], true),
            ],
            'pluginOptions' => [
                'autoclose' => true,
            ]
        ])->label(false) ?>

        <!--   <?= $form->field($model, 'fromPlace')->textInput() ?> -->


        <?= $form->field($model, 'toLocationId')->dropDownList([], [
            'prompt' => $model->getAttributeLabel('toLocationId'),
            'class' => 'form-control select-town',
            'data' => [
                'ajax--url' => Url::to(['/site/town'], true),
            ],
            'pluginOptions' => [
                'autoclose' => true,
            ]
        ])->label(false) ?>

        <!--  <?= $form->field($model, 'toPlace')->textInput() ?> -->

        <?= $form->field($model, 'dateTo')->widget(DateTimePicker::class, [
            'type' => DateTimePicker::TYPE_INPUT,
            'options' => [
                'placeholder' => 'Дата выезда',
            ],
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'dd.mm.yyyy hh:ii',
                'language' => 'ru',
            ]
        ])->label(false); ?>
        <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

        <?= $form->field($model, 'wayBack')->checkbox(); ?>

        <div class="date-back" style="display:none;">
            <div class="form-group">
                <?= $form->field($model, 'dateBack')->widget(DateTimePicker::class, [
                    'type' => DateTimePicker::TYPE_INPUT,
                    'options' => [
                        'placeholder' => 'Дата обратной поездки',
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'dd.mm.yyyy hh:ii',
                        'language' => 'ru',
                    ]
                ])->label(false)->error(); ?>
            </div>
        </div>
    </div>

</div>
