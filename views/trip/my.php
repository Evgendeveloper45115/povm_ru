<?php

use app\assets\Select2Asset;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchForm \app\models\SearchForm */

Select2Asset::register($this);

$this->title = 'Мои объявления';
?>

<div class="body-content">
    <h1><?= $this->title ?></h1>
    <div class="row">
        <div class="col-md-8">
            <p>
                <?= \app\widgets\TripWidget::widget([
                    'title' => '',
                    'my' => true
                ]); ?>
            </p>
        </div>
    </div>
</div>
