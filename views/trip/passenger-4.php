<?php

use app\auth\forms\RegistrationForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */
/* @var $form ActiveForm */
/* @var $model RegistrationForm */
/* @var $previousLink string */

$this->title = 'Новая поездка: шаг 4 из 4';
?>
<h1><?= $this->title ?></h1>
<?php $form = ActiveForm::begin(['id' => 'add-passenger-user-form']); ?>
<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

        <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>

        <?= $form->field($model, 'phone')->textInput(['autofocus' => true]) ?>

        <?= $form->field($model, 'password')->passwordInput() ?>

        <div class="form-group">
            <?= Html::a('Назад', $previousLink, ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
            <?= Html::submitButton('Завершить', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
        </div>
    </div>
    <div class="col-md-4"></div>
    <?php ActiveForm::end(); ?>
