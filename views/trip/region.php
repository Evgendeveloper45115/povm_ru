<?php

use app\widgets\GeoWidget;
use yii\helpers\Html;

/* @var \yii\web\View $this */
/* @var \app\models\GeoCountry|null $country */
/* @var \app\models\GeoRegion|null $region */

$title = Html::encode($region->name);
$this->title = 'Попутчики на машине ' . $title;

$this->registerMetaTag(['name' => 'description',
    'content' => 'Поиск попутчиков в Совместные поездки на авто по ' . $title . '. Найти попутчика']);

$this->params['breadcrumbs'][] = [
    'label' => Html::encode($country->name),
    'url' => $country->getUrl(),
];

$this->params['breadcrumbs'][] = $title;

?>

<div class="trips-region">
    <div class="col-md-2 col-sm-2">
        <?= GeoWidget::widget(['model' => $region]); ?>
    </div>
    <div class="col-md-7 col-sm-7">
        <h1><?= $title; ?></h1>
    </div>
</div>
