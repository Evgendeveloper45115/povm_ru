<?php

use yii\helpers\Html;
use app\components\StringHelper;
use app\companion\model\Trip;

/* @var $this yii\web\View */
/* @var $model \app\companion\model\Trip */

$passengerIcon = '<div class="trip-icon" title="Кол-во пассажиров"><i class="fas fa-user"></i></div>';
$carIcon = '<div  class="trip-icon" title="Автомобиль"><i class="fas fa-car"></i></div>';
$clockIcon = '<div class="trip-icon" title="Время выезда"><i class="fas fa-clock"></i></div>';
$rubIcon = '<div class="trip-icon" title="Стоимость поездки"><i class="fas fa-ruble-sign "></i></div>';
$viewsIcon = '<div class="trip-icon" title="Просмотров"><i class="fas fa-eye "></i></div>';

$townFrom = Html::encode($model->town_from);
$townTo = Html::encode($model->town_to);

$this->title = 'Попутчики ' . $townFrom . ' - ' . $townTo . '. На машине ' .
    StringHelper::mb_ucfirst(Yii::$app->formatter->asDate($model->trip_time, 'php:D d M Y H:i')) . '. ';

$this->registerMetaTag(['name' => 'description',
    'content' => "Поиск попутчиков (пассажиров или водителя) на машине по маршруту: $townFrom - $townTo. Отправление:  " . $model->trip_time]);

$this->params['breadcrumbs'][] = [
    'label' => 'Попутчики на автомобиле',
    'url' => ['/trip/index']
];
$this->params['breadcrumbs'][] = $townFrom . ' - ' . $townTo;

$car = $model->car && $model->car->model_id && $model->car->mark_id ? Html::encode($model->car->carMark->name) . '&nbsp;' . Html::encode($model->car->carModel->name) : Html::encode($model->car->manufacturer) . '&nbsp;' . Html::encode($model->car->model);
$userName = Html::encode($model->user->name);

\app\assets\YMapsAsset::register($this);

?>
    <h1 class="vert-margin30"><?= 'Попутчики' . ' ' . $townFrom . ' &rarr; ' . $townTo; ?> </h1>
    <div class="row">
        <div class="col-md-8">
            <?php
            if ($model->status == Trip::STATUS_FINISHED) {
                ?>
                <div class="alert alert-info">Внимание! данная поездка <strong>состоялась</strong>, попробуйте найти
                    другую поездку по этому маршруту
                </div>
                <?php
            }
            ?>
            <div class="row">


                <div class="col-md-3 center-align vert-margin20">
                    <?= Html::a(
                        Html::img($model->user->avatar ? '/img/profile/' . $model->user->avatar : '/img/thumbs/user150.jpg', [
                            'alt' => Html::encode($model->user->name),
                            'class' => 'img-responsive',
                            'style' => 'max-height: 300px',
                        ]), ['/profile/view', 'id' => $model->user->id]
                    ); ?>

                </div>
                <?php
                if ($model->type == Trip::USER_TYPE_PASSENGER && $model->car) {
                    ?>
                    <div class="col-md-5 center-align vert-margin20">
                        <?= Html::a(
                            Html::img($model->car->avatar ? '/img/car/' . $model->car->avatar : '/img/thumbs/no_photo_auto.png', [
                                'alt' => Html::encode($model->user->name),
                                'class' => 'img-responsive car-img-trip',
                                'style' => 'max-height: 120px;',
                            ]), \yii\helpers\Url::to(['/car/' . $model->car->id])
                        ); ?>
                        <p> <?= Html::a($car, ['/car/' . $model->car->id]) . ' (' . $model->car->year . ') г.в.'; ?></p>
                        <?php
                        if ($model->car_id && $model->car->state_number) {
                            ?>
                            <div class="lead">
                                номер <?= $model->car->state_number ?>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                }
                ?>
                <div class="col-md-4 vert-margin20">
                    <div class="trip-icon"><i class="fas fa-user"></i></div>
                    <strong> <?= Html::a($userName, ['/profile/view', 'id' => $model->user->id]); ?></strong> ищет
                    <?= $model->type === Trip::USER_TYPE_PASSENGER ? 'пассажиров' : 'водителя'; ?>
                    <br/>
                    <div class="trip-icon"><i class="fas fa-calendar"></i></div>
                    <strong>Дата:</strong> <?= StringHelper::mb_ucfirst(Yii::$app->formatter->asDate($model->trip_time, 'php:D d M Y')); ?>
                    <br/>
                    <div class="trip-icon"><i class="fas fa-clock"></i></div>
                    <strong>Время выезда:</strong> <?= Yii::$app->formatter->asTime($model->trip_time, 'php:H:i'); ?>
                    <br/>
                    <strong>
                        <div class="trip-icon"><i class="fas fa-user"></i>
                        </div><?= $model->type === Trip::USER_TYPE_PASSENGER ? 'Свободных мест:' : 'Пассажиров:'; ?>
                    </strong> <?= $model->passengers; ?>
                    <br/>
                    <strong>
                        <?php
                        $display = false;
                        if ($model->price) {
                            ?>
                            <div class="trip-icon"><i class="fas fa-ruble-sign"></i></div>
                            <?php
                            $display = true;
                            echo $model->type === Trip::USER_TYPE_PASSENGER ? 'Цена за место:' : 'Готовы заплатить:';
                        }
                        ?>

                    </strong>
                    <?= $display ? $model->price . ' руб.' : null; ?>
                </div>

            </div>

            <?php if (!empty($model->description)) { ?>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <h2>Комментарий к поездке</h2>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <?= $model->description; ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>

            <h2>Маршрут на карте (ориентировочный)</h2>
            <div id="ymap" style="width:100%;height:400px;" class="vert-margin20">
            </div>
            <p><strong>*Маршрут строится автоматически и может отличаться от реального!</strong></p>


        </div>


        <div class="col-md-4">
            <a href="<?= \yii\helpers\Url::to(['/trip/respond', 'id' => $model->id]) ?>"
               class="btn btn-lg btn-block btn-default">Откликнуться</a>
            <button type="button" class="btn btn-lg btn-block btn-default">Создать свою поездку</button>
            <?php
            if ($model->user_id == Yii::$app->user->id) {
                ?>
                <h3>Это ваша поездка и вы можете:</h3>
                <a href="<?= \yii\helpers\Url::to(['/trip/archive', 'id' => $model->id]) ?>"
                   class="btn btn-lg btn-block btn-default">Деактивировать</a>
                <a href="<?= \yii\helpers\Url::to(['/trip/update', 'id' => $model->id]) ?>"
                   class="btn btn-lg btn-block btn-default">Редактировать</a>
                <?php
            }
            ?>

            <h2>Информация о маршруте:</h2>
            <div class="vert-margin30 small">
                <p>
                    Расстояние: км.<br/>
                    Примерное время в пути: ч.м.<br/>
                    При среднем расходе топлива указанного автомобиля
                    количество топлива которое будет использовано: л.<br/>
                    Ориентировочная стоимость топлива: руб.<br/>
                </p>
            </div>
        </div>
    </div>


<?php
$townFrom = $model->town_from;
$townTo = $model->town_to;
$this->registerJs(
    <<<JS

var map;
var townFrom = "$townFrom";
var townTo = "$townTo";

ymaps.ready(initMap);

function initMap()
{
    map = new ymaps.Map('ymap', {
        center: [55.76, 37.64],
        zoom: 10
    }, {
        searchControlProvider: 'yandex#search'
    });
    
    var route = new ymaps.multiRouter.MultiRoute({   
        referencePoints: [
            townFrom,
            townTo
        ]
    }, {
        boundsAutoApply: true
    });
    
    map.geoObjects.add(route);
    
    route.events.once('update', function() {
        route.getActiveRoute().balloon.open();
    });
    
}

JS
);
