<?php

use app\companion\model\Car;
use app\models\CarMark;
use app\models\CarModel;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model \app\companion\model\Trip */
/* @var $user \tests\models\User */

$this->title = 'Редактирование поездки: ' . $model->from->name . ' - ' . $model->to->name;

$this->params['breadcrumbs'][] = [
    'label' => 'Моё объявление',
    'url' => ['/trip/' . $model->id],
];

$this->params['breadcrumbs'][] = 'Редактирование';
$user = Yii::$app->user->identity;
?>

<div class="auto-form">

    <h1><?= Html::encode($this->title); ?></h1>
    <?php $form = ActiveForm::begin([]); ?>
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">

            <?= $form->field($model, 'trip_time')->widget(DateTimePicker::class, [
                'type' => DateTimePicker::TYPE_INPUT,
                'options' => [
                    'placeholder' => 'Дата выезда',
                ],
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'dd.mm.yyyy hh:ii',
                    'language' => 'ru',
                ]
            ]) ?>

            <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'passengers')->textInput() ?>

            <?= $form->field($model, 'price')->textInput() ?>

            <?php
            if ($user && $user->cars) {
                echo '<strong class="vert-margin30 ">Выберите свое авто:</strong>';
                echo $form->field($model, 'car_id')->dropDownList([$model->car_id => $model->car->model] + \yii\helpers\ArrayHelper::map($user->cars, 'id', function (Car $model) {
                        return $model->carMark->name . ' ' . $model->carModel->name . ' (' . $model->year . ')';
                    }))->label(false);
            }
            ?>
            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-lg btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

    <div class="col-md-4"></div>


</div>