<?php

use app\companion\form\SimpleEmailForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */
/* @var $form ActiveForm */
/* @var $model SimpleEmailForm */
/* @var $previousLink string */

$this->title = 'Новая поездка: шаг 3 из 4';
?>
<h1><?= $this->title ?></h1>
<?php $form = ActiveForm::begin(['id' => 'add-email-trip-form']); ?>
<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>
        <div class="form-group">
            <?= Html::a('Назад', $previousLink, ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
            <?= Html::submitButton('Продолжить', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
        </div>
    </div>
    <div class="col-md-4"></div>

    <?php ActiveForm::end(); ?>

