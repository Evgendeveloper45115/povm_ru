<?php

use app\auth\forms\RegistrationForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */
/* @var $form ActiveForm */
/* @var $model RegistrationForm */
/* @var $previousLink string */
/* @var $trip \app\companion\model\Trip */

$this->title = 'Отклик на поездку';
?>
<?php $form = ActiveForm::begin(['id' => 'add-passenger-user-form']); ?>
<div class="row">
    <?php
    if (Yii::$app->user->isGuest && !isset($exist)) {
        ?>
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <h1><?= $this->title ?></h1>

            <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'phone')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'comment')->textarea(['autofocus' => true, 'rows' => 5]) ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

            <div class="form-group">
                <?= Html::a('Назад', '/trip/' . $trip->id, ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                <?= Html::submitButton('Откликнуться', ['class' => 'btn btn-primary', 'name' => 'contact-button-add']) ?>
            </div>
        </div>
        <div class="col-md-4"></div>

        <?php
    } else {
        ?>
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <p class="lead">
                <span>Маршрут:</span>  <?= $trip->town_from . ' - ' . $trip->town_to ?>

                <br/>
                <span>Когда: </span>  <?= $trip->trip_time ?>
            </p>

            <?= $form->field($model, 'comment')->textarea(['autofocus' => true, 'rows' => 5]) ?>
            <?= $form->field(new \app\auth\models\User(), 'email')->hiddenInput(['value' => $model->email ?: Yii::$app->user->identity->email])->label(false) ?>
            <div class="form-group">
                <?= Html::a('Назад', '/trip/' . $trip->id, ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                <?= Html::submitButton('Откликнуться', ['class' => 'btn btn-primary', 'name' => 'contact-button-add']) ?>
            </div>

        </div>
        <div class="col-md-3"></div>

        <?php
    }
    ?>
    <?php ActiveForm::end(); ?>
