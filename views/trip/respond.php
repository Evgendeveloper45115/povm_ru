<?php

use app\auth\models\User;
use app\companion\form\SimpleEmailForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */
/* @var $form ActiveForm */
/* @var $model SimpleEmailForm */
/* @var $trip \app\companion\model\Trip */

$this->title = 'Отклик на поездку: ';
?>
<?php $form = ActiveForm::begin(['id' => 'add-email-trip-form']); ?>
<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <h1><?= $this->title ?></h1>
        <p class="lead">
            <span>Маршрут:</span> <?= $trip->town_from . ' - ' . $trip->town_to ?>

            <br/>
            <span>Когда: </span><?= $trip->trip_time ?>
        </p>
        <?= $form->field(new User(), 'email')->textInput(['autofocus' => true]) ?>
        <div class="form-group">
            <?= Html::a('Назад', '/trip/' . $trip->id, ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
            <?= Html::submitButton('Откликнуться', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
        </div>
    </div>
    <div class="col-md-3"></div>

</div>
<?php ActiveForm::end(); ?>

