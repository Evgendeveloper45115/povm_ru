<?php

use app\companion\form\AddPassengerTripForm;
use kartik\datetime\DateTimePicker;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/* @var $this View */
/* @var $form ActiveForm */
/* @var $model AddPassengerTripForm */
/* @var $previousLink string */

$this->title = 'Новая поездка: шаг 2 из 4';
?>
<h1 class="vert-margin40"><?= $this->title ?></h1>
<?php $form = ActiveForm::begin(['id' => 'add-passenger-trip-form']); ?>
<?= $form->errorSummary($model) ?>
<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-9">
        <h2>Откуда и куда необходимо доехать:</h2>
    </div>
</div>

<?= $this->render('simple-trip', ['model' => $model->simpleTrip, 'form' => $form]) ?>
<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <div class="form-group">
            <?= Html::a('Назад', $previousLink, ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
            <?= Html::submitButton('Продолжить', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
        </div>
    </div>
    <div class="col-md-4"></div>
</div>
<?php ActiveForm::end(); ?>
