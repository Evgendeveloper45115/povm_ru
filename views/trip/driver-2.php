<?php

use app\companion\form\AddDriverTripForm;
use app\companion\model\Car;
use app\models\CarMark;
use app\models\CarModel;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */
/* @var $form ActiveForm */
/* @var $model AddDriverTripForm */
/* @var $previousLink string */

$this->title = 'Новая поездка: шаг 2 из 4';
$user = Yii::$app->user->identity;
?>
<h1 class="vert-margin40"><?= $this->title ?></h1>

<?php $form = ActiveForm::begin(['id' => 'add-driver-trip-form']); ?>
<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-9">
        <h2>Места встречи и высадки:</h2>
    </div>

</div>

<?= $this->render('simple-trip', ['model' => $model->simpleTrip, 'form' => $form]) ?>
<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-9">
        <h2>Количество мест и стоимость:</h2>
    </div>
</div>
<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <?= $form->field($model, 'tripCapacity')->textInput() ?>

        <?= $form->field($model, 'price')->textInput() ?>
    </div>
    <div class="col-md-4"></div>
</div>

<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-9">
        <h2>Информация об автомобиле:</h2>
    </div>
</div>
<div class="row vert-margin10">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <?php

        if ($user && $user->cars) {
            echo '<strong class="vert-margin30 ">Выбрите из ранее добавленных:</strong>';
            echo Html::dropDownList('AddDriverTripForm[MyAuto]', [], ['Выбрать свое авто'] + \yii\helpers\ArrayHelper::map($user->cars, 'id', function (Car $model) {
                    return $model->carMark->name . ' ' . $model->carModel->name . ' (' . $model->year . ')';
                }), ['class' => 'form-control my_auto']);
            echo '<strong class="vert-margin30 ">Или добавьте новую:</strong>';
        }
        ?>
    </div>
    <div class="col-md-4"></div>
</div>
<div class="row vert-margin40 custom_disable">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <?= $form->field($model, 'carMarkId')->dropDownList(CarMark::getDropdownList(), [
            'class' => 'form-control select2',
            'id' => 'car-mark',
            'prompt' => '- Выбрать -',
        ]); ?>

        <?= $form->field($model, 'carModelId')->dropDownList(CarModel::getDropdownList($model->carMarkId), [
            'class' => 'form-control select2',
            'id' => 'car-model',
            'prompt' => '- Выбрать -',
        ]); ?>

        <?= $form->field($model, 'carYear')->dropDownList(Car::getYears(), [
            'class' => 'form-control select2',
            'prompt' => '- Выбрать -',
        ]); ?>

        <div class="form-group">
            <?= Html::a('Назад', $previousLink, ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
            <?= Html::submitButton('Продолжить', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
        </div>
    </div>
    <div class="col-md-4"></div>
</div>


<?php ActiveForm::end(); ?>
