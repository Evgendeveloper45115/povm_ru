<?php

use app\widgets\GeoWidget;
use yii\helpers\Html;

/* @var \yii\web\View $this */
/* @var \app\models\GeoCountry|null $country */

$title = Html::encode($country->name);
$this->title = 'Попутчики ' . $title . ' в совместные поездки на автомобиле';

$this->params['breadcrumbs'][] = $title;

$this->registerMetaTag(['name' => 'description',
    'content' => 'Совместные поездки на автомобиле по ' . $title . ' , найди себе попутчика и сокращай свои расходы на поездки']);

?>

<div class="trips-country">
    <div class="col-md-2 col-sm-2">
        <?= GeoWidget::widget(['model' => $country]); ?>
    </div>
    <div class="col-md-7 col-sm-7">
        <h1><?= $title; ?></h1>

    </div>
</div>
