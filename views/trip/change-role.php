<?php

use yii\web\View;
use yii\helpers\Url;

/* @var $this View */

$this->title = 'Новая поездка: шаг 2 из 4';
?>
<div class="row">
    <div class="col-md-6 col-xs-12 col-lg-offset-3 vert-margin40">
        <h1><?= $this->title ?></h1>

        <a href="<?= Url::to(['trip/driver', 'step' => 2, 'type' => \app\auth\models\User::ROLE_USER]) ?>"
           class="btn btn-lg btn-block btn-success select-role-btn">Частное лицо</a>
        <a href="<?= Url::to(['site/register']) ?>"
           class="btn btn-lg btn-block btn-success select-role-btn">Коммерческий перевозчик</a>

    </div>
</div>
