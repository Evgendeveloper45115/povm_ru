<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \app\auth\forms\RestorePasswordForm */

$title = 'Восстановление пароля';

$this->title = Yii::$app->name . ' - ' . $title;
$this->params['breadcrumbs'][] = $title;
?>
<div class="restore-password">
    <h1><?= Html::encode($title) ?></h1>

    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <?php $form = ActiveForm::begin([
                'id' => 'restore-password-form',
            ]); ?>

            <?= $form->field($model, 'email')->textInput(['autofocus' => true]); ?>

            <div class="form-group">
                <?= Html::submitButton('Восстановить пароль', ['class' => 'btn btn-primary', 'name' => 'restore-password-button']); ?>
            </div>

            <?= Html::a('Регистрация', ['/site/register']); ?>
            |
            <?= Html::a('Вход', ['/site/login']); ?>

            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-md-4"></div>

    </div>
</div>
