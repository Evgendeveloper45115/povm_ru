<?php
$title = 'Пользовательское соглашение';
$this->title = $title . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = $title;
$this->registerMetaTag(['name' => 'description',
    'content' => 'Пользовательское соглашение проекта Поехали Вместе.']);
?>

<h1>Пользовательское соглашение</h1>
