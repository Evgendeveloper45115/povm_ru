<?php

/* @var \yii\web\View $this */

$title = 'Наши партнеры';
$this->title = $title;

$this->params['breadcrumbs'][] = $title;
$this->registerMetaTag(['name' => 'description',
    'content' => 'Партнеры проекта Поехали Вместе.']);
?>

<h1>Наши партнеры</h1>
