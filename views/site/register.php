<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model \app\auth\forms\RegistrationForm */

$this->title = 'Регистрация';

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auto-form">
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <h1><?= Html::encode($this->title); ?></h1>

            <?php
            $form = ActiveForm::begin([
                'fieldConfig' => [
                    'options' => ['class' => 'form_group clearfix'],
                    'enableClientValidation' => false,
                    'template' => "{label}\n<div class=\"col_right\">{input}\n<div class=\"error\">{error}</div></div>",
                ],
            ]) ?>
            <?php
            //            \yii\helpers\VarDumper::dump(Yii::$app->request->referrer,11,1)
            if (stristr(Yii::$app->request->referrer, 'trip/driver')) {
                echo $form->field($model, 'role')->hiddenInput(['value' => 'carrier'])->label(false);
            } else {
                echo $form->field($model, 'role')->dropDownList(\app\auth\models\User::$roles, ['class' => 'roles form-control'])->label('Кто вы?');
            }
            ?>


            <div class="carrier_fields"
                 style="display: <?= $model->role == 'carrier' || stristr(Yii::$app->request->referrer, 'trip/driver') ? 'block' : 'none' ?>">
                <?= $form->field($model, 'name_carrier')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'carrier_count_car')->textInput(['autofocus' => true]) ?>

            </div>
            <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'email')->textInput() ?>

            <?= $form->field($model, 'town_id')->dropDownList([], [
                //  'prompt' => $user->getAttributeLabel('town_id'),
                'class' => 'form-control select-town',
                'data' => [
                    'ajax--url' => Url::to(['/site/town'], true),
                ],
                'pluginOptions' => [
                    'autoclose' => true,
                ]
            ])->label('Город проживания' . ($user->town_id ? ': ' . $user->getTownName() : null)) ?>

            <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::class, [
                'mask' => '+7 999 999 99 99',
            ]) ?>

            <?= $form->field($model, 'phone_access')->dropDownList($model->phoneAccess())->label('Кому виден ваш телефон') ?>

            <?= $form->field($model, 'password')->passwordInput() ?>


            <div class="form-group">
                <?= Html::submitButton('Зарегистрироваться', ['class' => 'btn btn-block btn-lg btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>
