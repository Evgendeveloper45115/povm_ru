<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \app\companion\model\Car */

$this->title = 'Автомобиль ' . $model->getTitle();

$this->registerMetaTag(['name' => 'description',
    'content' => $model->getContent() . '. Владелец автомобиля ' . $model->user->name]);

$this->params['breadcrumbs'][] = [
    'label' => 'Автомобили',
    'url' => ['/auto/index'],
];

$this->params['breadcrumbs'][] = $this->title;

$town = $model->user->town;
?>

<div class="auto-view">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <h1>
                <?= Html::encode($this->title); ?>
                <?php
                if ($model->name) {
                    ?>
                    <span class="car-nickname"><?= $model->name ?></span>
                    <?php
                }
                ?>
            </h1>
        </div>
        <div class="col-md-6 cols-sm-6">

            <h2>
                Владелец: <?= Html::a(Html::encode($model->user->name), ['/profile/view', 'id' => $model->user->id]); ?></h2>
            <?= Html::a(
                Html::img($model->user->avatar ? '/img/profile/' . $model->user->avatar : '/img/thumbs/user150.jpg', [
                    'class' => '',
                    'alt' => Html::encode($model->user ? $model->user->name : 'unknown'),
                ]), ['/profile/view', 'id' => $model->user->id]
            ); ?>

            <h2>Описание машины</h2>
            <div class="lead">
                <?= 'Год выпуска: ' . $model->year . '<br/>'; ?>
                <?= 'Расход топлива: ' . $model->consumption . 'л. на 100км.<br/>'; ?>
                <?= 'Регистрационный номер авто: ' . $model->state_number . '<br/>'; ?>
            </div>
            <?php if ($model->description) { ?>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <?= $model->description; ?>
                    </div>
                </div>
            <?php } ?>
            <div>
                <?= \app\widgets\TripHistory::widget([
                    'userId' => $model->user_id,
                ]); ?>
            </div>

        </div>
        <div class="col-md-6 cols-sm-6">
            <h2>Фото автомобиля</h2>
            <?= Html::img($model->avatar ? '/img/car/' . $model->avatar : '/img/thumbs/no_photo_auto.png', [
                'class' => 'img-responsive car-img-trip',
                'alt' => Html::encode($model->name ? $model->name : 'unknown'),
            ]);
            ?>
        </div>
    </div>
</div>
