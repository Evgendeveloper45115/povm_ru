<?php

/* @var $this yii\web\View */

$this->title = 'Автомобили наших пользователей - автопарк';
$this->registerMetaTag(['name' => 'description',
    'content' => 'Автопарк нашего проекта. Тут мы покажем вам немног статистики об автомобилях наших пользователеей']);
?>

<div class="auto-index">
    <h1><?= $this->title; ?></h1>
</div>
