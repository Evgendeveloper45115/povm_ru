<?php

use app\companion\model\Car;
use app\models\CarMark;
use app\models\CarModel;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \app\companion\model\Car */

$this->title = $model->isNewRecord ? 'Добавление автомобиля' : 'Редактирование автомобиля';

$this->params['breadcrumbs'][] = [
    'label' => 'Автомобили',
    'url' => ['/auto/index'],
];

$this->params['breadcrumbs'][] = [
    'label' => $model->getTitle(),
    'url' => ['/auto/view', 'id' => $model->id],
];

$this->params['breadcrumbs'][] = 'Редактирование';


?>

<div class="auto-form">

    <h1><?= Html::encode($this->title); ?></h1>
    <?php $form = ActiveForm::begin([]); ?>
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <?= $form->field($model, 'mark_id')->dropDownList(CarMark::getDropdownList(), [
                        'class' => 'form-control select2',
                        'id' => 'car-mark',
                        'prompt' => '- Выбрать -',
                        'disabled' => $model->isNewRecord ? false : 'disabled'
                    ]); ?>

                    <?= $form->field($model, 'model_id')->dropDownList(CarModel::getDropdownList($model->mark_id), [
                        'id' => 'car-model',
                        'prompt' => '- Выбрать -',
                        'disabled' => $model->isNewRecord ? false : 'disabled'
                    ]); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <?= $form->field($model, 'year')->dropDownList(Car::getYears(), [
                        'class' => 'form-control select2',
                        'prompt' => '- Выбрать -',
                        'disabled' => $model->isNewRecord ? false : 'disabled'
                    ]); ?>
                </div>
                <div class="col-md-4 col-sm-4">
                    <?= $form->field($model, 'state_number')->textInput([
                        'disabled' => $model->isNewRecord || !$model->state_number ? false : 'disabled'
                    ]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'description')->textarea(); ?>
                </div>
            </div>


        </div>

        <div class="col-md-6 col-sm-6">
            <label class="progress_load_form_photo_upload">
                <?= Html::img($model->avatar ? '/img/car/' . $model->avatar : '/img/thumbs/no_photo_auto.png', [
                    'class' => 'img-responsive car-img-trip',
                    'style' => 'max-width:100%',
                    'alt' => $userName . ($town ? ' ' . Html::encode($town->name) : ''),
                ]); ?>

                <?= $form->field($model, 'avatar')->fileInput(['class' => 'avatar_img'])->label(false) ?>
                <p class="small text-muted text-center">Кликните на фото для загрузки новой фотографии вашего
                    автомобиля</p>

            </label>
            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-lg btn-primary']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>