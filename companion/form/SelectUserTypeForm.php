<?php

namespace app\companion\form;

use app\companion\model\Trip;
use yii\base\Model;

class SelectUserTypeForm extends Model
{
    public $userType;

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['userType'], 'required'],
            [['userType'], 'in', 'range' => [Trip::USER_TYPE_DRIVER, Trip::USER_TYPE_PASSENGER]],
        ];
    }
}
