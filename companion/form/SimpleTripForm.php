<?php

namespace app\companion\form;

use app\models\GeoTown;
use DateTime;
use yii\base\Model;

class SimpleTripForm extends Model
{
    public $fromLocationId;
    public $toLocationId;
    public $dateTo;
    public $dateBack;
    public $wayBack;
    public $fromPlace;
    public $toPlace;
    public $description;

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [

            [['fromLocationId', 'toLocationId', 'dateTo'], 'required'],
            [['dateTo'], 'validateDateTime', 'skipOnEmpty' => false, 'skipOnError' => false],
            [['dateBack'], 'validateDateTime', 'skipOnEmpty' => false, 'skipOnError' => false, 'when' => function ($model) {
                return $model->wayBack;
            }],
            [['wayBack'], 'in', 'range' => [0, 1]],
            [['fromLocationId', 'toLocationId'], 'exist', 'targetClass' => GeoTown::class, 'targetAttribute' => 'id'],
            [['fromPlace', 'toPlace', 'description'], 'string', 'max' => 255],
        ];
    }

    public function validateDateTime($attribute, $params)
    {
        if (DateTime::createFromFormat('d.m.Y H:i', str_replace('+', ' ', $this->$attribute)) == false) {
            $this->addError($attribute, 'Неверно заполнены дата/время');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fromLocationId' => 'Город отправления',
            'toLocationId' => 'Город прибытия',
            'dateTo' => 'Дата/время выезда',
            'dateBack' => 'Дата/время выезда',
            'wayBack' => 'Поездка в обе стороны',
            'fromPlace' => 'Точное место встречи',
            'description' => 'Комментарий к поездке',
            'toPlace' => 'Место прибытия'
        ];
    }
}
