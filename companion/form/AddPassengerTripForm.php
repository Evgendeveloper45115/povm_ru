<?php

namespace app\companion\form;

use elisdn\compositeForm\CompositeForm;

/**
 * Форма валидации информации о путешествии попутчика:
 * Откуда, куда, когда, во сколько забрать, куда именно доставить
 *
 * @property SimpleTripForm $simpleTrip
 */
class AddPassengerTripForm extends CompositeForm
{
    /**
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->simpleTrip = new SimpleTripForm();
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    protected function internalForms()
    {
        return ['simpleTrip'];
    }
}
