<?php

namespace app\companion\form;

use yii\base\Model;

class SimpleEmailForm extends Model
{
    /** @var string */
    public $email;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['email'], 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => 'E-mail',
        ];
    }

    /**
     * @inheritDoc
     */
    public function attributeHints()
    {
        return [
            'e-mail' => 'E-mail',
        ];
    }
}
