<?php

namespace app\companion\form;

use app\auth\models\User;
use app\models\CarMark;
use app\models\CarModel;
use elisdn\compositeForm\CompositeForm;
use yii\helpers\VarDumper;

/**
 * Форма валидации информации о путешествии водителя:
 * Откуда, куда, когда, какая машина, сколько попутчиков
 *
 * @property SimpleTripForm $simpleTrip
 */
class AddDriverTripForm extends CompositeForm
{
    public $carMarkId;
    public $carModelId;
    public $carYear;
    public $tripCapacity;
    public $price;

    /**
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->simpleTrip = new SimpleTripForm();
        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['carMarkId', 'carModelId', 'carYear'], 'required'],
            [['carMarkId'], 'exist', 'targetClass' => CarMark::class, 'targetAttribute' => 'id'],
            [['carModelId'], 'exist', 'targetClass' => CarModel::class, 'targetAttribute' => 'id'],
            [['carYear', 'tripCapacity', 'price'], 'integer'],
            [['tripCapacity'], 'myValidator']
        ];
    }

    public function attributeLabels()
    {
        return [
            'carMarkId' => 'Марка автомобиля',
            'carModelId' => 'Модель автомобиля',
            'carYear' => 'Год выпуска',
            'tripCapacity' => 'Мест для пассажиров',
            'price' => 'Цена за одного пассажира',
        ];
    }

    /**
     * @inheritdoc
     */
    protected function internalForms()
    {
        return ['simpleTrip'];
    }

    public function myValidator($attribute, $params)
    {
        if (\Yii::$app->user->isGuest && $this->tripCapacity > 3 || \Yii::$app->user->identity->role == User::ROLE_USER && $this->tripCapacity > 3) {
            $this->addError('tripCapacity', 'Указывать более 3 посадочных мест возможно только для аккаунтов коммерческих перевозчиков');
        }
    }
}
