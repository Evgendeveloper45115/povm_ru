<?php

namespace app\companion\repository;

use app\companion\model\Car;
use RuntimeException;

class CarRepository
{
    /**
     * @param Car $car
     */
    public function save(Car $car): void
    {
        if (!$car->save()) {
            throw new RuntimeException('Ошибка сохранения');
        }
    }
}
