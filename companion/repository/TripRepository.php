<?php

namespace app\companion\repository;

use app\companion\model\Trip;
use app\repositories\NotFoundException;
use RuntimeException;
use yii\db\ActiveRecord;

class TripRepository
{
    /**
     * @param int $id
     * @return Trip
     */
    public function get(int $id): Trip
    {
        return $this->getBy(['id' => $id]);
    }

    /**
     * @param array $condition
     * @return Trip|ActiveRecord
     */
    private function getBy(array $condition): Trip
    {
        if (!$user = Trip::find()->andWhere($condition)->limit(1)->one()) {
            throw new NotFoundException('Поездка не найдена');
        }
        return $user;
    }

    /**
     * @param Trip $trip
     */
    public function save(Trip $trip): void
    {
        if (!$trip->save()) {
            throw new RuntimeException('Ошибка сохранения');
        }
    }
}
