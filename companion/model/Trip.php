<?php

namespace app\companion\model;

use app\models\GeoTown;
use app\auth\models\User;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsTrait;
use yii\db\ActiveRecord;
use yii\helpers\VarDumper;

/**
 * @property int $id
 * @property int $type 1-ищу водителя, 2-ищу пассажира
 * @property int $user_id
 * @property string $trip_time
 * @property string $town_from
 * @property string $town_to
 * @property int $from_id
 * @property int $to_id
 * @property string $from_place
 * @property string $to_place
 * @property int $driver_id
 * @property int $passengers
 * @property string $description
 * @property int $price
 * @property int $periodic
 * @property int $car_id
 * @property int $views
 * @property int $status 0-неактивная, 10-активная, 20-законченная, -10-удалена
 * @property string $created_at
 *
 * @property GeoTown $from
 * @property GeoTown $to
 * @property Car $car
 * @property User $user
 */
class Trip extends ActiveRecord
{
    use SaveRelationsTrait;

    public const USER_TYPE_DRIVER = 1;
    public const USER_TYPE_PASSENGER = 2;

    public const STATUS_INACTIVE = 0;
    public const STATUS_ACTIVE = 10;
    public const STATUS_FINISHED = 20;
    public const STATUS_DELETED = -10;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%trip}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['car', 'user'], 'safe'],
            [['trip_time', 'description', 'passengers', 'price', 'car_id'], 'safe'],
            [['passengers'], 'myValidator']

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'user_id' => 'User ID',
            'trip_time' => 'Дата поездки',
            'town_from' => 'Town From',
            'town_to' => 'Town To',
            'from_id' => 'From ID',
            'to_id' => 'To ID',
            'from_place' => 'From Place',
            'to_place' => 'To Place',
            'driver_id' => 'Driver ID',
            'passengers' => 'Количество свободных мест',
            'description' => 'Комментарий к поездке',
            'price' => 'Цена за место',
            'periodic' => 'Periodic',
            'car_id' => 'Авто',
            'views' => 'Views',
            'status' => 'Status',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return array
     */
    public static function getTypeList()
    {
        return [
            static::USER_TYPE_DRIVER => 'Ищу водителя',
            static::USER_TYPE_PASSENGER => 'Ищу пассажиров',
        ];
    }

    /**
     * @return array
     */
    public static function getStatusList()
    {
        return [
            static::STATUS_INACTIVE => 'Неактивная',
            static::STATUS_ACTIVE => 'Активная',
            static::STATUS_FINISHED => 'Завершенная',
            static::STATUS_DELETED => 'Архив',
        ];
    }

    /**
     * @param $type
     * @param $date
     * @param $status
     * @param GeoTown $from
     * @param GeoTown $to
     * @return Trip
     */
    public static function create($type, $date, $status, GeoTown $from, GeoTown $to): self
    {
        $trip = new self;
        $trip->type = $type;
        $trip->trip_time = $date;
        $trip->town_from = $from->name;
        $trip->from_id = $from->id;
        $trip->town_to = $to->name;
        $trip->to_id = $to->id;
        $trip->status = $status;

        return $trip;
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function myValidator($attribute, $params)
    {
        if (\Yii::$app->user->identity->role == User::ROLE_USER && $this->passengers > 3) {
            $this->addError('passengers', 'Указывать более 3 посадочных мест возможно только для аккаунтов коммерческих перевозчиков');
        }
    }

    public function addCar(Car $car)
    {
        $this->car = $car;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'saveRelations' => [
                'class' => SaveRelationsBehavior::class,
                'relations' => [
                    'car',
                    'user'
                ],
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFrom()
    {
        return $this->hasOne(GeoTown::class, ['id' => 'from_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTo()
    {
        return $this->hasOne(GeoTown::class, ['id' => 'to_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Car::class, ['id' => 'car_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public static function getStatusName($id)
    {
        $statusList = self::getStatusList();
        return (isset($statusList[$id])) ? $statusList[$id] : false;
    }
    public function getText($user, $comment)
    {
        return 'По вашей поездке ' . $this->town_from . ' - ' . $this->town_to . 'есть отклик от' . $user->name . 'Комментарий: ' . $comment;
    }

}
