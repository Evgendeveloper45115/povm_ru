<?php

namespace app\companion\model;

use app\auth\models\User;
use app\components\StringHelper;
use app\models\CarMark;
use app\models\CarModel;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsTrait;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\Html;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "car".
 *
 * @property int $id
 * @property int $mark_id
 * @property int $model_id
 * @property string $manufacturer
 * @property string $model
 * @property int $year
 * @property int $user_id
 * @property double $pollution
 * @property string $consumption
 * @property string $name
 * @property string $avatar
 * @property int $capacity
 * @property string $description
 * @property int $status
 * @property int $state_number
 *
 * @property User $user
 * @property CarMark $carMark
 * @property CarModel $carModel
 */
class Car extends ActiveRecord
{
    use SaveRelationsTrait;

    public const STATUS_INACTIVE = 0;
    public const STATUS_ACTIVE = 10;
    public const STATUS_DELETED = -10;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%car}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mark_id', 'model_id', 'year'], 'required'],
            [['year'], 'integer', 'min' => 1990, 'max' => (int)date('Y')],
            [['capacity'], 'integer', 'min' => 1],
            [['pollution', 'consumption'], 'number'],
            [['description', 'avatar'], 'string'],
            [['manufacturer', 'model'], 'string', 'max' => 50],
            [['name', 'state_number'], 'string', 'max' => 255],

            [['pollution', 'consumption'], 'default', 'value' => 0],
            [['user'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mark_id' => 'Марка',
            'model_id' => 'Модель',
            'year' => 'Год',
            'user_id' => 'User ID',
            'pollution' => 'Pollution',
            'consumption' => 'Consumption',
            'name' => 'Name',
            'capacity' => 'Количество мест',
            'description' => 'Описание',
            'state_number' => 'Гос. номер',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'manufacturer' => 'Например, Mini',
            'model' => 'Например, Cooper JCW',
            'year' => 'Например, 2010',
            'capacity' => 'Например, 3',
            'description' => 'Расскажите немного о своем авто – насколько он хорош?',
        ];
    }

    /**
     * @param int $carMarkId
     * @param int $carModelId
     * @param int $year
     * @param $user_id
     * @return Car
     */
    public static function create(int $carMarkId, int $carModelId, int $year, $user_id): self
    {

        $car = Car::findOne(
            [
                'user_id' => $user_id,
                'mark_id' => $carMarkId,
                'year' => $year,
                'model_id' => $carModelId,
            ]
        );
        if (!$car) {
            $car = new Car();
        }

        if ($car->isNewRecord) {
            $car->mark_id = $carMarkId;
            $car->model_id = $carModelId;
            $car->year = $year;
            $car->manufacturer = CarMark::findOne($carMarkId)->name;
            $car->model = CarModel::findOne($carModelId)->name;
            $car->pollution = 0;
            $car->consumption = 0.0;
            $car->user_id = $user_id;
            $car->status = self::STATUS_ACTIVE;
        }
        return $car;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'saveRelations' => [
                'class' => SaveRelationsBehavior::class,
                'relations' => [
                    'user',
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public static function getYears()
    {
        $currentYear = (int)date('Y');
        $startYear = $currentYear - Yii::$app->params['maxCarAge'] + 1;

        $range = range($currentYear, $startYear);

        return array_combine($range, $range);
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return Html::encode(StringHelper::mb_ucfirst($this->manufacturer)) . ' ' . Html::encode(StringHelper::mb_ucfirst($this->model));
    }


    /**
     * @return string
     */
    public function getContent()
    {
        $mark = $this->manufacturer;
        $model = $this->model;
        if ($this->carMark) {
            $mark = $this->carMark->name;
        }
        if ($this->carModel) {
            $model = $this->carModel->name;
        }
        return Html::encode(StringHelper::mb_ucfirst($mark)) . ' ' . Html::encode(StringHelper::mb_ucfirst($model));
    }

    /**
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCarMark()
    {
        return $this->hasOne(CarMark::class, ['id' => 'mark_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCarModel()
    {
        return $this->hasOne(CarModel::class, ['id' => 'model_id']);
    }
}
