<?php

namespace app\companion;

use app\companion\form\AddPassengerTripForm;
use app\companion\form\SimpleEmailForm;
use app\companion\storage\TripStorage;

class PassengerTrip
{
    const EVENT_USER_TYPE_SELECTED = 'trip.user_type_selected';
    const EVENT_ROUTE_INFO_FILLED = 'trip.route_info_filled';
    const EVENT_USER_ADDED = 'trip.user_added';
    const EVENT_SET_ACTIVE = 'trip.set_active';

    private $listeners = [];

    private $storage;
    private $trip;

    public function __construct(TripStorage $storage)
    {
        $this->storage = $storage;
    }

    public function getTrip(): array
    {
        return $this->loadTrip();
    }

    public function addTripInfo(AddPassengerTripForm $form): void
    {
        $trip = $this->loadTrip();
        $trip = array_merge($trip, $form->toArray(), ['simpleTrip' => $form->simpleTrip->toArray()]);
        $this->saveTrip($trip);
    }

    public function addTripEmail(SimpleEmailForm $form): void
    {
        $trip = $this->loadTrip();
        $trip = array_merge($trip, $form->toArray(), ['email' => $form->email]);
        $this->saveTrip($trip);
    }

    public function addUser(int $userId): void
    {
        $trip = $this->loadTrip();
        $trip['userId'] = $userId;
        $this->saveTrip($trip);
    }

    public function setStep(int $step): void
    {
        $trip = $this->loadTrip();
        $trip['step'] = $step;
        $this->saveTrip($trip);
    }

    public function clean(): void
    {
        $this->saveTrip([]);
    }

    protected function loadTrip(): array
    {
        if ($this->trip === null) {
            $this->trip = $this->storage->get();
        }
        return $this->trip;
    }

    protected function saveTrip(array $trip): void
    {
        $this->storage->put($trip);
        $this->trip = $trip;
    }

    /**
     * @param $name
     * @param callable $listener
     */
    public function addEventListener($name, callable $listener): void
    {
        $this->listeners[$name][] = $listener;
    }

    /**
     * @param $name
     * @param callable $listener
     */
    public function removeEventListener($name, callable $listener): void
    {
        if (array_key_exists($name, $this->listeners)) {
            foreach ($this->listeners[$name] as $i => $current) {
                if ($current === $listener) {
                    unset($this->listeners[$i]);
                }
            }
        }
    }

    /**
     * @param $name
     * @param $event
     */
    private function trigger($name, $event): void
    {
        if (array_key_exists($name, $this->listeners)) {
            foreach ($this->listeners[$name] as $i => $listener) {
                $listener($event);
            }
        }
    }
}
