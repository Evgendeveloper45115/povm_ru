<?php

namespace app\companion\services;

use app\auth\models\User;
use app\companion\model\Trip;

class TripActivateService
{
    /**
     * @param User $user
     * @return User
     */
    public function activateTripsForUser(User $user): User
    {
        $trips = $user->inactiveTrips;
        foreach ($trips as $trip) {
            $trip->status = Trip::STATUS_ACTIVE;
        }
        $user->inactiveTrips = $trips;
        return $user;
    }
}
