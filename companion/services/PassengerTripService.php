<?php

namespace app\companion\services;

use app\auth\models\User;
use app\companion\form\AddPassengerTripForm;
use app\companion\model\Trip;
use app\models\GeoTown;

class PassengerTripService
{
    /**
     * @param User $user
     * @param AddPassengerTripForm $form
     * @param int $status
     * @return User
     */
    public function createPassengerTrip(User $user, AddPassengerTripForm $form, int $status): User
    {
        $dateTo = \DateTime::createFromFormat('d.m.Y H:i', str_replace('+', ' ', $form->simpleTrip->dateTo))
            ->format('Y-m-d H:i:s');

        $trip = Trip::create(
            Trip::USER_TYPE_DRIVER,
            $dateTo,
            $status,
            GeoTown::findOne($form->simpleTrip->fromLocationId),
            GeoTown::findOne($form->simpleTrip->toLocationId)
        );

        $trip->from_place = $form->simpleTrip->fromPlace ?? null;
        $trip->to_place = $form->simpleTrip->toPlace ?? null;

        if ($form->simpleTrip->wayBack) {

            $dateBack = \DateTime::createFromFormat('d.m.Y H:i', str_replace('+', ' ', $form->simpleTrip->dateBack))
                ->format('Y-m-d H:i:s');

            $tripBack = Trip::create(
                Trip::USER_TYPE_DRIVER,
                $dateBack,
                $status,
                GeoTown::findOne($form->simpleTrip->toLocationId),
                GeoTown::findOne($form->simpleTrip->fromLocationId)
            );

            $tripBack->to_place = $form->simpleTrip->fromPlace ?? null;
            $tripBack->from_place = $form->simpleTrip->toPlace ?? null;

            $user->addTrip($tripBack);
        }

        $user->addTrip($trip);

        return $user;
    }
}
