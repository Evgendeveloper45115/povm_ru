<?php

namespace app\companion\services;

use app\auth\models\User;
use app\companion\form\AddDriverTripForm;
use app\companion\model\Car;
use app\companion\model\Trip;
use app\components\Dumper;
use app\models\GeoTown;
use yii\helpers\VarDumper;

class DriverTripService
{
    /**
     * @param User $user
     * @param AddDriverTripForm $form
     * @param int $status
     * @return User
     */
    public function createDriverTrip(User $user, AddDriverTripForm $form, int $status): User

    {
        $dateTo = \DateTime::createFromFormat('d.m.Y H:i', str_replace('+', ' ', $form->simpleTrip->dateTo))
            ->format('Y-m-d H:i:s');

        $car = Car::create((int)$form->carMarkId, (int)$form->carModelId, (int)$form->carYear, $user->id);
        $car->capacity = $form->tripCapacity ?? null;
        $trip = Trip::create(
            Trip::USER_TYPE_PASSENGER,
            $dateTo,
            $status,
            GeoTown::findOne($form->simpleTrip->fromLocationId),
            GeoTown::findOne($form->simpleTrip->toLocationId)
        );

        $trip->from_place = $form->simpleTrip->fromPlace ?? null;
        $trip->to_place = $form->simpleTrip->toPlace ?? null;
        $trip->passengers = $form->tripCapacity ?? null;
        $trip->price = (int)$form->price ?? (int)0;
        $trip->description = $form->simpleTrip->description ?? null;

        if ($form->simpleTrip->wayBack) {

            $dateBack = \DateTime::createFromFormat('d.m.Y H:i', str_replace('+', ' ', $form->simpleTrip->dateBack))
                ->format('Y-m-d H:i:s');

            $tripBack = Trip::create(
                Trip::USER_TYPE_PASSENGER,
                $dateBack,
                $status,
                GeoTown::findOne($form->simpleTrip->toLocationId),
                GeoTown::findOne($form->simpleTrip->fromLocationId)
            );

            $tripBack->to_place = $form->simpleTrip->fromPlace ?? null;
            $tripBack->from_place = $form->simpleTrip->toPlace ?? null;
            $tripBack->passengers = $form->tripCapacity ?? null;
            $tripBack->price = (int)$form->price ?? (int)0;
            $tripBack->description = $form->simpleTrip->description ?? null;

            $tripBack->addCar($car);
            $user->addTrip($tripBack);
        }

        $trip->addCar($car);
        $user->addTrip($trip);

        return $user;
    }
}
