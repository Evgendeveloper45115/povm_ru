<?php

namespace app\companion;

interface TripInterface
{
    public function getId(): string;
}
