<?php

namespace app\companion\storage;

use InvalidArgumentException;
use Yii;
use yii\helpers\Json;
use yii\web\Cookie;

class CookieStorage
{
    private $name;
    private $timeout;

    public function __construct($name, $timeout)
    {
        if (empty($name)) {
            throw new InvalidArgumentException('Уточните ключ');
        }
        $this->name = $name;
        $this->timeout = $timeout;
    }

    public function get(): array
    {
        $cookie = Yii::$app->request->cookies->get($this->name);
        return $cookie ? Json::decode($cookie->value) : [];
    }

    /**
     * @param array $trip
     */
    public function put(array $trip): void
    {
        Yii::$app->response->cookies->add(new Cookie([
            'name' => $this->name,
            'value' => Json::encode($trip),
            'expire' => time() + $this->timeout,
        ]));
    }
}
