<?php

namespace app\companion\storage;

interface TripStorage
{
    /**
     * @return array
     */
    public function get(): array;

    /**
     * @param array $trip
     */
    public function put(array $trip): void;
}
