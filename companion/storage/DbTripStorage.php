<?php

namespace app\companion\storage;

use yii\db\Query;

class DbTripStorage implements TripStorage
{
    private $userId;

    public function __construct($userId)
    {
        $this->userId = $userId;
    }

    public function get(): array
    {
        $trip = (new Query())
            ->select('*')
            ->from('{{%trip}} t')
            ->where(['user_id' => $this->userId])
            ->where(['status' => 'не законченный'])
            ->all();

        // отдаём незаконченное объявление
        return [];
    }

    public function put(array $trip): void
    {
        // сохраняем объявление, даже незаконченное
    }
}
