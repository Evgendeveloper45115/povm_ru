<?php

namespace app\companion\storage;

use yii\web\User;

class HybridTripStorage implements TripStorage
{
    private $storage;
    private $cookieName;
    private $cookieTimeout;
    private $user;

    public function __construct(User $user, $cookieName, $cookieTimeout)
    {
        $this->cookieName = $cookieName;
        $this->cookieTimeout = $cookieTimeout;
        $this->user = $user;
    }

    public function get(): array
    {
        return $this->getStorage()->get();
    }

    public function put(array $items): void
    {
        $this->getStorage()->put($items);
    }

    private function getStorage()
    {
        if ($this->storage === null) {
            $cookieStorage = new CookieStorage($this->cookieName, $this->cookieTimeout);
            $this->storage = $cookieStorage;
        }
        return $this->storage;
    }
}
