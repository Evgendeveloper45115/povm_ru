<?php

namespace app\companion\storage;

use InvalidArgumentException;
use Yii;

class SessionTripStorage implements TripStorage
{
    private $key;

    public function __construct($key)
    {
        if (empty($key)) {
            throw new InvalidArgumentException('Уточните ключ сессии');
        }
        $this->key = $key;
    }

    public function get(): array
    {
        return Yii::$app->session->get($this->key, []);
    }

    public function put(array $trip): void
    {
        Yii::$app->session->set($this->key, $trip);
    }
}
