<?php

namespace app\controllers;

use app\auth\services\UserTripsManageService;
use app\feedback\MailService;
use app\models\ContactForm;
use app\models\GeoTown;
use app\auth\forms\LoginForm;
use app\auth\forms\RegistrationForm;
use app\auth\forms\RestorePasswordForm;
use app\models\Message;
use app\models\SearchForm;
use app\auth\models\User;
use app\models\Transaction;
use Yii;
use yii\base\Exception;
use yii\base\Module;
use yii\captcha\CaptchaAction;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\ErrorAction;
use yii\web\HttpException;
use yii\web\Response;

/**
 * Class SiteController
 * @package app\controllers
 */
class SiteController extends Controller
{

    /** @var MailService */
    private $mailService;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['balance-add-request'],
                        'allow' => true,
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                    'town' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => ErrorAction::class,
            ],
            'captcha' => [
                'class' => CaptchaAction::class,
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function __construct(
        string $id,
        Module $module,
        MailService $mailService,
        array $config = []
    )
    {
        parent::__construct($id, $module, $config);
        $this->mailService = $mailService;
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @return array
     */
    public function actionTown()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        return GeoTown::findByTerm(Yii::$app->request->post('query'));
    }

    /**
     * @return array
     */
    public function actionTownRegionCountry()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        return GeoTown::findByTermTownRegionCountry(Yii::$app->request->post('query'));
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            if (Yii::$app->getUser()->getIdentity()->role == User::ROLE_ADMIN) {
                return $this->redirect(['/admin/']);
            }
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * @return string
     * @throws Exception
     */
    public function actionRestorePassword()
    {
        $model = new RestorePasswordForm();

        if ($model->load(Yii::$app->request->post()) && $model->resetPassword() && $model->sendLetter()) {
            return $this->render('restore-letter-sent');
        }

        return $this->render('restore-password', [
            'model' => $model,
        ]);
    }

    /**
     * @return string
     * @throws Exception
     */
    public function actionRegister()
    {
        $model = new RegistrationForm();
        if ($model->load(Yii::$app->request->post())) {
            $model->setScenario(User::ROLE_USER);
            if ($model->role == User::ROLE_CARRIER) {
                $model->setScenario(User::ROLE_CARRIER);
            }
            $user = $model->addAttributes();
            if ($model->validate() && $user->save(false)) {
                $this->mailService->sendConfirmEmail($user);
                Yii::$app->session->setFlash('success', 'На адрес ' . $user->email . ' отправлено письмо с инструкцией для подтверждения регистрации');
                return $this->redirect(['/site/index']);
            } else {
                VarDumper::dump(Yii::$app->request->post(), 11, 1);
                VarDumper::dump($model->getErrors(), 11, 1);
            }
        }

        return $this->render('register', [
            'model' => $model,
        ]);
    }

    /**
     * @param string $email
     * @param string $code
     * @return Response
     */
    public function actionConfirm($email, $code)
    {
        $user = User::findOne([
            'email' => $email,
            'confirm_code' => $code,
            'status' => User::STATUS_INACTIVE
        ]);

        if ($user === null) {
            Yii::$app->session->setFlash('error', 'Пользователь с такими данными не найден, либо не требует активации');
            return $this->redirect(['index']);
        }

        if (UserTripsManageService::confirm($user)) {
            Yii::$app->session->setFlash('success', 'Ваш аккаунт активирован');
            Yii::$app->user->login($user);
            return $this->redirect(['/profile/view']);
        }

        Yii::$app->session->setFlash('error', 'Возникла непредвиденная ошибка. Обратитесь в службу поддержки');
        return $this->redirect(['index']);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionFeedback()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('feedback', [
            'model' => $model,
        ]);
    }

    /**
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * @return string
     */
    public function actionSafety()
    {
        return $this->render('safety');
    }

    /**
     * @return string
     */
    public function actionRules()
    {
        return $this->render('rules');
    }

    /**
     * @return string
     */
    public function actionPartners()
    {
        return $this->render('partners');
    }

    public function actionOffer()
    {
        return $this->render('offer');
    }


    /**
     * На этот адрес будут приходить запросы от Яндекса о пополнении кошелька
     * https://povm.ru/user/balanceAddRequest
     */
    public function actionBalanceAddRequest()
    {

        Yii::info('Пришел запрос от Яндекса с уведомлением о пополнении баланса', 'info');
        Yii::info('POST запрос: ' . print_r($_POST, true), 'info');
        $request = Yii::$app->request;

        // разбираем данные, которые пришли от Яндекса
        $amount = $request->post('withdraw_amount') * 100;
        $userId = $request->post('label');
        $hash = $request->post('sha1_hash');

        $secret = Yii::$app->params['yandexMoneySecret'];

        $requestString = $request->post('notification_type') . '&' .
            $request->post('operation_id') . '&' .
            $request->post('amount') . '&' .
            $request->post('currency') . '&' .
            $request->post('datetime') . '&' .
            $request->post('sender') . '&' .
            $request->post('codepro') . '&' .
            $secret . '&' .
            $request->post('label');

        Yii::info('Собранная строка для проверки: ' . $requestString, 'info');

        $requestString = sha1($requestString);

        Yii::info('Зашифрованная строка для проверки: ' . $requestString, 'info');

        if ($requestString == $hash) {
            // данные от яндекса не подделаны, можно зачислять бабло

            $user = User::findOne($userId);

            if ($user instanceof User) {

                Yii::info('Пополняем баланс пользователя: ' . $user->name, 'info');
                $user->balance += $amount;
                $transaction = new Transaction();
                $transaction->user_id = $user->id;
                $transaction->amount = $amount;
                $transaction->description = 'Пополнение баланса пользователя';


                if ($transaction->save() && $user->save()) {
                    Yii::info('Транзакция сохранена, id: ' . $transaction->id, 'info');
                } else {
                    Yii::error('Ошибка при пополнении баланса пользователя ' . $user->id . ' (' . $amount . ' руб.)', 'error');
                    throw new HttpException(500, 'Не удалось пополнить баланс пользователя');
                }

                Yii::info('Пришло бабло от пользователя ' . $user->id . ' (' . $amount . ' руб.)', 'info');
            }
        }
    }
}
