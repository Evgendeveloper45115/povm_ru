<?php

namespace app\controllers;

use app\auth\models\User;
use app\feedback\MailService;
use app\models\Dialog;
use app\models\DialogHasUser;
use app\models\Message;
use app\models\MessageSearch;
use app\models\TransactionSearch;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

class UserController extends Controller
{

    /** @var MailService */
    private $mailService;

    public function __construct(
        string $id,
        Module $module,
        MailService $mailService,
        array $config = []
    )

    {
        parent::__construct($id, $module, $config);
        $this->mailService = $mailService;
    }


    /**
     * Список Финансов клиента
     */
    public function actionBalance()
    {
        $searchModel = new TransactionSearch();
        $dataProvider = $searchModel->search();
        return $this->render('balance', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function beforeAction($action)
    {
        if (\Yii::$app->user->isGuest) {
            \Yii::$app->session->setFlash('error', 'Отказ доступа');
            return $this->redirect('/site/login');
        }
        return parent::beforeAction($action);
    }
}
