<?php

namespace app\controllers\auth;

use app\auth\models\User;
use app\auth\repositories\UserRepository;
use app\auth\services\SignUpService;
use app\companion\model\Trip;
use app\companion\services\TripActivateService;
use app\feedback\MailService;
use app\models\Dialog;
use app\models\DialogHasUser;
use app\models\Message;
use app\models\Respond;
use app\repositories\NotFoundException;
use Yii;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\Response;

class AuthController extends Controller
{
    private $signUpService;
    private $userRepository;
    private $userService;
    /** @var MailService */
    private $mailService;

    public function __construct
    (
        $id,
        $module,
        UserRepository $userRepository,
        MailService $mailService,
        SignUpService $signUpService,
        TripActivateService $userService,
        $config = []
    )
    {
        parent::__construct($id, $module, $config);
        $this->signUpService = $signUpService;
        $this->userRepository = $userRepository;
        $this->mailService = $mailService;
        $this->userService = $userService;
    }

    /**
     * @param string $code
     * @return Response
     */
    public function actionConfirm($code)
    {

        /**
         * @var $respond Respond
         */
        try {
            $user = $this->userRepository->getByConfirmCode($code);
            $user = $this->signUpService->confirm($user);
            $this->userRepository->save($user);
            $user = $this->userService->activateTripsForUser($user);
            $this->userRepository->save($user);
            Yii::$app->session->setFlash('success', 'Ваш аккаунт активирован');
            Yii::$app->user->login($user);
            $respond = Respond::find()->where(['user_id' => $user->id, 'status' => 0])->one();
            if ($respond) {
                $trip = Trip::findOne($respond->trip_id);
                $dialog = new Dialog();
                $dialog->save();
                $message = new Message();
                $message->messageFrom = $user->id;
                $message->messageTo = $trip->user_id;
                $message->dialog_id = $dialog->id;
                $message->text = $this->getText($trip, $user, $respond->comment);
                $message->datetime = date('Y-m-d H:i:s');
                $message->viewed = 0;
                if ($message->save()) {
                    $DHU = new DialogHasUser();
                    $DHU->user_id = $user->id;
                    $DHU->dialog_id = $dialog->id;
                    if ($DHU->save()) {
                        $DHU = new DialogHasUser();
                        $DHU->user_id = $trip->user->id;
                        $DHU->dialog_id = $dialog->id;
                        if ($DHU->save()) {
                            $respond->status = true;
                            $respond->save(false);
                            Yii::$app->session->setFlash('success', 'Отклик успешно отправлен!');
                        }
                    }
                }
            }
            return $this->redirect(['/profile/view']);
        } catch (NotFoundException $e) {
            Yii::$app->session->setFlash('error', 'Пользователь с такими данными не найден');
            return $this->redirect(['index']);
        } catch (\Exception $e) {
            Yii::$app->session->setFlash('error', 'Возникла непредвиденная ошибка. Обратитесь в службу поддержки');
            return $this->redirect(['index']);
        }
    }

    protected function getText($trip, $user, $comment)
    {
        return $this->renderPartial('/trip/text', ['trip' => $trip, 'user' => $user, 'comment' => $comment]);
    }

    /**
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
