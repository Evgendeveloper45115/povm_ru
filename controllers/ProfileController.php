<?php

namespace app\controllers;


use app\companion\model\Trip;
use app\components\Dumper;
use app\auth\models\User;
use Yii;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * Class ProfileController
 * @package app\controllers
 */
class ProfileController extends Controller
{

    /**
     * @param int|null $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id = null)
    {
        if (!$id && Yii::$app->user->isGuest) {
            throw new NotFoundHttpException();
        }
        if ($id === null) {
            $id = (int)Yii::$app->user->id;
        }


        $model = $this->findModel($id);
        $file = UploadedFile::getInstance($model, 'avatar');
        $dir = Yii::getAlias('@webroot/img/profile/');
        if (!is_dir($dir)) {
            mkdir($dir);
        }
        if ($file) {
            if (is_file($dir . $model->avatar)) {
                unlink($dir . $model->avatar);
            }
            $fname = uniqid("", true) . '.' . $file->extension;
            $model->avatar = $fname;
            $file->saveAs($dir . $fname);
            if ($model->save(false)) {
                return $this->redirect(Url::to(['/user/']));
            }
        }

        return $this->render('view', [
            'model' => $model,
            'id' => $id,
        ]);
    }

    public function actionBlock()
    {

    }

    public function actionUpdate()
    {
        /* @var User $user */
        $user = Yii::$app->user->identity;
        if ($user->load(Yii::$app->request->post()) && $user->save()) {
            Yii::$app->session->setFlash('success', 'Данные успешно сохранены');
            return $this->redirect(Url::to(['/user/']));
        }
        return $this->render('update', ['user' => $user]);
    }

    /**
     * @return \yii\web\Response
     * Удаление данных о клиенте и перевод в архив всех его поездок в случае удаления пользователем своего профиля
     */
    public function actionDelete()
    {
        /* @var User $user */
        $user = Yii::$app->user->identity;
        $user->status = User::STATUS_DELETED;
        foreach ($user->trips as $trip) {
            $trip->status = Trip::STATUS_DELETED;
            $trip->save();
        }
        $user->save();
        Yii::$app->session->setFlash('success', 'Вы успешно удалили свой аккаунт');
        return $this->redirect('/');
    }

    /**
     * @param $id
     * @return User
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $model = User::find()->where(['id' => $id, 'status' => User::STATUS_ACTIVE])->one();

        if ($model === null) {
            throw new NotFoundHttpException();
        }

        return $model;
    }

    /**
     * @return string
     * Смена пароля клиента
     */
    public function actionChangePassword()
    {
        /* @var User $user */
        $user = Yii::$app->user->identity;
        $user->scenario = User::SCENARIO_CHANGE_PASSWORD_USER;
        if ($user->load(Yii::$app->request->post()) && $user->validate()) {
            $user->password_hash = Yii::$app->security->generatePasswordHash($user->new_password);
            if ($user->save(false)) {
                Yii::$app->session->setFlash('success', 'Вы успешно изменили пароль!');
                return $this->redirect('/user/');
            }
        }
        return $this->render('change-password', ['user' => $user]);
    }

}
