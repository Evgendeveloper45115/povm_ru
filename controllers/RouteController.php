<?php

namespace app\controllers;

use app\auth\models\User;
use app\companion\model\Trip;
use app\feedback\MailService;
use app\models\Dialog;
use app\models\DialogHasUser;
use app\models\GeoTown;
use app\models\Message;
use app\models\MessageSearch;
use app\models\RouteQueryObject;
use app\models\SearchLogTrip;
use app\models\TripSearch;
use yii\base\Module;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

class RouteController extends Controller
{

    /** @var MailService */
    private $mailService;

    public function __construct(
        string $id,
        Module $module,
        MailService $mailService,
        array $config = []
    )
    {
        parent::__construct($id, $module, $config);
        $this->mailService = $mailService;
    }


    /**
     * Каталог Букв
     * @return string
     * @throws \yii\db\Exception
     */
    public function actionIndex()
    {
        if (\Yii::$app->session->get('typeFind')) {
            \Yii::$app->session->remove('typeFind');
        }
        $letters = RouteQueryObject::getFirstLettersOfTownsFrom();
        $trips = [];

        foreach ($letters as $letter) {
            $trips[$letter] = RouteQueryObject::getRoutesByFirstLetter($letter, 5, 3600);
        }
        return $this->render('index', [
            'letters' => $letters,
            'trips' => $trips,
        ]);
    }

    /**
     * Список поездок по букве пункта отправления
     * @param string $letter
     */
    public function actionRoutes($letter)
    {
        $letter = mb_substr($letter, 0, 1, 'utf-8');
        $trips = RouteQueryObject::getRoutesByFirstLetter($letter, 5000, 0);

        return $this->render('routes', array(
            'trips' => $trips,
            'letter' => $letter,
        ));
    }

    /**
     * Страница конкретного маршрута
     * @param string $cityFrom
     * @param string $cityTo
     * @return string
     * @throws HttpException
     */
    public function actionView($cityFrom = null, $cityTo = null)
    {
        // поиск поездок..

        $type = null;
        if (!$cityFrom && !$cityTo) {
            $post = \Yii::$app->request->post();
            if (isset($post['driver'])) {
                $type = Trip::USER_TYPE_PASSENGER;
            } elseif (isset($post['passenger'])) {
                $type = Trip::USER_TYPE_DRIVER;
            }
            $post = \Yii::$app->request->post('SearchForm');
            $cityFrom = $post['from'];
            $cityTo = $post['to'];
            $from = GeoTown::getById($cityFrom);
            $to = GeoTown::getById($cityTo);
            if (!$from && $to) {
                RouteQueryObject::updateCreateSearchLogByFromTo(null, $cityTo, null, $to->name);
                return $this->redirect([$to->country->alias . '/' . $to->region->alias . '/' . $to->alias]);
            } elseif ($from && !$to) {
                RouteQueryObject::updateCreateSearchLogByFromTo($cityFrom, null, $from->name, null);
                return $this->redirect([$from->country->alias . '/' . $from->region->alias . '/' . $from->alias]);
            } elseif (!$from && !$to) {
                \Yii::$app->session->setFlash('error', 'Заполните поля поиска');
                return $this->redirect(Url::to(['/']));
            } else {
                RouteQueryObject::updateCreateSearchLogByFromTo($cityFrom, $cityTo, $from->name, $to->name);
            }
            \Yii::$app->session->set('typeFind', $type);
            return $this->redirect(Url::to(['route/view', 'cityFrom' => $from->alias, 'cityTo' => $to->alias]));
        } else {
            $from = GeoTown::getByAlias($cityFrom);
            $to = GeoTown::getByAlias($cityTo);
        }
        if (is_null($from) || is_null($to)) {
            throw new HttpException(404, 'Маршрут не найден');
        }
        if (\Yii::$app->session->get('typeFind')) {
            $type = \Yii::$app->session->get('typeFind');
        }
        return $this->render('view', array(
            'from' => $from,
            'to' => $to,
            'type' => $type
        ));

    }
}
