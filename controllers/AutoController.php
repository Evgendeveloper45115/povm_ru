<?php

namespace app\controllers;


use app\companion\model\Car;
use app\models\CarMark;
use app\models\CarModel;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

class AutoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['create', 'update', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                    'models' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', [

        ]);
    }

    /**
     * @return string
     */
    public function actionCreate()
    {
        $model = new Car();

        if ($model->load(Yii::$app->request->post())) {
            $model->manufacturer = CarMark::findOne($model->mark_id)->name;
            $model->model = CarModel::findOne($model->model_id)->name;
            $model->user_id = Yii::$app->getUser()->getId();
            $file = UploadedFile::getInstance($model, 'avatar');
            $dir = Yii::getAlias('@webroot/img/car/');
            if (!is_dir($dir)) {
                mkdir($dir);
            }
            if ($file) {
                if (is_file($dir . $model->avatar)) {
                    unlink($dir . $model->avatar);
                }
                $fname = uniqid("", true) . '.' . $file->extension;
                $model->avatar = $fname;
                $file->saveAs($dir . $fname);
            }
            if ($model->save()) {
                return $this->redirect('/profile/view');
            }
        }

        return $this->render('form', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findUserModel($id);
        $avatar = $model->avatar;
        if ($model->load(Yii::$app->request->post())) {
            $file = UploadedFile::getInstance($model, 'avatar');
            $dir = Yii::getAlias('@webroot/img/car/');
            if (!is_dir($dir)) {
                mkdir($dir);
            }
            if ($file) {
                if (is_file($dir . $model->avatar)) {
                    unlink($dir . $model->avatar);
                }
                $fname = uniqid("", true) . '.' . $file->extension;
                $model->avatar = $fname;
                $file->saveAs($dir . $fname);
            } else {
                $model->avatar = $avatar;
            }
            if ($model->save()) {
                return $this->redirect('/profile/view');
            }
        }

        return $this->render('form', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {

        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->findUserModel($id);
        $model->status = Car::STATUS_DELETED;
        $model->save();

        return $this->redirect('/profile/view');
    }

    public function actionModels()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $id = Yii::$app->request->post('id');
        if (!$id) {
            return [];
        }

        return CarModel::find()->select(['id', 'name'])->where(['mark_id' => $id])->orderBy(['name' => SORT_ASC])->asArray()->all();
    }

    /**
     * @param $id
     * @return \app\companion\model\Car
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $model = Car::find()->with(['user'])->where(['id' => $id, 'status' => Car::STATUS_ACTIVE])->one();

        if ($model === null) {
            throw new NotFoundHttpException();
        }

        return $model;
    }

    /**
     * @param $id
     * @return \app\companion\model\Car
     * @throws NotFoundHttpException
     */
    protected function findUserModel($id)
    {
        $model = Car::find()->where(['id' => $id, 'user_id' => Yii::$app->user->id, 'status' => Car::STATUS_ACTIVE])->one();

        if ($model === null) {
            throw new NotFoundHttpException();
        }

        return $model;
    }
}
