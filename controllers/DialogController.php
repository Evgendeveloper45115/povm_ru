<?php

namespace app\controllers;

use app\auth\models\User;
use app\feedback\MailService;
use app\models\Dialog;
use app\models\DialogHasUser;
use app\models\Message;
use app\models\MessageSearch;
use yii\base\Module;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

class DialogController extends Controller
{

    /** @var MailService */
    private $mailService;

    public function __construct(
        string $id,
        Module $module,
        MailService $mailService,
        array $config = []
    )
    {
        parent::__construct($id, $module, $config);
        $this->mailService = $mailService;
    }


    /**
     * Список сообщений группированые по диалогам
     */
    public function actionIndex()
    {
        $searchModel = new MessageSearch();
        $dataProviderIn = $searchModel->search(\Yii::$app->request->queryParams);
        return $this->render('index', array(
            'dataProvider' => $dataProviderIn,
        ));
    }

    public function beforeAction($action)
    {
        if (\Yii::$app->user->isGuest && $action->id != 'send-view') {
            \Yii::$app->session->setFlash('error', 'Отказ доступа');
            return $this->redirect('/site/login');
        }
        return parent::beforeAction($action);
    }

    /**
     * @param $dialog
     * @return string|\yii\web\Response
     */
    public function actionView($dialog)
    {
        /** @var $user User */
        /** @var $messageTo DialogHasUser */
        if ($this->getAccess($dialog)) {
            $user = \Yii::$app->user->identity;
            $searchModel = new MessageSearch();
            $dataProvider = $searchModel->searchView($dialog);
            $messageTo = DialogHasUser::find()
                ->where(['dialog_id' => $dialog])
                ->andWhere(['!=', 'user_id', $user->id])
                ->one();
            Message::updateAll(['viewed' => 1], ['dialog_id' => $dialog, 'messageTo' => $user->id, 'viewed' => 0]);
            $model = new Message();
            if ($model->load(\Yii::$app->request->post())) {
                $model->messageTo = $messageTo->user_id;
                $model->messageFrom = $user->id;
                $model->viewed = 0;
                $model->datetime = date('y-m-d H:i:s');
                $model->dialog_id = $dialog;
                if ($model->save() && $this->mailService->sendNewMessage(User::findOne($messageTo->user_id), $model->text)) {
                    \Yii::$app->session->setFlash('success', 'Сообщение успешно отправлено!');
                    return $this->redirect(Url::to(['/dialog/view/', 'dialog' => $dialog]));
                }
            }
        } else {
            \Yii::$app->session->setFlash('error', 'Отказ в доступе');
            return $this->redirect(Url::to(['/dialog/']));
        }
        return $this->render('view', array(
            'dataProvider' => $dataProvider,
            'sender' => $messageTo
        ));
    }

    /**
     * @param $dialog_id
     * @return bool|\yii\web\Response
     */
    private function getAccess($dialog_id)
    {
        /** @var $user User */
        $user = \Yii::$app->user->identity;
        $DHU = DialogHasUser::findOne(['dialog_id' => $dialog_id, 'user_id' => $user->id]);
        if (!$DHU) {
            return false;

        }
        return true;
    }

    /**
     * @param $email
     * @param $code
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionSendView($email, $code)
    {
        $model = User::findOne(['email' => $email, 'confirm_code' => $code]);
        if ($model) {
            \Yii::$app->user->login($model);
            return $this->redirect(Url::to(['/dialog']));
        } else {
            throw new NotFoundHttpException('Email или код активации не верный!');
        }
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * Отправка сообщения с профиля
     */
    public function actionSend($id)
    {
        $modelItem = Message::findOne(['messageFrom' => \Yii::$app->user->id, 'messageTo' => $id]);
        if ($modelItem) {
            return $this->redirect(Url::to(['/dialog/view', 'dialog' => $modelItem->dialog_id]));
        }
        $message = new Message();
        $dialog = new Dialog();
        $users = [\Yii::$app->user->identity, User::findOne($id)];
        if ($message->load(\Yii::$app->request->post())) {
            $dialog->save(false);
            $message->messageTo = $id;
            $message->messageFrom = \Yii::$app->user->id;
            $message->viewed = 0;
            $message->datetime = date('Y-m-d H:i:s');
            $message->dialog_id = $dialog->id;
            if ($message->save() && $this->saveUserMessage($users, $dialog->id) === true && $this->mailService->sendNewMessage($users[1], $message->text)) {
                \Yii::$app->session->setFlash('success', 'Сообщение успешно отправлено!');
                return $this->redirect(Url::to(['/dialog/view', 'dialog' => $dialog->id]));
            }
        }
        return $this->render('send', array(
            'model' => $message,
            'user' => $users[1],
        ));

    }

    /**
     * @param $users
     * @param $dialog_id
     * @return bool
     * @throws NotFoundHttpException
     */
    private function saveUserMessage($users, $dialog_id)
    {
        foreach ($users as $user) {
            $model = new DialogHasUser();
            $model->user_id = $user->id;
            $model->dialog_id = $dialog_id;
            if (!$model->save()) {
                throw new NotFoundHttpException();
            }
        }
        return true;
    }
}
