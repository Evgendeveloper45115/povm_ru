<?php

use yii\helpers\Html;

/* @var \yii\web\View $this */
/* @var \app\models\GeoRegion[]|\app\models\GeoTown[] $items*/

?>

<div class="geo-widget">
    <ul>
        <?php foreach ($items as $item) { ?>
            <li><?= Html::a(Html::encode($item->name), $item->getUrl()); ?></li>
        <?php } ?>
    </ul>
</div>