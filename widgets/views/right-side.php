<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $town \app\models\GeoTown */
/* @var $showCreateBtn bool */
/* @var $showVkGroup bool */
/* @var $showYaShare bool */
/* @var $showCars bool */
/* @var $showTown bool */

?>

<div class="right-side hidden-xs">
    <?php if ($showCreateBtn === true) { ?>
        <div>
            <?= Html::a('Найти попутчика!', ['/trips/create'], [
                'class' => 'btn btn-lg btn-default btn-block',
            ]); ?>
        </div>
    <?php } ?>

    <?php if ($showVkGroup === true) { ?>
        <div>
            <h3>Мы в VK</h3>
        </div>
    <?php } ?>

    <?php if ($showYaShare === true) { ?>
        <div>
            <h3>Расскажите друзьям</h3>
            <div id="ya-share">

            </div>
        </div>
    <?php } ?>

    <?php if ($showCars === true) { ?>
        <div>
            <h3>Авто наших водителей</h3>
        </div>
    <?php } ?>

    <?php if ($showTown === true) { ?>
        <div>
            <h3>город</h3>
        </div>
    <?php } ?>
</div>
