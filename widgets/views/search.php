<?php
/**
 * @var $searchForm \app\models\SearchForm
 */

use  \yii\widgets\ActiveForm;
use  \yii\helpers\Html;
use  \yii\helpers\Url;

?>
<ul class="nav nav-tabs">
    <li class="active in nav nav-tabs"><a href="#auto" data-toggle="tab"><i class="fa fa-car" aria-hidden="true"></i> На
            автомобиле</a></li>
    <li class="in nav nav-tabs"><a href="#vacation" data-toggle="tab"><i class="fa fa-plane" aria-hidden="true"></i> В
            отпуск заграницу</a></li>
</ul>

<!-- Содержимое вкладок -->
<div class="tab-content">
    <div class="tab-pane active" id="auto">
        <div class="search-form ">
            <div class="row">
                <div class="col-md-3 search-form-input">
                    <?php $form = ActiveForm::begin([
                        'action' => Url::to(['/route/view']),
                        'method' => 'post',
                        'validateOnSubmit' => false,
                        'options' => [
                            'id' => 'search-form',
                            'style' => 'margin: 0px 0',
                        ],
                    ]); ?>

                    <?= $form->field($searchForm, 'from')->dropDownList([], [
                        'prompt' => 'Откуда',
                        'class' => 'form-control select-town',
                        'data-language' => 'ru',
                        'style' => 'width:100% height:34px;',
                        'data' => [
                            'ajax--url' => Url::to(['/site/town-region-country'], true),
                        ],
                        'pluginOptions' => [
                            'autoclose' => true,
                        ]
                    ])->label(false) ?>
                </div>
                <div class="col-md-3 search-form-input">
                    <?= $form->field($searchForm, 'to')->dropDownList([], [
                        'prompt' => 'Куда',
                        'class' => 'form-control select-town',
                        'data-language' => 'ru',
                        'style' => 'width:100% height:34px;',
                        'data' => [
                            'ajax--url' => Url::to(['/site/town-region-country'], true),
                        ],
                        'pluginOptions' => [
                            'autoclose' => true,
                        ]
                    ])->label(false) ?>
                </div>
                <div class="col-md-3 search-form-input">
                    <?= Html::submitButton('Ищу пассажиров', [
                        'class' => 'btn btn-block btn-default submit-search-form',
                        'name' => 'passenger',
                        'value' => 'passenger'
                    ]); ?>
                </div>
                <div class="col-md-3 search-form-input">
                    <?= Html::submitButton('Ищу машину', [
                        'class' => 'btn btn-block btn-default submit-search-form',
                        'name' => 'driver',
                        'value' => 'driver',
                    ]); ?>
                </div>
                <?php ActiveForm::end(); ?>


            </div>
            <p><strong>Если вы не нашли нужную
                    поездку</strong>, <?= Html::a('создайте свою', '/trip/start/', [
                    'class' => 'create-link',
                ]); ?>, и вас найдут ваши попутчики.</p>
        </div>

    </div>

    <div class="tab-pane" id="vacation">
        <div class="vacation-search-form lead">
            Тут будет форма поиска попутчиков в отпуск заграницу, с кучей параметров.
        </div>
    </div>

</div>
