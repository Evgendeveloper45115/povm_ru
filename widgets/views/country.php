<?php

use yii\helpers\Html;

/* @var \yii\web\View $this */
/* @var \app\models\GeoCountry[] $items */

?>

<ul>
    <?php foreach ($items as $item) { ?>
        <li><?= Html::a($item->name, $item->getUrl()); ?></li>
    <?php } ?>
</ul>
