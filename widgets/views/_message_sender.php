<?php

use app\components\StringHelper;
use yii\helpers\Html;
use app\companion\model\Trip;
use yii\i18n\MessageFormatter;

/* @var \yii\web\View $this */
/* @var \app\models\Message $model */
/* @var \app\models\Message[] $messages */
?>
<h1>Исходящие сообщения</h1>
<div class="trips-list-item well">
    <div class="row">
        <div class="col-md-2">
            <div class="text-center">
                <?= Html::a(
                    Html::img($model->user->avatar ? '/img/profile/' . $model->user->avatar : '/img/thumbs/user.jpg', [
                        'class' => 'user-avatar',
                        'style' => 'max-width:50px',
                        'alt' => Html::encode($model->user ? $model->user->name : 'unknown'),
                    ]), ['/user/', 'id' => $model->user->id]
                ); ?>
            </div>
        </div>
        <div class="col-md-10">
            <?php
            $messages = \app\models\Message::find()->where(['sender_id' => \Yii::$app->user->id, 'user_id' => $model->user_id])->andWhere(['not', ['text_user' => null]])->andWhere(['!=', 'id', $model->id])->orderBy('id DESC')->all();
            foreach ($messages as $message) {
                ?>

                <div class="tip-content">
                    <div class="panel panel-default trip-description">
                        <div class="panel-body">
                            <?= $message->text_user ?>
                        </div>
                    </div>
                </div>
                <?php
                if ($message->text) {
                    ?>
                    <div class="tip-content" style="text-align: right">
                        <div class="panel panel-default trip-description">
                            <div class="panel-body">
                                <?= $message->text ?>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
            <div class="tip-content">
                <div class="panel panel-default trip-description">
                    <div class="panel-body">
                        <?= $model->text_user ?>
                    </div>
                </div>
            </div>
            <?php
            if ($model->text) {
                ?>
                <div class="tip-content" style="text-align: right">
                    <div class="panel panel-default trip-description">
                        <div class="panel-body">
                            <?= $model->text ?>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>

