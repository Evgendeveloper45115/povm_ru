<?php

use app\companion\model\Trip;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \app\companion\model\Trip */

$passengerIcon = '<div class="trip-icon" title="Кол-во пассажиров"><i class="fas fa-user"></i></div>';
$carIcon = '<div  class="trip-icon" title="Автомобиль"><i class="fas fa-car"></i></div>';
$clockIcon = '<div class="trip-icon" title="Время выезда"><i class="fas fa-clock"></i></div>';
$rubIcon = '<div class="trip-icon" title="Стоимость поездки"><i class="fas fa-ruble-sign "></i></div>';
$viewsIcon = '<div class="trip-icon" title="Просмотров"><i class="fas fa-eye "></i></div>';

$ulItems = [];
$ulItems[] = $passengerIcon . $model->passengers;

if ($model->type === Trip::USER_TYPE_DRIVER) {
    $title = 'Пассажиры ищут водителя';
    $lookingFor = 'Ищу водителя';
} elseif ($model->type === Trip::USER_TYPE_PASSENGER) {
    $title = 'Водитель ищет пассажиров';
    $lookingFor = 'Ищу пассажира';
    if ($model->car) {
        $car = Html::encode($model->car->manufacturer) . '&nbsp;' . Html::encode($model->car->model) . ' (' . $model->car->year . ')';
        $ulItems[] = $carIcon . $car;
    }
}

$ulItems[] = $clockIcon . Yii::$app->formatter->asTime($model->trip_time, 'php:H:i');

if ($model->price > 0) {
    $ulItems[] = $rubIcon . Yii::$app->formatter->asInteger($model->price);
}

$ulItems[] =  $viewsIcon . $model->views;

?>

<div class="trip-icons">
    <?= Html::ul($ulItems, [
        'encode' => false,
        'class' => 'trip-ul',
    ]); ?>
    <div class="clr"></div>
</div>

