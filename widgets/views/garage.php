<?php

use app\companion\model\Car;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider \yii\data\ArrayDataProvider */
/* @var $user \app\auth\models\User */

$currentUser = $user->id == Yii::$app->user->id;

?>

<h2>Гараж (<?= $dataProvider->count; ?>)</h2>
<div class="auto-list">
    <?php if ($currentUser) { ?>
        <div style="margin-bottom:10px;">
            <?= Html::a('<i class="fas fa-plus"></i> Добавить авто', ['/auto/create'], [
                'class' => 'btn btn-link',
            ]); ?>
        </div>
    <?php } ?>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [
            'class' => 'table',
        ],
        'showHeader' => false,
        'summary' => false,
        'emptyText' => $currentUser ? 'Вы пока не добавили автомобилей' : 'Пользователь еще не добавил авто',
        'columns' => [
            [
                'format' => 'raw',
                'value' => function($model) {
                    /** @var \app\companion\model\Car $model */
                    $car = Html::encode($model->manufacturer) . '&nbsp;' . Html::encode($model->model) . ' (' . $model->year . ')';
                    return Html::a($car, ['/auto/view', 'id' => $model->id]);
                }
            ],
            [
                'class' => \yii\grid\ActionColumn::class,
                'contentOptions' => [
                    'class' => 'text-right',
                ],
                'visible' => $currentUser,
                'controller' => 'auto',
                'template' => '{update} {delete}',
                'urlCreator' => function($action, $model) {
                    /** @var \app\companion\model\Car $model */
                    return ['/auto/' . $action, 'id' => $model->id];
                }
            ],
        ],
    ]); ?>
</div>
