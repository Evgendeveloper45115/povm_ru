<?php

use app\components\StringHelper;
use yii\helpers\Html;
use app\companion\model\Trip;
use yii\i18n\MessageFormatter;

/* @var \yii\web\View $this */
/* @var \app\models\Message $model */
/* @var \app\models\Message[] $messages */
?>
<h1>Входящие сообщения</h1>
<div class="trips-list-item well">
    <div class="row">
        <div class="col-md-2">
            <div class="text-center">
                <?= Html::a(
                    Html::img($model->sender->avatar ? '/img/profile/' . $model->sender->avatar : '/img/thumbs/user.jpg', [
                        'class' => 'user-avatar',
                        'style' => 'max-width:50px',
                        'alt' => Html::encode($model->sender ? $model->sender->name : 'unknown'),
                    ]), ['/user/', 'id' => $model->sender->id]
                ); ?>
            </div>
        </div>
        <div class="col-md-10">
            <?php
            $messages = \app\models\Message::find()->where(['user_id' => \Yii::$app->user->id, 'sender_id' => $model->sender_id])->andWhere(['!=', 'id', $model->id])->orderBy('id DESC')->all();
            foreach ($messages as $message) {
                ?>

                <div class="tip-content">
                    <div class="panel panel-default trip-description">
                        <div class="">
                            <?php
                            if (!$message->text_user) {
                                ?>
                                <div class="trip-title pull-right">
                                    <?= Html::a('Ответить', \yii\helpers\Url::to(['/site/send-message-to-sender', 'id' => $message->id])); ?>
                                </div>
                                <div class="clr"></div>
                                <?php
                            }
                            ?>
                        </div>

                        <div class="panel-body">
                            <?= $message->text ?>
                        </div>
                    </div>
                </div>
                <?php

                if ($message->text_user) {
                    ?>
                    <div class="tip-content" style="text-align: right">
                        <div class="panel panel-default trip-description">
                            <div class="panel-body">
                                <?= $message->text_user ?>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
            <div class="tip-content">
                <div class="panel panel-default trip-description">
                    <div class="">
                        <?php
                        if (!$model->text_user) {
                            ?>
                            <div class="trip-title pull-right">
                                <?= Html::a('Ответить', \yii\helpers\Url::to(['/site/send-message-to-sender', 'id' => $model->id])); ?>
                            </div>
                            <div class="clr"></div>
                            <?php
                        }
                        ?>
                    </div>

                    <div class="panel-body">
                        <?= $model->text ?>
                    </div>
                </div>
            </div>
            <?php
            if ($model->text_user) {
                ?>
                <div class="tip-content" style="text-align: right">
                    <div class="panel panel-default trip-description">
                        <div class="panel-body">
                            <?= $model->text_user ?>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>

