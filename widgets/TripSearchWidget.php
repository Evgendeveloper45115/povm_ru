<?php

namespace app\widgets;


use app\companion\model\Trip;
use app\models\GeoTown;
use app\models\TripSearch;
use yii\base\Widget;

class TripSearchWidget extends Widget
{
    public $my = false;
    public $title;
    public $type = null;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $aliasFrom = \Yii::$app->getRequest()->get('cityFrom');
        $aliasTo = \Yii::$app->getRequest()->get('cityTo');
        $townFrom = GeoTown::findOne(['alias' => $aliasFrom]);
        $townTo = GeoTown::findOne(['alias' => $aliasTo]);
        $searchModel = new TripSearch();
        $dataProvider = $searchModel->searchByTown($townFrom, $townTo, $this->type);

        return $this->render('trips', [
            'dataProvider' => $dataProvider,
            'title' => $this->title,
        ]);
    }
}
