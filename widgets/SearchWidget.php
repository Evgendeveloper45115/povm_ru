<?php

namespace app\widgets;


use app\companion\model\Trip;
use app\models\SearchForm;
use app\models\TripSearch;
use yii\base\Widget;

class SearchWidget extends Widget
{
    public $townFrom;
    public $townTo;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $searchForm = new SearchForm();

        return $this->render('search', [
            'searchForm' => $searchForm,
        ]);
    }
}
