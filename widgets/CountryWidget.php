<?php

namespace app\widgets;

use app\models\GeoCountry;
use yii\base\Widget;

/**
 * Class CountryWidget
 * @package app\widgets
 */
class CountryWidget extends Widget
{
    /**
     * @var GeoCountry[]
     */
    protected $items;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->items = GeoCountry::getList();

        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        return $this->render('country', [
            'items' => $this->items,
        ]);
    }
}