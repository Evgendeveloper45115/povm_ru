<?php

namespace app\widgets;

use Yii;
use app\models\GeoTown;
use yii\base\Widget;

/**
 * Class RightSideWidget
 * @package app\widgets
 */
class RightSideWidget extends Widget
{
    /**
     * Отображать кнопку "Найти попутчика"
     *
     * @var bool
     */
    public $showCreateBtn = false;

    /**
     * Отображать группу VK
     *
     * @var bool
     */
    public $showVkGroup = false;

    /**
     * Отображать яндексовскую "Рассказать друзям"
     *
     * @var bool
     */
    public $showYaShare = false;

    /**
     * Отображать "Авто наших водителей"
     *
     * @var bool
     */
    public $showCars = false;

    /**
     * Отображать город
     *
     * @var bool
     */
    public $showTown = false;

    /**
     * Попытка автоматического определение местоположения юзера
     *
     * @var bool
     */
    public $detectTown = true;

    /**
     * ID города для отображения
     *
     * @var int
     */
    public $townId;

    /**
     * Название города для отображения
     *
     * @var string
     */
    public $townName;

    /**
     * Город для отображения
     *
     * @var GeoTown
     */
    protected $town;


    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        return $this->render('right-side', [
            'town'          => $this->town,
            'showCreateBtn' => $this->showCreateBtn,
            'showVkGroup'   => $this->showVkGroup,
            'showYaShare'   => $this->showYaShare,
            'showCars'      => $this->showCars,
            'showTown'      => $this->showTown,
        ]);
    }
}