<?php

namespace app\widgets;


use app\companion\model\Trip;
use app\models\TripSearch;
use yii\base\Widget;

class TripWidget extends Widget
{
    public $title;
    public $type;
    public $tripTime;
    public $townFrom;
    public $townTo;
    public $my = false;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $params = [
            'type' => $this->type,
            'tripTime' => $this->tripTime,
            'townFrom' => $this->townFrom,
            'townTo' => $this->townTo,
            'status' => [Trip::STATUS_ACTIVE, Trip::STATUS_FINISHED],
        ];
        $searchModel = new TripSearch();
        $dataProvider = $this->my ? $searchModel->search($params, $this->my) : $searchModel->search($params);

        return $this->render('trips', [
            'dataProvider' => $dataProvider,
            'title' => $this->title,
        ]);
    }
}
