<?php

namespace app\widgets;


use app\auth\models\User;
use yii\base\InvalidArgumentException;
use yii\base\Widget;
use yii\data\ArrayDataProvider;

/**
 * Class GarageWidget
 * @package app\widgets
 */
class GarageWidget extends Widget
{
    /**
     * @var User
     */
    public $user;

    /**
     * @inheritdoc
     */
    public function init()
    {
        if ($this->user instanceof User) {
            parent::init();
        } else {
            throw new InvalidArgumentException('User can\'t be empty');
        }
        
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $dataProvider = new ArrayDataProvider([
            'allModels' => $this->user->cars,
        ]);

        return $this->render('garage', [
            'user' => $this->user,
            'dataProvider' => $dataProvider,
        ]);
    }
}
