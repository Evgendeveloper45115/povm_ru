<?php

namespace app\widgets;


use app\models\TripHistorySearch;
use app\auth\models\User;
use Yii;
use yii\base\InvalidArgumentException;
use yii\base\Widget;

/**
 * Class TripHistory
 * @package app\widgets
 */
class TripHistory extends Widget
{
    /**
     * @var int
     */
    public $userId;

    /**
     * @inheritdoc
     */
    public function init()
    {
        if (!$this->userId) {
            throw new InvalidArgumentException('UserID must be set');
        }

        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $searchModel = new TripHistorySearch();
        $searchModel->user_id = $this->userId;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('trip-history', [
            'dataProvider' => $dataProvider,
        ]);
    }
}
