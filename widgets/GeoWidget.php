<?php

namespace app\widgets;


use app\models\GeoCountry;
use app\models\GeoRegion;
use yii\base\InvalidArgumentException;
use yii\base\Widget;

/**
 * Class GeoWidget
 * @package app\widgets
 */
class GeoWidget extends Widget
{
    /** @var GeoCountry|GeoRegion */
    public $model;

    /** @var \app\models\GeoRegion[]|\app\models\GeoTown[] */
    protected $items;

    /**
     * @inheritdoc
     */
    public function init()
    {
        if ($this->model instanceof GeoCountry || $this->model instanceof GeoRegion) {
            $this->getItems();

            parent::init();
        } else {
            throw new InvalidArgumentException('model must be instance of GeoCountry or GeoRegion model');
        }

    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        return $this->render('geo', [
            'items' => $this->items,
        ]);
    }

    /**
     * 
     */
    protected function getItems()
    {
        if ($this->model instanceof GeoCountry) {
            $this->items = $this->model->regions;
        }

        if ($this->model instanceof GeoRegion) {
            $this->items = $this->model->towns;
        }
    }
}