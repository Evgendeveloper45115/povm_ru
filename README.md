# Composer + Docker
Установить зависимости
```docker-compose run --rm php composer install```

Обновить зависимости
```docker-compose run --rm php composer update```

# Как поддерживать проект
### Перед коммитом - обязательно локально запусти тесты

Запуск unit тестов
```
docker-compose run --rm php vendor/bin/codecept run unit
```

# Как развернуть проект
1. Поднимаем докер ```docker-compose up```
2. Заливаем дамп в старую базу ```docker exec -i povm2_db_old_1 /usr/bin/mysql -u root --password=example povm2 < deploy/dump/dump.sql```
3. Запускаем ```docker-compose run --rm php sh install```

## Конфиги для Docker

config/db.php
```php
<?php

use yii\db\Connection;

return [
    'class' => Connection::class,
    'dsn' => 'mysql:host=db:3306;dbname=povm2',
    'username' => 'root',
    'password' => 'example',
    'charset' => 'utf8',
];
```

config/db_old.php
```php
<?php

use yii\db\Connection;

return [
    'class' => Connection::class,
    'dsn' => 'mysql:host=db_old:3306;dbname=povm2',
    'username' => 'root',
    'password' => 'example',
    'charset' => 'utf8',
];
```
