<?php
/**
 * Created by PhpStorm.
 * User: michael
 * Date: 20.07.19
 * Time: 13:59
 */

namespace tests\models;


use app\models\CarMark;

class CarMarkTest extends \Codeception\Test\Unit
{
    public function testObjectCreation()
    {
        $carMark = new CarMark();
        $this->assertTrue(CarMark::class, get_class($carMark));
    }
}
