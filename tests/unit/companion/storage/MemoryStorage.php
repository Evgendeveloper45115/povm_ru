<?php

namespace app\tests\unit\companion\storage;

use app\companion\storage\TripStorage;

class MemoryStorage implements TripStorage
{
    private $items = [];

    public function get(): array {
        return $this->items;
    }

    public function put(array $items): void {
        $this->items = $items;
    }
}
