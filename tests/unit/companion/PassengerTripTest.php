<?php

namespace app\tests\unit\companion;

use app\companion\form\AddPassengerTripForm;
use app\companion\PassengerTrip;
use app\tests\unit\companion\storage\MemoryStorage;
use Codeception\Test\Unit;

class PassengerTripTest extends Unit
{
    /**
     * @var PassengerTrip
     */
    private $trip;

    public function setUp(): void
    {
        parent::setUp();
        $this->trip = new PassengerTrip(new MemoryStorage());
    }

    public function testCreate(): void
    {
        $this->assertEquals([], $this->trip->getTrip());
    }

    public function testAdd(): void
    {
        // todo добавить в зависимость города с идентификаторами 1 и 2
        $form = new AddPassengerTripForm();
        $form->simpleTrip->toLocationId = 1;
        $form->simpleTrip->fromLocationId = 2;

        $this->trip->addTripInfo($form);

        $this->assertCount(2, $trip = $this->trip->getTrip());

        $this->assertEquals(1, $trip['fromLocationId']);
        $this->assertEquals(2, $trip['toLocationId']);

        $this->trip->addUser(4);
        $this->assertCount(3, $trip = $this->trip->getTrip());
        $this->assertEquals(4, $trip['userId']);
    }
}
