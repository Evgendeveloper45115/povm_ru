<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <div id="main-wrapper">
        <table width="100%" border="0" cellpadding="5" cellspacing="5">
            <tr>
                <td width="20%"><a href='http://www.povm.ru/?utm_medium=email&utm_source=message_header'><img src="http://povm.ru/img/logo_bar.png" alt="Поиск попутчиков" width="209" height="60" align="middle" /></a></td>
                <td width="33%"><h3>Попутчики.Ру</h3></td>
                <td width="33%"></td>
            </tr>

        </table>
        <hr/>

        <?= $content ?>

    </div>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
