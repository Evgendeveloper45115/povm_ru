<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $confirmCode string */
/* @var $message_text string */
$confirmUrl = Url::to(['/dialog/send-view', 'email' => $user->email, 'code' => $user->confirm_code], true);
$confirmLink = Html::a('Ссылке', $confirmUrl);

?>
<h1>Новое сообщение!</h1>

<p>Пользователь <strong><?= Yii::$app->user->identity->name ?></strong> написал вам новое сообщение:</p>

<! --
<?= $message_text ?>
-->

<div>
    Для просмотра сообщения перейдите по этой <?= $confirmLink ?>
</div>
