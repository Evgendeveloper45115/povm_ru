<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $confirmCode string */

$confirmUrl = Url::to(['/signup/confirm', 'code' => $confirmCode], true);
$confirmLink = Html::a($confirmUrl, $confirmUrl);

?>
Регистрация на сайте «Поехали вместе!»
Здравствуйте!
    Вы зарегистрировались на сайте поиска попутчиков <?= Html::a('Поехали вместе!', '/') ?>
Для того, чтобы начать пользоваться всеми возможностями сайта, необходимо подтвердить свой email. Для этого перейдите по ссылке:
<?= $confirmLink ?>
