<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $user \tests\models\User */
/* @var $trip \app\companion\model\Trip */
/* @var $respond \app\models\Respond */
$confirmUrl = Url::to(['/dialog/send-view', 'email' => $user->email, 'code' => $user->confirm_code], true);
$confirmLink = Html::a('Ссылке', $confirmUrl);

?>
<h1>Новый отклик на вашу поездку!</h1>
<p>По вашей поездке:<strong> <?= $trip->town_from . ' - ' . $trip->town_to ?> </strong><br/>
    Получен новый отклик от:<strong> <?= Yii::$app->user->identity->name ?> </strong><br/>
    Комментарий:<strong> <?= $respond->comment ?> </strong></p>
<div>
    Для просмотра отклика и отправки ответа перейдите по этой <?= $confirmLink ?>
</div>
