<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';
$dbOld = require __DIR__ . '/db_old.php';

$config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'db_old' => $dbOld,
        'urlManager' => [
            'baseUrl' => 'http://povm2/',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'suffix' => '/',
            'normalizer' => [
                'class' => \yii\web\UrlNormalizer::class,
                'collapseSlashes' => true,
                'normalizeTrailingSlash' => true,
            ],
            'rules' => [
                '/user' => '/profile/view',
                '/user/<id:\d+>' => '/profile/view',
                '/user/<action:(update|block|change-password)>' => '/profile/<action>',
                '/<country:(belarus|russia|ukraine)>' => '/trip/country',
                '/<country:(belarus|russia|ukraine)>/<region:[\w\-]+>' => '/trip/region',
                '/<country:(belarus|russia|ukraine)>/<region:[\w\-]+>/<town:[\w\-]+>' => '/trip/town',
                '/search' => '/trip/search',
                '/trip/<id:\d+>' => '/trip/view',
                '/trip/update/<id:\d+>' => '/trip/update',
                '/trip' => '/trip/index',
                '/route/<cityFrom:[\w\-]+>_<cityTo:[\w\-]+>' => '/route/view',
                '/route/<letter:[\w]{1}>' => '/route/routes',
                '/dialog/<dialog:\d+>' => '/dialog/view',
                '/dialog' => '/dialog/index',
                '/logout' => '/site/logout',
                '/<action:(login|feedback|about|safety|rules|partners|town)>' => '/site/<action>',
                '/car/<id:\d+>' => '/auto/view',
                '/car' => '/auto/index',
                '/car/create' => '/auto/create',
                '/car/models' => '/auto/models',
                '/car/<action:(update|delete)>/<id:\d+>' => '/auto/<action>',
                '/signup/confirm' => '/auth/auth/confirm',

            ],
        ],


    ],
    'params' => $params,
    /*
    'controllerMap' => [
        'fixture' => [ // Fixture generation command line.
            'class' => 'yii\faker\FixtureController',
        ],
    ],
    */
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
