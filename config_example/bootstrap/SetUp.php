<?php

namespace app\config\bootstrap;

use app\companion\DriverTrip;
use app\companion\PassengerTrip;
use app\companion\storage\HybridTripStorage;
use Yii;
use yii\base\BootstrapInterface;
use yii\mail\MailerInterface;

/**
 * Бутстрапим приложение
 */
class SetUp implements BootstrapInterface
{
    public function bootstrap($app): void
    {
        $container = Yii::$container;

        $container->setSingleton(PassengerTrip::class, function () use ($app) {
            $storage = new HybridTripStorage($app->user, 'passenger_trip', 3600 * 24);
            return new PassengerTrip($storage);
        });

        $container->setSingleton(DriverTrip::class, function () use ($app) {
            $storage = new HybridTripStorage($app->user, 'driver_trip', 3600 * 24);
            return new DriverTrip($storage);
        });

        $container->setSingleton(MailerInterface::class, function () use ($app) {
            return $app->mailer;
        });
    }
}
